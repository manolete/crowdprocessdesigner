name := "deps"

version := "1.0"

scalaVersion := "2.11.4"

retrieveManaged := true

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq(
	"org.scalatest" % "scalatest_2.11" % "3.0.0-SNAP2",
	"org.scalactic" % "scalactic_2.11" % "3.0.0-SNAP2",
	"com.fasterxml.jackson.module" % "jackson-module-scala_2.11" % "2.4.3",
	"com.fasterxml.jackson.module" % "jackson-module-paranamer" % "2.4.3",
	"com.corundumstudio.socketio" % "netty-socketio" % "1.6.6",
	"com.thoughtworks.paranamer" % "paranamer" % "2.7",
	"junit" % "junit" % "4.8.1" % "test",
	"com.novocode" % "junit-interface" % "0.8" % "test->default",
	"com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
	"org.slf4j" % "slf4j-log4j12" % "1.7.5",
	"org.apache.commons" % "commons-vfs2" % "2.0",
	"org.apache.httpcomponents" % "httpclient" % "4.3.5",
	"org.apache.httpcomponents" % "httpcore-osgi" % "4.3.2",
	"org.apache.httpcomponents" % "httpclient-osgi" % "4.3.5",
	"org.scala-lang.modules" %% "scala-xml" % "1.0.2"
)

// for debugging sbt problems
logLevel := Level.Debug
