/**
 * Router module for BackboneJS
 * @module router
 */
define(["./views/navbarView", "./views/introductionView"],
  /** @lends module:router */
  function(NavbarView, IntroductionView) {
    return Backbone.Router.extend({
      routes: {
        "": "index",
        "introduction": "introduction"
      },
			/** Initalizes the navbar views (which handles consecutive routing) */
      initialize: function() {
        new NavbarView();
      },
			/** Shows the {@link module:introductionView} */
      introduction: function() {
        IntroductionView.show();
      }
    });
  });
