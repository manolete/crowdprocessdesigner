/**
	RequireJS main module
	* @module main-ide
*/
require.config({
	paths : {
		backbone : '../lib/backbonejs/backbone',
		underscore : '../lib/underscorejs/underscore',
		jquery : '../lib/jquery/jquery',
		jsRouter : '/JavascriptRoutesController/router',
		text : '../lib/requirejs-text/text',
		chart : '../lib/chartjs/Chart',
		socketIo : 'socket.io.min',
		d3: '../lib/d3js/d3',
		fabricjsViewport: 'fabricjs_viewport'
	},
	shim : {
		fabricjsViewport: {
			deps: ["fabric", "jquery"],
			exports: "fabricjs_viewport"
		},
		jquery: {
			exports: "$"
		},
		underscore: {
			exports: "_"
		},
		jqueryMousewheel: {
			deps: ['jquery']
		}
	}
});

require([ "ide" ]);
