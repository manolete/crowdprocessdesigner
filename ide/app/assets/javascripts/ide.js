/**
 * Bootstrap Module to setup the ide
 * @module ide
 */
define(function() {
  require(["./router", "./canvas/editor", "./dispatcher"],
  /** @lends module:ide  */
  function(router, canvasEditor, dispatcher) {
    canvasEditor.setup();

    new router();
    Backbone.history.start();
  });
});
