/**
 * Module providing the backbone view to configure available services
 * @module views/serviceView
 */
define(
  ["backbone", "underscore", "../canvas/editor", '../repository', './objectPropertyView', 'text!../templates/serviceView.html'],
  /** @lends module:views/serviceView  */
  function(Backbone, _, editor, repository, ObjectPropertyView,
    ServiceViewTpl) {
    var serviceView = Backbone.View
      .extend({
        el: $("<div><form></form></div>").appendTo("#centerView"),
        events: {
          'click #closeServiceView': 'close',
          'click #addService': 'addService',
          'click a[service]': 'configureService',
          'click a[delete]': 'deleteService',
          'change input[taskname]': 'updateName'
        },
        initialize: function() {
          this.objectPropertyView = new ObjectPropertyView({
            parent: this
          });
        },
        /** Adds a new empty configuration to the services of the current canvas object */
        addService: function(e) {
          this.canvasObject.services.push([]);
          this.serviceIndex = this.canvasObject.services.length - 1;
          this.canvasObject.update();
          editor.updateCanvas();
          this.render();
          this.configureService(e);
        },
        /** Updates the displayed name of the current canvas object */
        updateName: function() {
          var label = this.$el.find("input[type='text']");

          if (this.canvasObject.text && label) {
            this.canvasObject.text.set('text', label.val());
            editor.updateCanvas();
          }
        },
        /** Renders the subview to configure a specific service of the current canvas object */
        configureService: function(clickEv) {
          this.serviceIndex = clickEv.currentTarget.id;
          this.assign({
            '#objectPropertyView': this.objectPropertyView
          });
        },
        /** Deletes a service configuration */
        deleteService: function(clickEv) {
          this.canvasObject.services.splice(clickEv.currentTarget.id, 1);
          this.canvasObject.update();
          editor.updateCanvas();
          this.render();
        },
        /** Opens the view
         * @param {string} canvasObject - The canvas object to display the configuration
         */
        show: function(canvasObject) {
          this.canvasObject = canvasObject;
          this.render();
        },
        /** Closes the view (empties the div and unbinds unneed event listeners) */
        close: function() {
          this.$el.empty();
          this.unbind();
        },
        /** Renders the view */
        render: function() {
          if (!this.canvasObject.services) {
            this.canvasObject.services = [];
          }

          var serviceList = _.template(ServiceViewTpl, {
            id: this.canvasObject.id,
            services: this.canvasObject.services,
            text: this.canvasObject.text ? this.canvasObject.text.text : null
          });

          this.$el.empty();
          this.$el.append(serviceList);

          return this.el;
        },
        /** Assigns the subview {@link module:objectPropertyView} to rerender on click on a service */
        assign: function(selector, view) {
          var selectors;

          if (_.isObject(selector)) {
            selectors = selector;
          } else {
            selectors = {};
            selectors[selector] = view;
          }

          if (!selectors)
            { return; }

          _.each(selectors, function(view, selector) {
            view.setElement(this.$(selector)).render(this.serviceIndex, this.canvasObject);
          }, this);
        }
      });

    return /** @alias module:views/serviceView */ new serviceView();
  });
