/**
 * Module providing event handlers for the navbar
 * Hooks directly into the html and provides no template
 * @module views/navbarView
 */
define(["backbone", "underscore", "../canvas/serializer", "../repository", "./logView", './portalView', './processView', './chartView', './treeView', './introductionView', './documentationView', '../notification'],
  /** @lends module:views/navbarView  */
  function(Backbone, _, Serializer, repository, logView, portalView, processView, chartView, treeView, introductionView, documentationView, notification) {
    var navbarView = Backbone.View.extend({
      el: $("#navbar"),
      events: {
        "click #save": "save",
        "click #load": "load",
        "click #downloadAsJson": "downloadAsJson",
        "click #downloadLogs": "downloadLogs",
        "click #run": "run",
        "click #nextStep": "nextStep",
        "click #log": "showLog",
        "click #portalSettings": "showPortalSettings",
        "click #uploadLink": "loadProcess",
        "change #upload:hidden": "uploadProcess",
        "click #uploadLogsLink": "loadLogs",
        "change #uploadLogs:hidden": "uploadLogs",
        "click #processes": "showProcesses",
        "click #chart": "showChart",
        "click #tree": "showTree",
        "click #introduction": "showIntroduction",
        "click #documentation": "showDocumentation"
      },
      /** Serializes and saves the current canvas to localStorage */
      save: function() {
        Serializer.serialize("canvas");
        notification.addMessage("Process saved to local storage");
      },
      /** Loads and draws a new canvas by deserializing from localStorage*/
      load: function() {
        Serializer.deserialize("canvas");
        notification.addMessage("Process loaded from local storage");
      },
      /**
     * Trigger download of the canvas as Json
     * @param {string} e - Jquery Event (onclick)
     */
      downloadAsJson: function(e) {
        e.target.href = "data:text/json;charset=utf-8," + Serializer.serialize();
      },
      /**
     * Trigger download of the logs stored in localStorage
     * @param {string} e - Jquery Event (onclick)
     */
      downloadLogs: function(e) {
        e.target.href = "data:text/json;charset=utf-8," + localStorage.getItem('logs');
      },
      /** Empties the current logs and runs the process defined on the current canvas */
      run: function() {
        localStorage.removeItem("logs");
        repository.runProcess(Serializer.serialize());
      },
      /** Shows the  {@link module:views/logView} */
      showLog: function() {
        logView.show();
      },
      /** Shows the  {@link module:views/treeView} */
      showTree: function() {
        treeView.show();
      },
      /** Shows the  {@link module:views/processView} */
      showProcesses: function() {
        processView.show();
      },
      /** Shows the  {@link module:views/chartView} */
      showChart: function() {
        chartView.show();
      },
      /** Shows the  {@link module:views/introductionView} */
      showIntroduction: function() {
        introductionView.show();
      },
      /** Shows the  {@link module:views/documentationView} */
      showDocumentation: function() {
        documentationView.show();
      },
      /** Shows the  {@link module:views/portalView} */
      showPortalSettings: function() {
        portalView.show();
      },
      loadLogs: function(e) {
        e.preventDefault();
        $("#uploadLogs:hidden").trigger('click');
      },
      /**
     * Upload a given log file
     * @param {string} e - Jquery Event
     */
      uploadLogs: function(e) {
        var file = e.target.files[0];
        var reader = new FileReader();

        reader.onload = (function(file) {
          return function(e) {
            var fileText = e.target.result;
            localStorage.setItem('logs', fileText);
          };
        })(file);

        reader.readAsText(file);
        notification.addMessage("Logs successfully uploaded");
      },
      loadProcess: function(e) {
        e.preventDefault();
        $("#upload:hidden").trigger('click');
      },
      /**
     * Upload a given process file
     * @param {string} e - Jquery Event
     */
      uploadProcess: function(e) {
        var file = e.target.files[0];
        var reader = new FileReader();

        reader.onload = (function(file) {
          return function(e) {
            var fileText = e.target.result;
            Serializer.deserializeFromString(fileText);
          };
        })(file);

        reader.readAsText(file);
        notification.addMessage("Process successfully uploaded");
      }
    });

    return  /** @alias module:views/navbarView */navbarView;
  });
