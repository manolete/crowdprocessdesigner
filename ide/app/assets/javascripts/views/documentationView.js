/**
 * Module providing the backbone view for the introduction video
 * @module views/documentationView
 */
define(["backbone", "underscore", 'text!../templates/documentationView.html'],
  /** @lends module:views/documentationView  */
  function(Backbone, _, documentationViewTpl) {
    var documentationView = Backbone.View.extend({
      el: $("<div></div>").appendTo("#centerView"),
      events: {
        'click #closeDocumentationView': 'close'
      },
      /** Opens the view */
      show: function() {
        var documentation = _.template(documentationViewTpl, {});

        this.$el.empty();
        this.$el.append(documentation);
      },
      /** Closes the view (empties the div and unbinds unneed event listeners) */
      close: function() {
        this.$el.empty();
        this.unbind();
      },
    });

    return /** @alias module:views/documentationView */ new documentationView();
  });
