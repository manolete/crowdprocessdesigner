/**
 * Module providing a view showing a list of logs
 * @module views/logView
 */
define(["backbone", "underscore", 'text!../templates/logView.html'],
  /** @lends module:views/logView  */
  function(Backbone, _, logViewTpl) {
    var logView = Backbone.View.extend({
      el: $("<div><form></form></div>").appendTo("#centerView"),
      events: {
        'click #closeLogView': 'close'
      },
      /** Opens the view */
      show: function() {
        var columns = [];
        var logString = localStorage.getItem("logs");
        var logs = JSON.parse(logString);

        if (logs && logs.length > 0) {
          for (var k in logs[0]) {
            if (logs[0].hasOwnProperty(k)) {
              columns.push(k);
            }
          }
        }

        var logTpl = _.template(logViewTpl, {
          columns: columns,
          logs: logs
        });

        this.$el.empty();
        this.$el.append(logTpl);
      },
      /** Closes the view (empties the div and unbinds unneed event listeners) */
      close: function() {
        this.$el.empty();
        this.unbind();
      },
    });

    return /** @alias module:views/logView */ new logView();
  });
