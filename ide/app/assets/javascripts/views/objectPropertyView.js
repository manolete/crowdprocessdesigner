/**
 * Module providing the backbone view for the introduction video
 * @module views/objectPropertyView
 */
define(["backbone", "underscore", 'text!../templates/objectPropertyView.html', '../repository', '../canvas/editor', '../notification'],
  /** @lends module:views/objectPropertyView  */
  function(Backbone, _, objectPropertyViewTpl, repository, editor, notification) {
    var ObjectPropertyView = Backbone.View.extend({
      events: {
        'click #saveButton': 'save',
        'change #service': 'saveAndRender'
      },
      /**
       * Renders the service index with its input parameters
       * @param {int} serviceIndex - The index of the service configuration in the canvas objects service array
       * @param {canvasObject} author - The canvas object to display the service configuration
       */
      render: function(serviceIndex, canvasObject) {
        this.canvasObject = canvasObject;
        this.serviceIndex = serviceIndex;
        repository.getService(canvasObject.type).done(
          this.renderTemplate.bind(this));
      },
      /** Renders the template
       * @param {Array} tasks - The available tasks that may be executed
       */
      renderTemplate: function(tasks) {
        this.tasks = tasks;

        var taskInformation = _.find(this.$el.serializeArray(),
          function(s) {
            return s.name === "service";
          });
        var inputValueArray = this.canvasObject.services[this.serviceIndex];
        if (!taskInformation) {
          taskInformation = _.find(inputValueArray, function(s) {
            return s.name === "service";
          });
        }

        var task = taskInformation ? _.find(tasks, function(d) {
          var classNameAndServiceName = taskInformation.value.split(",");
          return d.className === classNameAndServiceName[0] && d.information.name === classNameAndServiceName[1];
        }) : null;
        var inputParams = [];

        if (task) {
          for (var param in task.information.inputParams) {
            if (task.information.inputParams.hasOwnProperty(param)) {
              var paramVal = task.information.inputParams[param]; // default
              console.log(paramVal);
              inputParams.push({
                name: param,
                value: paramVal
              });
            }
          }
        }

        var inputValueMap = _.object(_.map(inputValueArray, function(item) {
          return [item.name, _.escape(item.value)];
        }));
        var dropdown = _.template(objectPropertyViewTpl, {
          name: "service",
          tasks: tasks,
          selected: taskInformation,
          inputs: inputParams,
          inputValues: inputValueMap
        });
        this.$el.empty();
        this.$el.append(dropdown);
      },
      /** Saves the configuration and renders the template */
      saveAndRender: function(e) {
        this.save(e);
        this.renderTemplate(this.tasks);
      },
      /** Saves the configuration on the canvas object */
      save: function(e) {
        e.preventDefault();
        var values = this.$el.children().first().serializeArray();
        this.canvasObject.services[this.serviceIndex] = values;
        if (e.type === 'click') { notification.addMessage("Configuration Saved"); }
      }
    });

    return /** @alias module:views/objectPropertyView */ ObjectPropertyView;
  });
