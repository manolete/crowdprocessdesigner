/**
 * Module providing the the html for displaying the chartJS charts
 * @module views/chartView
 */
define(["backbone", "underscore", 'text!../templates/chartView.html', "chart"],
  /** @lends module:views/chartView  */
  function(Backbone, _, chartViewTpl, chart) {
    var chartView = Backbone.View.extend({
      el: $("<div><form></form></div>").appendTo("#centerView"),
      /** Defines the relative url to the webworkers for the datasources in chartTypes */
      webworkerUrl: "./assets/javascripts/chartWebworkers/",
      /** Defines the available chart types */
      chartTypes: ["Bar", "Pie"],
      /** Defines the available chart data sources */
      data: ["Time", "Cost"],
      currentChart: null,
      events: {
        'click #closeChartView': 'close',
        'click #updateChart': 'updateChart'
      },
      /** Updates and rerenders the chart */
      updateChart: function(ev) {
        var formKeys = {};
        $.each($(ev.target.form).serializeArray(), function() {
          formKeys[this.name] = this.value;
        });

        var logString = localStorage.getItem("logs");
        var logs = JSON.parse(logString);

        var worker = new Worker(this.webworkerUrl + "chart" + formKeys.data + formKeys.chartType + ".js");
        worker.postMessage(logs);

        if (this.currentChart !== null) {
          this.currentChart.clear();
          this.currentChart.destroy();
        }

        var ctx = document.getElementById("logChart").getContext("2d");
        chart = new Chart(ctx);

        var self = this;
        var options = {
          showTooltips: false
        };

        worker.onmessage = function(oEvent) {
          switch (formKeys.chartType) {
            case "Bar":
              self.currentChartData = oEvent.data;
              self.currentChart = chart.Bar(oEvent.data, options);
              break;
            case "Pie":
              self.currentChartData = oEvent.data;
              self.currentChart = chart.Pie(oEvent.data, options);
              break;
          }

          $("#logChart").mousemove(function(e) {
            var activeElements = [];
            if (self.currentChart.getSegmentsAtEvent) {
              activeElements = self.currentChart.getSegmentsAtEvent(e);
            } else if (self.currentChart.getBarsAtEvent) {
              activeElements = self.currentChart.getBarsAtEvent(e);
            }

            if (activeElements.length > 0) {
              var relevantData = _.find(self.currentChartData.runtimes ? self.currentChartData.runtimes : self.currentChartData, function(d) {
                return d.label === activeElements[0].label && d.value === activeElements[0].value;
              });
              $("#legendToolTip").css({
                'visibility': 'visible',
                left: e.screenX - 150,
                top: e.screenY - 70
              }).html(relevantData ? relevantData.tooltipLabel : activeElements[0].value);
            } else {
              $("#legendToolTip").css({
                'visibility': 'hidden'
              });
            }
          });
        };

      },
      /** Opens the view */
      show: function() {
        var chartTpl = _.template(chartViewTpl, {
          chartTypes: this.chartTypes,
          data: this.data
        });

        this.$el.empty();
        this.$el.append(chartTpl);
      },
      /** Closes the view (empties the div and unbinds unneed event listeners) */
      close: function() {
        this.$el.empty();
        this.unbind();
      }
    });

    return /** @alias module:views/chartView */ new chartView();
  });
