/**
* Module providing the backbone view for the documentation
* @module views/introductionView
*/
define([ "backbone", "underscore", 'text!../templates/introductionView.html'],
/** @lends module:views/introductionView  */
function(Backbone, _, introductionViewTpl) {
	var introductionView = Backbone.View.extend({
		el : $("<div></div>").appendTo("#centerView"),
		events : {
			'click #closeIntroductionView' : 'close'
		},
		/** Opens the view */
		show : function() {
			var introduction = _.template(introductionViewTpl, {});

			this.$el.empty();
			this.$el.append(introduction);
		},
		/** Closes the view (empties the div and unbinds unneed event listeners) */
		close : function() {
			this.$el.empty();
			this.unbind();
		},
	});

	return /** @alias module:views/introductionView */new introductionView();
});
