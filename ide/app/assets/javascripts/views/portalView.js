/**
 * Module providing the backbone view for setting portal configurations (e.g. CrowdFlower apikey)
 * @module views/portalView
 */
define(
  ["backbone", "underscore", "../canvas/editor", '../repository', './objectPropertyView', 'text!../templates/portalView.html'],
  /** @lends module:views/portalView  */
  function(Backbone, _, editor, repository, ObjectPropertyView, portalTpl) {
    var PortalView = Backbone.View
      .extend({
        el: $("<div><form></form></div>").appendTo(
          "#centerView"),
        events: {
          'click #closePortalView': 'close',
          'change #portal': 'render',
          'click #saveButton': 'save'
        },
        /** Opens the view */
        show: function() {
          repository.getPortals().done(
            this.renderTemplate.bind(this));
        },
        /** Closes the view (empties the div and unbinds unneed event listeners) */
        close: function() {
          this.$el.empty();
          this.unbind();
        },
        /** Render the view on portal selection to show configuraiton parameters */
        render: function() {
          this.renderTemplate(this.portals);
        },
        /** Render template
         * @param {string} portals - Portals to display
         */
        renderTemplate: function(portals) {
          this.portals = portals;

          var portalInformation = _.find(this.$el.children().first().serializeArray(),
            function(s) {
              return s.name === "portal";
            });

          var portal = portalInformation ? _.find(this.portals, function(d) {
            var classNameAndServiceName = portalInformation.value.split(",");
            return d.className === classNameAndServiceName[0] && d.information.name === classNameAndServiceName[1];
          }) : null;

          var service = portalInformation ? _.find(editor.getCanvas().portalServices, function(s) {
            return _.some(s, function(d) {
              return d.name === "portal" && d.value === portalInformation.value;
            });
          }) : [];

          var inputParams = [];
          var getSaved = function(service, param) {
            return _.find(service, function(s) {
              return s.name === param;
            });
          };
          if (portal) {
            for (var param in portal.information.inputParams) {
              if (portal.information.inputParams.hasOwnProperty(param)) {
                var paramVal = portal.information.inputParams[param]; // default
                // value
                if (service) {
                  var saved = getSaved(service, param);
                  if (saved) {
                    paramVal = saved.value;
                  }
                }
                inputParams.push({
                  name: param,
                  value: paramVal
                });
              }
            }
          }

          var portalList = _.template(portalTpl, {
            name: "portal",
            portals: portals,
            selected: portalInformation,
            inputs: inputParams
          });

          this.$el.empty();
          this.$el.append(portalList);
        },
        /** Saves the configuration and renders the template */
        saveAndRender: function(e) {
          this.save(e);
          this.render(this.tasks);
        },
        /** Saves the configuration on the canvas object */
        save: function(e) {
          e.preventDefault();
          var values = this.$el.children().first().serializeArray();

          if (!editor.getCanvas().portalServices) {
            editor.getCanvas().portalServices = [];
          }

          var portalInformation = _.find(this.$el.children().first().serializeArray(),
            function(s) {
              return s.name === "portal";
            });

          var service = _.find(editor.getCanvas().portalServices, function(s) {
            return _.some(s, function(d) {
              return d.name === "portal" && d.value === portalInformation.value;
            });
          });

          if (service) {
            var index = _.indexOf(editor.getCanvas().portalServices, service);
            editor.getCanvas().portalServices[index] = values;
          } else {
            editor.getCanvas().portalServices.push(values);
          }
        },
      });

    return /** @alias module:views/portalView */ new PortalView();
  });
