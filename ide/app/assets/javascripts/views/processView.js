/**
 * Module providing the backbone view for showing available sample processes within the osgi environment
 * @module views/processView
 */
define(
  ["backbone", "underscore", "../canvas/editor", '../repository', 'text!../templates/processView.html', '../canvas/serializer'],
  /** @lends module:views/processView  */
  function(Backbone, _, editor, repository, processTpl, serializer) {
    var ProcessView = Backbone.View
      .extend({
        el: $("<div><form></form></div>").appendTo(
          "#centerView"),
        events: {
          'click #closeProcessView': 'close',
          'click a[process]': 'loadProcess'
        },
        /** Opens the view */
        show: function() {
          repository.getProcesses().done(
            this.renderTemplate.bind(this));
        },
        /** Closes the view (empties the div and unbinds unneed event listeners) */
        close: function() {
          this.$el.empty();
          this.unbind();
        },
        renderTemplate: function(processes) {
          var processList = _.template(processTpl, {
            processes: processes,
          });

          this.$el.empty();
          this.$el.append(processList);
        },
        /** Loads a process from the link
         * @param {string} clickEv - Jquery Event
         */
        loadProcess: function(clickEv) {
          var bundleIdAndResource = clickEv.currentTarget.id.split(",");
          repository.loadProcess(bundleIdAndResource[0], bundleIdAndResource[1]).done(function(d) {
            serializer.deserializeFromString(d);
          });
          this.close();
        }
      });

    return /** @alias module:views/processView */ new ProcessView();
  });
