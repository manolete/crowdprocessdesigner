/**
 * Module providing the backbone view for the execution tree
 * @module views/treeView
 */
define(["backbone", "underscore", 'text!../templates/treeView.html', 'd3', 'jquery', '../canvas/editor', 'repository'],
  /** @lends module:views/treeView */
  function(Backbone, _, treeViewTpl, d3, $, canvasEditor, repository) {
    var treeView = Backbone.View.extend({
      el: $("<div><form></form></div>").appendTo("#centerView"),
      /** Defines the relative url to the webworker for the tree data */
      webworkerUrl: "./assets/javascripts/treeWebWorkers/",
      events: {
        'click #closeTreeView': 'close'
      },
      /** Opens the view */
      show: function() {
        var logString = localStorage.getItem("logs");
        var logs = JSON.parse(logString);

        var treeTpl = _.template(treeViewTpl, {});

        this.$el.empty();
        this.$el.append(treeTpl);

        // get tree data from logs in an own worker to avoid stalling of the browser
        var worker = new Worker(this.webworkerUrl + "logTree.js");
        worker.postMessage(logs);

        var self = this;
        worker.onmessage = function(oEvent) {
          self.renderTree(oEvent.data, logs);
        };
      },
      /** Renders the tree on the canvas element
       * @param {Array} treeData - The treeData containing the root node in an array
       * @param {Array} logs - The logs to display the service configuration in the nodes
       */
      renderTree: function(treeData, logs) { // taken and adapted from: http://bl.ocks.org/d3noob/8375092
        var width = this.$el.width();
        var height = $(window).height() - 110;

        var margin = {
          top: 0,
          right: 0,
          bottom: 0,
          left: 80
        };

        var i = 0,
          duration = 750,
          root;

        var tree = d3.layout.tree()
          .size([height, width]);

        var diagonal = d3.svg.diagonal()
          .projection(function(d) {
            return [d.y, d.x];
          });

        var svg = d3.selectAll(this.$el.toArray()).append("svg")
          .attr("width", width + margin.right + margin.left)
          .attr("height", height + margin.top + margin.bottom)
          .append("g")
          .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        root = treeData[0];
        root.x0 = height / 2;
        root.y0 = 0;

        function update(source) {
          // Compute the new tree layout.
          var nodes = tree.nodes(root).reverse(),
            links = tree.links(nodes);

          // Normalize for fixed-depth.
          nodes.forEach(function(d) {
            d.y = d.depth * 60;
          });

          // Update the nodes
          var node = svg.selectAll("g.node")
            .data(nodes, function(d) {
              return d.id || (d.id = ++i);
            });

          var click = function(d) {
            if (d.taskType === 'File') {
              var resultLog = _.find(logs, function(l) {
                if (l.recombinationBranchIndexes) {
                  return d.recombinationBranchIndexes.join(",") === l.recombinationBranchIndexes.join(",") && d.taskObjectId === l.taskObjectId && l.eventType === 'Result';
                }
                return false;
              });

              var processIdLog = _.find(logs, function(l) {
                return l.eventType == 'ProcessStart';
              });
              if (resultLog) {
                var absoluteUrlRegex = new RegExp('^(?:[a-z]+:)?//', 'i');
                if (absoluteUrlRegex.test(resultLog.data)) {
                  window.open(resultLog.data, '_blank');
                } else {
                  var p = repository.getFile(processIdLog.data, d.recombinationBranchIndexes.join("_").replace(/,/g, "-"), resultLog.data);
                  p.done(function(url) {
                    window.open(url, '_blank');
                  });
                }
              }
            } else {
              if (d.children) {
                d._children = d.children;
                d.children = null;
              } else {
                d.children = d._children;
                d._children = null;
              }
              update(d);
            }
          };

          var tooltip = d3.select("body")
            .append("div")
            .attr("class", "tooltip");

          var mouseover = function(d) {
            tooltip.html("<b>" + d.name + "</b><br>" + d.taskSettings.split(",").join("<br>"));
            tooltip.style("visibility", "visible");
          };

          var mouseout = function(d) {
            tooltip.style("visibility", "hidden");
          };

          var mousemove = function(d) {
            tooltip.style("top", (d3.event.pageY - 10) + "px").style("left", (d3.event.pageX + 10) + "px");
          };

          // Enter any new nodes at the parent's previous position.
          var nodeEnter = node.enter().append("g")
            .attr("class", "node")
            .attr("transform", function(d) {
              return "translate(" + source.y0 + "," + source.x0 + ")";
            })
            .on("click", click)
            .on("mouseover", mouseover)
            .on("mouseout", mouseout)
            .on("mousemove", mousemove);

          nodeEnter.append("image")
            .attr("class", function(d) {
              return "node " + d.id;
            })
            .attr("xlink:href", function(d) {
              var url = $("#" + d.taskType).attr("src");
              return url;
            })
            .attr("width", "4%")
            .attr("height", "4%");

          // Transition nodes to their new position.
          this.nodeUpdate = node.transition()
            .duration(duration)
            .attr("transform", function(d) {
              return "translate(" + d.y + "," + d.x + ")";
            });

          // Transition exiting nodes to the parent's new position.
          this.nodeExit = node.exit().transition()
            .duration(duration)
            .attr("transform", function(d) {
              return "translate(" + source.y + "," + source.x + ")";
            })
            .remove();

          // Update the links
          var link = svg.selectAll("path.link")
            .data(links, function(d) {
              return d.target.id;
            });

          // Enter any new links at the parent's previous position.
          link.enter().insert("path", "g")
            .attr("class", "link")
            .attr("d", function(d) {
              var o = {
                x: source.x0,
                y: source.y0
              };
              return diagonal({
                source: o,
                target: o
              });
            });

          // Transition links to their new position.
          link.transition()
            .duration(duration)
            .attr("d", diagonal);

          // Transition exiting nodes to the parent's new position.
          link.exit().transition()
            .duration(duration)
            .attr("d", function(d) {
              var o = {
                x: source.x,
                y: source.y
              };
              return diagonal({
                source: o,
                target: o
              });
            })
            .remove();

          // Stash the old positions for transition.
          nodes.forEach(function(d) {
            d.x0 = d.x;
            d.y0 = d.y;
          });
        }

        update(root);

        d3.select(self.frameElement).style("height", height + "px");

      },
      /** Closes the view (empties the div and unbinds unneed event listeners) */
      close: function() {
        this.$el.empty();
        this.unbind();
      },
    });

    return /** @alias module:views/treeView */ new treeView();
  });
