/**
 * Module providing functions to retrieve and send data from outside the ide
 * (connection over SocketIO)
 * @module repository
 */
define(['socketIo', 'jquery', 'backbone', 'underscore', 'notification'],
  /** @lends module:repository  */
  function(io, $, Backbone, _, notification) {
    var repository = function(Repository) {

      this.socket = io.connect('http://localhost:4000', {
        'reconnect': true,
        'reconnection delay': 2000,
        'force new connection': true
      });

      this.socket.on('error', function(reason) {
        var html = '<div id="overlay" class="overlay"><div class="modal"><p>Waiting for connection to the SocketIO Server<br>(this may take a few seconds)</p><p>You may click to continue, but certain things will not work until the connection has been established.</p></div></div>';
        notification.addOverlay(html);
        this.socket.reconnect();
      });

      this.socket.on('connect', function() {
        $(".overlay").remove();
        notification.addMessage("SocketIO connected");
      });
      
      this.socket.on('disconnect', function() {
          console.log('Got disconnect!');        
          var html = '<div id="overlay" class="overlay"><div class="modal"><p>Connection Lost<br>Waiting for connection to the SocketIO Server</p></div></div>';
          notification.addOverlay(html);
          this.socket.reconnect();
       });

      /**
       * Retrieves available services for the given taskType over the socket
       * @param {string} taskType - The type of the task, e.g. HCT or Divide
       * @returns {Promise} jQuery Promise
       */
      this.getService = function(taskType) {
        var d = $.Deferred();
        this.socket.emit("getServices", taskType, function(data) {
          d.resolve(JSON.parse(data));
        });
        return d.promise();
      };

      /**
       * Retrieves available portals
       * @returns {Promise} jQuery Promise
       */
      this.getPortals = function() {
        var d = $.Deferred();
        this.socket.emit("getPortals", function(data) {
          d.resolve(JSON.parse(data));
        });
        return d.promise();
      };

      /**
       * Retrieves availble sample processes defined by osgi packages
       * @returns {Promise} jQuery Promise
       */
      this.getProcesses = function() {
        var d = $.Deferred();
        this.socket.emit("getProcesses", function(data) {
          d.resolve(JSON.parse(data));
        });
        return d.promise();
      };

      /**
       * Retrieves available services for the given taskType over the socket
       * @param {string} bundleId - The osgi bundle id containing the resource
       * @param {string} Resource - The resource location and name
       * @returns {Promise} jQuery Promise
       */
      this.loadProcess = function(bundleId, Resource) {
        var d = $.Deferred();
        this.socket.emit("loadProcess", bundleId + "," + Resource, function(data) {
          d.resolve(data);
        });
        return d.promise();
      };

      /**
       * Runs the given process
       * @param {string} json - Json String of the process
       */
      this.runProcess = function(json) {
        this.socket.emit("runProcess", json);
      };

      /**
       * Retrieves the link for the requested file
       * @param {string} fileName - The name of the file as defined in the service configuration
       * @param {string} recombinationBranchIndex - The recombination branch index list
       * @param {string} processId - the socketIO connectionId identifying the process execution
       * @returns {Promise} jQuery Promise
       */
      this.getFile = function(fileName, recombinationBranchIndex, processId) {
        var d = $.Deferred();
        this.socket.emit("getFileLink", [fileName, recombinationBranchIndex, processId].join(";"), function(data) {
          d.resolve(data);
        });
        return d.promise();
      };
    };

    var socketIORepo = new repository();
    _.extend(socketIORepo, Backbone.Events);

    socketIORepo.socket.on('ProcessEvent', function(data) {
      var json = JSON.parse(data);
      socketIORepo.trigger(json.eventType, json);
    });

    return /** @alias module:repository */ socketIORepo;
  });
