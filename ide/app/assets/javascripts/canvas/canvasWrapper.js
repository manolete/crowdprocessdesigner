/**
 * Module containing a singleton wrapper for fabricJS canvas
 * @module canvas/canvasWrapper
 */
define(["../fabric", "./configuration", "fabricjsViewport", "underscore", "backbone"],
  /** @lends module:canvas/canvasWrapper  */
  function(fabric, Configuration, Viewport, _, Backbone) {

    var instance = null;

    /**
     * @class
     */
    function CanvasWrapper() {
      if (instance !== null) {
        throw new Error("CanvasWrapper already instantiated");
      }
      this.initialize();
    }

    CanvasWrapper.prototype = {
      /** initialize a new canvas with viewport and add doubleclick handler */
      initialize: function() {
        this.canvas = new fabric.CanvasWithViewport('c', {
          selection: false
        });
        this.canvas.on('object:moving', this.onChange);
        this.canvas.on('mouse:up', this.tryConnect);

        fabric.Object.prototype.originX = fabric.Object.prototype.originY = 'center';

        fabric.util.addListener(fabric.document, 'dblclick',
          this.dblClickHandler);

        this.idCounter = 0;

        _.extend(this, Backbone.Events);
      },
      /** set the size of the canvas */
      setSize: function(width, height) {
        var canvasEl = document.getElementById('c');
        canvasEl.width = width;
        canvasEl.height = height;
        this.canvas.setWidth(width);
        this.canvas.setHeight(height);
        this.canvas.calcOffset();
      },
      /** returns the current canvas */
      getCanvas: function() {
        return this.canvas;
      },
      /** removes the currently selected canvas element */
      removeCurrentCanvasElement: function() {
        var currentElement = this.canvas.getActiveObject();
        if (currentElement) {
          currentElement.remove(this.canvas);
          if (currentElement.connectedTo) {
            currentElement.connectedTo.disconnect();
          }

          if (currentElement.inputConnector) {
            currentElement.inputConnector.disconnect();
            currentElement.outputConnector.disconnect();
          }
        }
      },
      /** onchange handler method */
      onChange: function(options) {
        var object = options.target;
        object.moveTo(options.target.left, options.target.top);
        instance.canvas.renderAll();

        instance.canvas
          .forEachObject(function(obj) {
            if (obj === options.target) {
              return;
            }

            if (_.contains(Configuration.getTaskTypes(), obj.type)) {
              var isNotConnected = (obj.connectedTo === null || !_.contains(obj.connectedTo, object));

              var intersectsInput = object.type === Configuration.Triangle.type && instance.customIntersectsWith(obj.inputConnector, object) || instance.customIntersectsWith(
                obj.innerInputConnector,
                object);
              var intersectsOutput = object.type === Configuration.Circle.type && instance.customIntersectsWith(
                obj.outputConnector, object) || instance.customIntersectsWith(obj.innerOutputConnector, object);

              if (isNotConnected && (intersectsInput || intersectsOutput)) {
                obj.setOpacity(0.5);
              } else if (!(intersectsInput || intersectsOutput)) {
                obj.setOpacity(1);
              }
            }
          });
      },
      /** tries to connect the currently selected method if it intersects with a connector of another object */
      tryConnect: function(options) {
        var activeObject = options.target ? options.target : instance.canvas.getActiveObject();

        instance.canvas.forEachObject(function(obj) {
          obj.setOpacity(1);
          if (obj === activeObject || activeObject === null) {
            return;
          }

          var isNotConnected, areConnected;
          if (activeObject.type === Configuration.Triangle.type) {
            if (obj.inputConnector) {
              isNotConnected = (obj.inputConnector.connectedTo === null ||
                !_.contains(obj.inputConnector.connectedTo, activeObject));

              areConnected = obj.inputConnector.connectedTo === activeObject ||
                _.contains(obj.inputConnector.connectedTo, activeObject);

              if (isNotConnected && instance.customIntersectsWith(obj.inputConnector, activeObject)) {
                obj.inputConnector.connectTo(activeObject, false);
              } else if (areConnected && !instance.customIntersectsWith(obj.inputConnector, activeObject)) {
                obj.inputConnector.disconnect(activeObject, false);
              }
            }
            if (obj.innerInputConnector) {
              isNotConnected = (obj.innerInputConnector.connectedTo === null ||
                !_.contains(obj.innerInputConnector.connectedTo, activeObject));

              areConnected = obj.innerInputConnector.connectedTo === activeObject ||
                _.contains(obj.innerInputConnector.connectedTo, activeObject);

              if (isNotConnected && instance
                .customIntersectsWith(obj.innerInputConnector, activeObject)) {
                obj.innerInputConnector.connectTo(activeObject, true);
              } else if (areConnected && !instance.customIntersectsWith(obj.innerInputConnector, activeObject)) {
                obj.innerInputConnector.disconnect(activeObject, true);
              }
            }
          } else if (activeObject.type === Configuration.Circle.type) {
            if (obj.outputConnector) {
              isNotConnected = (obj.outputConnector.connectedTo === null ||
                !_.contains(obj.outputConnector.connectedTo, activeObject));

              areConnected = obj.outputConnector.connectedTo === activeObject ||
                _.contains(obj.outputConnector.connectedTo, activeObject);

              if (isNotConnected && instance.customIntersectsWith(obj.outputConnector, activeObject)) {
                obj.outputConnector.connectTo(activeObject, false);
              } else if (areConnected && !instance.customIntersectsWith(obj.outputConnector, activeObject)) {
                obj.outputConnector.disconnect(activeObject, false);
              }
            }

            if (obj.innerOutputConnector) {
              isNotConnected = (obj.innerOutputConnector.connectedTo === null ||
                !_.contains(obj.innerOutputConnector.connectedTo, activeObject));

              areConnected = obj.innerOutputConnector.connectedTo === activeObject ||
                _.contains(obj.innerOutputConnector.connectedTo, activeObject);

              if (isNotConnected && instance.customIntersectsWith(obj.innerOutputConnector, activeObject)) {
                obj.innerOutputConnector.connectTo(activeObject, true);
              } else if (areConnected && !instance.customIntersectsWith(obj.innerOutputConnector, activeObject)) {
                obj.innerOutputConnector.disconnect(activeObject, true);
              }
            }
          }
        });
      },
      /** Returns a unique id for the current canvas */
      getUniqueId: function() {
        this.idCounter++;
        return this.idCounter;
      },
      /** Double click handler */
      dblClickHandler: function(e) {
        var object = instance.canvas.getActiveObject();
        if (object) {
          instance.trigger("canvasEditor:dblClick", object);
        }
      },
      /** FabricJS's intersectWith Method does not work well with group objects, therefore a custom method is needed
       * @param {MultipleConnector} connector - A MultipleConnector element
       * @param {DataFlow} flowObject - A DataFlow element
       */
      customIntersectsWith: function(connector, flowObject) {
        if (!(connector && flowObject)) {
          return false;
        }

        var rect = connector.getBoundingRect();
        var groupRect = connector.group.getBoundingRect();
        var rect3 = flowObject.getBoundingRect();

        var connectorTop = rect.top + groupRect.top + groupRect.height / 2;
        var connectorBottom = connectorTop + rect.height;
        var connectorLeftOutput = rect.left * connector.group.scaleX + groupRect.left + groupRect.width / 2;
        var connectorRightOutput = connectorLeftOutput + rect.width * connector.group.scaleX;
        var isInsideLeftOutput = rect3.left >= connectorLeftOutput;
        var isInsideRightOutput = rect3.left <= connectorRightOutput;
        var isInsideTop = rect3.top > connectorTop;
        var isInsideBottom = rect3.top < connectorBottom;

        return isInsideLeftOutput && isInsideRightOutput && isInsideTop && isInsideBottom;
      }
    };

    CanvasWrapper.getInstance = function() {
      if (instance === null) {
        instance = new CanvasWrapper();
      }
      return instance;
    };

    return CanvasWrapper.getInstance();
  });
