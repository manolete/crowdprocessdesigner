/**
 * Module providing custom fabricJS object for elements to be able to be connected to a DataFlow object
 * @module canvas/Element/Helper/MultipleConnector
 */
define(
  ["../../../fabric", "../../configuration", "underscore"],
  /** @lends module:canvas/Element/MultipleConnector  */
  function(fabric, Configuration, _) {
    /**
     * The fabricJS canvas element, connects DataFlows to itself
     * @class
     */
    var MultipleConnector = {
      /** fabricJS type, needed for serialization*/
      type: Configuration.MultipleConnector.type,
      /** Inits a canvas object at the given coordinates and the given options
       * @param {int} left - The position on the x-axis
       * @param {int} top - The position on the y-axis
       * @param {Object} options - FabricJS option parameters
       */
      initialize: function(left, top, options) {

        this.hasBorders = true;
        this.hasControls = false;
        this.connectedTo = [];

        var mergedOptions = _.extend({}, Configuration.MultipleConnector, options);

        mergedOptions.left = left;
        mergedOptions.top = top;

        this.callSuper('initialize', mergedOptions);
      },
      /** Needed for deserialization, returns null since it contains no information
      @returns null
      */
      toObject: function() {
        return null;
      },
      /** Add the element to the given canvas
       * @param {canvas}
       */
      addToCanvas: function(canvas) {
        canvas.add(this);
      },
      /** Moves the element and connected elements to the new position */
      moveTo: function(left, top) {
        _.each(this.connectedTo, function(obj) {
          // If this is the inputConnector, we move the connected object to the left
          var left = this.group.left + (this.left * this.group.scaleX);
          if (this === this.group.inputConnector) {
            left -= 10;
          }

          var offsetTop = this.group.symbol ? 10 : 0;

          obj.moveTo(left, this.group.top + offsetTop);
        }, this);
      },
      /** Connects the given element to itself
       * @param {CanvasObject} object - The fabricJS object to connect to
       * @param {bool} isInnerConnect - Whether it should connect to the inner connector (for Loop only)
       */
      connectTo: function(object, isInnerConnect) {
        this.connectedTo.push(object);

        if (!isInnerConnect) {
          // DataFlow Output
          if (object.type === Configuration.Triangle.type) {
            if (!object.line.connectedToIds) {
              object.line.connectedToIds = [];
            }
            if (!_.contains(object.line.connectedToIds, this.group.id)) {
              object.line.connectedToIds.push(this.group.id);
            }
          }

          // DataFlow Input
          if (object.type === Configuration.Circle.type) {
            if (!this.group.connectedToIds) {
              this.group.connectedToIds = [];
            }

            if (!_.contains(this.group.connectedToIds, object.line.id)) {
              this.group.connectedToIds.push(object.line.id);
            }
          }
        } else {
          // DataFlow Output
          if (object.type === Configuration.Triangle.type) {
            if (!object.line.innerConnectedToIds) {
              object.line.innerConnectedToIds = [];
            }
            if (!_.contains(object.line.innerConnectedToIds, this.group.id)) {
              object.line.innerConnectedToIds.push(this.group.id);
            }
          }

          // DataFlow Input
          if (object.type === Configuration.Circle.type) {
            if (!this.group.innerConnectedToIds) {
              this.group.innerConnectedToIds = [];
            }

            if (!_.contains(this.group.innerConnectedToIds, object.line.id)) {
              this.group.innerConnectedToIds.push(object.line.id);
            }
          }
        }

        // Circle or Triangle Object
        object.connectedTo = this;
      },
      /** Disconnect from given canvasObject
       * @param {CanvasObject} object - The fabricJS object to disconnect from
       */
      disconnect: function(object) {
        if (object && object.connectedTo && object.connectedTo === Configuration.Circle.type) {
          this.group.connectedToIds = _.without(this.group.connectedToIds, object.line.id);
        } else if (object && object.connectedTo && object.connectedTo === Configuration.Triangle.type) {
          this.connectedTo.line.connectedToId = null;
        }
        if (object) {
          object.connectedTo = null;
          this.connectedTo = _.without(this.connectedTo, object);
        }
      }
    };

    fabric.MultipleConnector = fabric.util.createClass(fabric.Rect, MultipleConnector);

    return fabric.MultipleConnector;
  });
