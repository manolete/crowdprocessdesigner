/**
 * Module providing custom Circle fabricJS object for module {@link module: canvas/Element/DataFlow}
 * @module canvas/Element/Helper/ConnectorCircle
 */
define(["../../../fabric", "../../configuration", "underscore"],
  /** @lends module:canvas/Element/ConnectorCircle  */
  function(fabric, Configuration, _) {
    /**
     * The fabricJS canvas element, callable via this module or fabric.ConnectorCircle
     * @class
     */
    var ConnectorCircle = {
      /** fabricJS type, needed for serialization*/
      type: Configuration.Circle.type,
      /** Inits a canvas object at the given coordinates and the given options
       * @param {int} left - The position on the x-axis
       * @param {int} top - The position on the y-axis
       * @param {Object} options - FabricJS option parameters
       */
      initialize: function(left, top, line, options) {

        this.hasControls = false;

        this.line = line;
        this.connectedTo = null;

        var mergedOptions = _.extend({}, Configuration.Circle, options);

        mergedOptions.left = left;
        mergedOptions.top = top;

        this.callSuper('initialize', mergedOptions);
      },
      toObject: function() {
        return null;
      },
      addToCanvas: function(canvas) {
        canvas.add(this);
      },
      moveTo: function(left, top) {
        this.set({
          'left': left,
          'top': top
        }).setCoords();
        this.line.set({
          'x1': left,
          'y1': top
        }).setCoords();
        this.line.endPoint.updateAngle();
      },
      remove: function(canvas) {
        canvas.remove(this.line);
        canvas.remove(this);
        canvas.remove(this.line.endPoint);
      }
    };

    fabric.ConnectorCircle = fabric.util.createClass(fabric.Circle, ConnectorCircle);

    return fabric.ConnectorCircle;
  });
