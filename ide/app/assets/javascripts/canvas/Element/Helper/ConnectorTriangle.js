/**
 * Module providing custom Triangle fabricJS object for module {@link module: canvas/Element/DataFlow}
 * @module canvas/Element/Helper/ConnectorTriangle
 */
define(["../../../fabric", "../../configuration", "underscore"],
  /** @lends module:canvas/Element/ConnectorTriangle  */
  function(fabric, Configuration, _) {
    /**
     * The fabricJS canvas element, callable via this module or fabric.ConnectorTriangle
     * @class
     */
    var ConnectorTriangle = {
      /** fabricJS type, needed for serialization*/
      type: Configuration.Circle.type,
      /** Inits a canvas object at the given coordinates and the given options
       * @param {int} left - The position on the x-axis
       * @param {int} top - The position on the y-axis
       * @param {Object} options - FabricJS option parameters
       */
      initialize: function(left, top, line, options) {
        this.hasBorders = false;
        this.hasControls = false;

        this.line = line;
        this.connectedTo = null;

        var mergedOptions = _.extend({}, Configuration.Triangle, options);

        mergedOptions.left = left;
        mergedOptions.top = top;

        this.callSuper('initialize', mergedOptions);
      },
      toObject: function() {
        return null;
      },
      addToCanvas: function(canvas) {
        canvas.add(this);
      },
      moveTo: function(left, top) {
        this.set({
          'left': left,
          'top': top
        }).setCoords();
        this.line.set({
          'x2': left,
          'y2': top
        }).setCoords();
        this.updateAngle();
      },
      /** Updates the angle of the arrow */
      updateAngle: function() {
        var xdiff = this.line.x2 - this.line.x1;
        var ydiff = this.line.y2 - this.line.y1;
        var angle = Math.atan2(ydiff, xdiff);
        angle *= 180 / Math.PI;
        this.set({
          'angle': angle + 90
        }).setCoords();
      },
      remove: function(canvas) {
        canvas.remove(this);
        canvas.remove(this.line);
        canvas.remove(this.line.startPoint);
      }
    };

    fabric.ConnectorTriangle = fabric.util.createClass(fabric.Triangle, ConnectorTriangle);

    return fabric.ConnectorTriangle;
  });
