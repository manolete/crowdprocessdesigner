/**
 * Module providing basic methods for other custom fabricJS elements such as MCT
 * @module canvas/Element/AbstractElement
 */
define([],
  /** @lends module:canvas/Element/AbstractElement  */
  function() {
    /**
     * @class
     */
    var AbstractElement = {
      /** Serializes the object and the needed properties, needed by fabricJS */
      toObject: function() {
        return fabric.util.object.extend(this.callSuper('toObject'), {
          label: this.text.text,
          id: this.id,
          connectedToIds: this.connectedToIds,
          services: this.services
        });
      },
      /** Add the element to the given canvas
      * @param {canvas}
      */
      addToCanvas: function(canvas) {
        canvas.add(this);
      },
      /** Moves the element and connected elements to the new position */
      moveTo: function(left, top) {
        this.set({
          'left': left,
          'top': top
        }).setCoords();
        this.inputConnector.moveTo();
        this.outputConnector.moveTo();
      },
      /** Removes the element from the given canvas
      * @param {canvas}
      */
      remove: function(canvas) {
        canvas.remove(this);
      },
      /** Updates the element manually */
      update: function() {
        if (this.services && this.services.length > 1 && this.recombinationText) {
          this.recombinationText.setText(this.services.length + "⇉");
        } else if (this.recombinationText) {
          this.recombinationText.setText("");
        }
      }
    };

    return /** @alias module:canvas/Element/AbstractElement */AbstractElement;
  });
