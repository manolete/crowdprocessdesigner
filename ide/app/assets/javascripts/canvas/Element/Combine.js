/**
 * Module registering a custom Combine fabricJS group object
 * @module canvas/Element/Combine
 */
define(["../../fabric", "./Helper/MultipleConnector", "../configuration", "underscore", "./AbstractElement"],
  /** @lends module:canvas/Element/Combine  */
  function(fabric, MultipleConnector, Configuration, _, abstractElement) {
    /**
     * The fabricJS canvas element, callable via this module or fabric.Combine
     * @class
     */
    var Combine = {
      /** fabricJS type, needed for serialization*/
      type: Configuration.Combine.type,
      /** Inits a canvas object at the given coordinates and the given options
       * @param {int} left - The position on the x-axis
       * @param {int} top - The position on the y-axis
       * @param {Object} options - FabricJS option parameters
       */
      initialize: function(left, top, options) {
        this.hasBorders = false;
        this.hasControls = false;

        var offsetX = Configuration.Combine.radius + 3;

        this.blockElement = new fabric.Circle(Configuration.Combine);
        this.inputConnector = new MultipleConnector(-offsetX, 0);
        this.outputConnector = new MultipleConnector(offsetX, 0);
        this.text = new fabric.Text(options.label ? options.label : "", Configuration.TextAboveElement);
        this.symbol = new fabric.Text(Configuration[this.type].label, Configuration.Text);
        this.recombinationText = new fabric.Text("", Configuration.RecombinationText);

        _.extend(this, abstractElement);

        var objects = [this.blockElement, this.inputConnector, this.outputConnector, this.text, this.recombinationText, this.symbol];
        var mergedOptions = _.extend({left: left, top: top}, Configuration[this.type], options);
        this.callSuper('initialize', objects, mergedOptions);
      }
    };

    fabric.Combine = fabric.util.createClass(fabric.Group, Combine);

    return fabric.Combine;
  });
