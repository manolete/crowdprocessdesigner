/**
 * Module registering a custom MCT fabricJS group object
 * @module canvas/Element/MCT
 */
define(["../../fabric", "./Helper/MultipleConnector", "../configuration", "underscore", "./AbstractElement"],
  /** @lends module:canvas/Element/MCT  */
  function(fabric, MultipleConnector, Configuration, _, abstractElement) {
    /**
     * The fabricJS canvas element, callable via this module or fabric.MCT
     * @class
     */
    var MCT = {
      /** fabricJS type, needed for serialization*/
      type: Configuration.MCT.type,
      /** Inits a canvas object at the given coordinates and the given options
       * @param {int} left - The position on the x-axis
       * @param {int} top - The position on the y-axis
       * @param {Object} options - FabricJS option parameters
       */
      initialize: function(left, top, options) {
        this.hasBorders = false;
        this.hasControls = false;

        var mergedOptions = _.extend({left: left, top: top}, Configuration.MCT, options);

        var offsetX = Configuration.MCT.width / 2 + 3;

        this.blockElement = new fabric.Rect(Configuration.MCT);
        this.inputConnector = new MultipleConnector(-offsetX, 0, Configuration.MultipleConnector);
        this.outputConnector = new MultipleConnector(offsetX, 0, Configuration.MultipleConnector);
        this.text = new fabric.Text(mergedOptions.label, Configuration.Text);
        this.recombinationText = new fabric.Text("", Configuration.RecombinationText);

        _.extend(this, abstractElement);

        var objects = [this.blockElement, this.inputConnector, this.outputConnector, this.text, this.text, this.recombinationText];
        this.callSuper('initialize', objects, mergedOptions);
      }
    };

    fabric.MCT = fabric.util.createClass(fabric.Group, MCT);

    return fabric.MCT;
  });
