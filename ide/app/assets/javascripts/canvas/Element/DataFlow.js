/**
 * Module registering a custom fabricJS DataFlow object
 * @module canvas/Element/DataFlow
 */
define(["../../fabric", "./Helper/ConnectorCircle", "./Helper/ConnectorTriangle", "../configuration", "underscore"],
  /** @lends module:canvas/Element/DataFlow  */
  function(fabric, Circle, Triangle, Configuration, _) {

    /**
     * The fabricJS canvas element, callable via this module or fabric.DataFlow
     * @class
     */
    var DataFlow = {
      /** fabricJS type, needed for serialization*/
      type: Configuration.DataFlow.type,
      /** Inits a canvas object at the given coordinates and the given options
       * @param {int} left - The position on the x-axis
       * @param {int} top - The position on the y-axis
       * @param {Object} options - FabricJS option parameters
       */
      initialize: function(left, top, options) {
        var mergedOptions = _.extend({}, Configuration.DataFlow, options);

        this.hasControls = false;

        // For deserialization we need to position the circle either from the options or from the given options object
        if (mergedOptions.x1) {
          this.x1 = left + mergedOptions.x1;
          this.x2 = left + mergedOptions.x2;
          this.y1 = top + mergedOptions.y1;
          this.y2 = top + mergedOptions.y2;
        } else {
          var defaultLength = Configuration.DataFlow.defaultLength;
          this.x1 = left - defaultLength / 2;
          this.y1 = top;
          this.x2 = left + defaultLength / 2;
          this.y2 = top;
        }

        this.startPoint = new Circle(this.x1, this.y1, this);
        this.endPoint = new Triangle(this.x2, this.y2, this);
        this.endPoint.updateAngle();

        var coordinates = [this.x1, this.y1, this.x2, this.y2];
        this.callSuper('initialize', coordinates, mergedOptions);
      },
      /** Returns the object containing all deserialization information */
      toObject: function() {
        return fabric.util.object.extend(this.callSuper('toObject'), {
          id: this.id,
          connectedToIds: this.connectedToIds,
          innerConnectedToIds: this.innerConnectedToIds
        });
      },
      /** Adds the element to the given canvas
      * @param {Canvas} canvas - The canvas to add the element to
      */
      addToCanvas: function(canvas) {
        canvas.add(this);
        this.startPoint.addToCanvas(canvas);
        this.endPoint.addToCanvas(canvas);
      },
      /** Moves the element to the given coordinates, does not connect/disconnect the DataFlow
      * @param {int} left
      * @param {int} top
      */
      moveTo: function(left, top) {
        var diffx = this.x2 - this.x1;
        var diffy = this.y2 - this.y1;

        var newX1 = left - diffx / 2;
        var newY1 = top - diffy / 2;
        var newX2 = left + diffx / 2;
        var newY2 = top + diffy / 2;

        this.startPoint.set({
          'left': newX1,
          'top': newY1
        }).setCoords();
        this.endPoint.set({
          'left': newX2,
          'top': newY2
        }).setCoords();
        this.set("x1", newX1);
        this.set("x2", newX2);
        this.set("y1", newY1);
        this.set("y2", newY2);
        this.setCoords();
      },
      /** Removes this element from the given canvas */
      remove: function(canvas) {
        canvas.remove(this.startPoint);
        canvas.remove(this);
        canvas.remove(this.endPoint);
      }
    };

    fabric.DataFlow = fabric.util.createClass(fabric.Line, DataFlow);

    return fabric.DataFlow;
  });
