/**
 * Module registering a custom Process fabricJS group object
 * @module canvas/Element/Process
 */
define(["../../fabric", "./Helper/MultipleConnector", "../configuration", "underscore", "./AbstractElement"],
  /** @lends module:canvas/Element/Process  */
  function(fabric, MultipleConnector, Configuration, _, abstractElement) {
    /**
     * The fabricJS canvas element, callable via this module or fabric.Process
     * @class
     */
    var Process = {
      /** fabricJS type, needed for serialization*/
      type: Configuration.MCT.type,
      /** Inits a canvas object at the given coordinates and the given options
       * @param {int} left - The position on the x-axis
       * @param {int} top - The position on the y-axis
       * @param {Object} options - FabricJS option parameters
       */
      initialize: function(left, top, options) {
        this.hasBorders = false;
        this.hasControls = false;

        var mergedOptions = _.extend({left: left, top: top}, Configuration.Process, options);

        var offsetX = Configuration.Process.width / 2 + 3;

        var rectOptions = _.extend(Configuration.Process, {
          angle: 45
        });
        this.blockElement = new fabric.Rect(rectOptions);
        this.inputConnector = new MultipleConnector(-offsetX, 0, Configuration.MultipleConnector);
        this.outputConnector = new MultipleConnector(offsetX, 0, Configuration.MultipleConnector);
        this.text = new fabric.Text(mergedOptions.label, Configuration.Text);
        this.recombinationText = new fabric.Text("", Configuration.RecombinationText);

        _.extend(this, abstractElement);

        var objects = [this.blockElement, this.inputConnector, this.outputConnector, this.text, this.text, this.recombinationText];
        this.callSuper('initialize', objects, mergedOptions);
      }
    };

    fabric.Process = fabric.util.createClass(fabric.Group, Process);

    return fabric.Process;
  });
