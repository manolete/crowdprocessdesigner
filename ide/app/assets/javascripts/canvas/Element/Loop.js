/**
 * Module registering a custom Loop fabricJS group object
 * @module canvas/Element/Loop
 */
define(["../../fabric", "../configuration", "./Helper/MultipleConnector", "underscore"],
  /** @lends module:canvas/Element/Loop  */
  function(fabric, Configuration, MultipleConnector, _) {
    /**
     * The fabricJS canvas element, callable via this module or fabric.Loop
     * @class
     */
    var Loop = {
      /** fabricJS type, needed for serialization*/
      type: Configuration.Loop.type,
      /** Inits a canvas object at the given coordinates and the given options
       * @param {int} left - The position on the x-axis
       * @param {int} top - The position on the y-axis
       * @param {Object} options - FabricJS option parameters
       */
      initialize: function(left, top, options) {
        this.hasBorders = false;
        this.hasControls = true;
        this.lockRotation = true;
        this.lockScalingX = false;
        this.lockScalingY = false;

        var mergedOptions = _.extend({}, Configuration.Loop, options);
        mergedOptions.left = left;
        mergedOptions.top = top;

        var offsetX = Configuration.Loop.width / 2 + 3;
        var offsetY = Configuration.Loop.height / 2 + 3;
        this.blockElement = new fabric.Rect(Configuration.Loop);
        this.symbol = new fabric.Text(Configuration.Loop.label, Configuration.Text);
        this.symbol.set({
          'left': offsetX - Configuration.Text.fontSize,
          'top': offsetY - Configuration.Text.fontSize
        });
        var textConfig = _.extend({}, Configuration.Text, {
          left: 0,
          top: -offsetY - 5
        });
        this.text = new fabric.Text(options.label ? options.label : "", textConfig);

        var halfConnectorWidth = Configuration.MultipleConnector.width / 2;

        this.inputConnector = new MultipleConnector(-(offsetX + halfConnectorWidth), 0, Configuration.MultipleConnectorLoop);
        this.outputConnector = new MultipleConnector(offsetX + halfConnectorWidth, 0, Configuration.MultipleConnectorLoop);

        this.innerInputConnector = new MultipleConnector(offsetX - halfConnectorWidth, 0, Configuration.MultipleConnectorLoop);
        this.innerOutputConnector = new MultipleConnector(-(offsetX - halfConnectorWidth), 0, Configuration.MultipleConnectorLoop);

        var objects = [this.blockElement, this.text, this.symbol, this.inputConnector, this.outputConnector, this.innerInputConnector, this.innerOutputConnector];
        this.callSuper('initialize', objects, mergedOptions);
      },
      /** Serializes the object and the needed properties, needed by fabricJS */
      toObject: function() {
        return fabric.util.object.extend(this.callSuper('toObject'), {
          id: this.id,
          connectedToIds: this.connectedToIds,
          innerConnectedToIds: this.innerConnectedToIds,
          services: this.services,
          label: this.text.text
        });
      },
      /** Add the element to the given canvas
       * @param {canvas}
       */
      addToCanvas: function(canvas) {
        canvas.add(this);
      },
      /** Moves the element and connected elements to the new position */
      moveTo: function(left, top) {
        this.set({
          'left': left,
          'top': top
        }).setCoords();
        this.inputConnector.moveTo();
        this.outputConnector.moveTo();
        this.innerInputConnector.moveTo();
        this.innerOutputConnector.moveTo();
        this.sendToBack();
      },
      /** Removes the element from the given canvas
       * @param {canvas}
       */
      remove: function(canvas) {
        canvas.remove(this);
      }
    };

    fabric.Loop = fabric.util.createClass(fabric.Group, Loop);

    return fabric.Loop;
  });
