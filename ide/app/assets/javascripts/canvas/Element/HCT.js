/**
 * Module registering a custom HCT fabricJS group object
 * @module canvas/Element/HCT
 */
define(["../../fabric", "../configuration", "./Helper/MultipleConnector", "underscore", "./AbstractElement", "./AbstractElement"],
  /** @lends module:canvas/Element/HCT  */
  function(fabric, Configuration, MultipleConnector, _, abstractElement) {
    /**
     * The fabricJS canvas element, callable via this module or fabric.HCT
     * @class
     */
     var HCT = {
       /** fabricJS type, needed for serialization*/
       type: Configuration.HCT.type,
       /** Inits a canvas object at the given coordinates and the given options
        * @param {int} left - The position on the x-axis
        * @param {int} top - The position on the y-axis
        * @param {Object} options - FabricJS option parameters
        */
       initialize: function(left, top, options) {
         this.hasBorders = false;
         this.hasControls = false;

         var mergedOptions = _.extend({left: left, top: top}, Configuration.HCT, options);

         var offsetX = Configuration.HCT.width / 2 + 3;

         this.blockElement = new fabric.Rect(Configuration.HCT);
         this.inputConnector = new MultipleConnector(-offsetX, 0, Configuration.MultipleConnector);
         this.outputConnector = new MultipleConnector(offsetX, 0, Configuration.MultipleConnector);
         this.text = new fabric.Text(mergedOptions.label, Configuration.Text);
         this.recombinationText = new fabric.Text("", Configuration.RecombinationText);

         _.extend(this, abstractElement);

         var objects = [this.blockElement, this.inputConnector, this.outputConnector, this.text, this.recombinationText, this.text, this.recombinationText];
         this.callSuper('initialize', objects, mergedOptions);
       }
     };

    fabric.HCT = fabric.util.createClass(fabric.Group, HCT);

    return fabric.HCT;
  });
