/**
 * Module registering a custom File fabricJS group object
 * @module canvas/Element/File
 */
define(["../../fabric", "./Helper/MultipleConnector", "../configuration", "underscore", "./AbstractElement"],
  /** @lends module:canvas/Element/File  */
  function(fabric, MultipleConnector, Configuration, _, abstractElement) {
    /**
     * The fabricJS canvas element, callable via this module or fabric.File
     * @class
     */
     var File = {
       /** fabricJS type, needed for serialization*/
       type: Configuration.File.type,
       /** Inits a canvas object at the given coordinates and the given options
        * @param {int} left - The position on the x-axis
        * @param {int} top - The position on the y-axis
        * @param {Object} options - FabricJS option parameters
        */
       initialize: function(left, top, options, img) {
         this.hasBorders = false;
         this.hasControls = false;

        var offsetX = Configuration.File.width / 2 + 3;

         this.symbol = new fabric.Image(document.getElementById(this.type));
         this.blockElement = new fabric.Rect(Configuration.File);
         this.inputConnector = new MultipleConnector(-offsetX, 0, Configuration.MultipleConnector);
         this.outputConnector = new MultipleConnector(offsetX, 0, Configuration.MultipleConnector);
         this.recombinationText = new fabric.Text("", Configuration.RecombinationText);
         this.text = new fabric.Text(options.label ? options.label : Configuration.File.label, Configuration.TextAboveElement);

         _.extend(this, abstractElement);

         var objects = [this.blockElement, this.inputConnector, this.outputConnector, this.symbol, this.text, this.recombinationText];
         var mergedOptions = _.extend({left: left, top: top}, Configuration[this.type], options);
         this.callSuper('initialize', objects, mergedOptions);
       }
     };

    fabric.File = fabric.util.createClass(fabric.Group, File);

    return fabric.File;
  });
