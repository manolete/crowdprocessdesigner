/**
 * Module listening to {@link module:dispatcher} to update the process state on the canvas
 * @module canvas/canvasDebugger
 */
define(['backbone', 'underscore', "dispatcher", './editor'],
/** @lends module:canvas/canvasDebugger  */
  function(Backbone, _, dispatcher, editor) {

    var canvasDebugger = _.extend({}, Backbone.Events);

		/** Grey background on Task Start */
    canvasDebugger.listenTo(dispatcher, 'TaskStart', function(event) {
      var obj = editor.getObjectById(event.taskObjectId);
      obj.blockElement.fill = "grey";
      editor.updateCanvas();
    });

		/** Green background on Task Finished */
    canvasDebugger.listenTo(dispatcher, 'TaskFinished', function(event) {
      var obj = editor.getObjectById(event.taskObjectId);
      obj.blockElement.fill = "green";
      editor.updateCanvas();
    });

		/** Red background on Task Failed */
    canvasDebugger.listenTo(dispatcher, 'TaskFailed', function(event) {
      var obj = editor.getObjectById(event.taskObjectId);
      if (obj && obj.blockElement) {
        obj.blockElement.fill = "red";
        editor.updateCanvas();
      }
    });

    return /** @alias module:canvas/canvasDebugger */canvasDebugger;
  });
