/**
 * Module providing the main entry point for canvas related stuff
 * @module canvas/editor
 */
define(["./canvasWrapper", "./dragAndDrop", "./keyStrokes", "underscore"],
  /** @lends module:canvas/editor  */
  function(canvasWrapper, DragAndDrop, KeyStrokes, _) {
    return {
      /** Initializes the canvas and loading required modules */
      setup: function() {
        /** set canvas to max window size */
        canvasWrapper.setSize(window.innerWidth, window.innerHeight);
        canvasWrapper.canvas.renderAll();
        canvasWrapper.canvas.calcOffset();
        this.loadCanvasElements();
        // Attach canvas debugger for event handling
        require(["./canvas/canvasDebugger"]);
        // Register drag and drop
        DragAndDrop();
        // register keystrokes
        KeyStrokes();
      },
      /** Gets the canvas element for the given id
       * @param {int} elementId - The id of the desired canvas object
       * @returns {CanvasElement}
       */
      getObjectById: function(elementId) {
        var object = _.find(canvasWrapper.canvas._objects,
          function(d) {
            return d.id === elementId;
          });
        return object;
      },
      /** Renders the updated canvas */
      updateCanvas: function() {
        canvasWrapper.canvas.renderAll();
      },
      /** Returns the fabricJS canvas
       * @returns {fabricJsCanvas}
       */
      getCanvas: function() {
        return canvasWrapper.canvas;
      },
      /** Loads the custom editor elements which attach themselves to fabricJS
       * These are lazy loaded to avoid circular dependencies
       */
      loadCanvasElements: function() {
        require(["./canvas/Element/DataFlow"]);
        require(["./canvas/Element/Divide"]);
        require(["./canvas/Element/File"]);
        require(["./canvas/Element/HCT"]);
        require(["./canvas/Element/IfElse"]);
        require(["./canvas/Element/Loop"]);
        require(["./canvas/Element/MCT"]);
        require(["./canvas/Element/Split"]);
        require(["./canvas/Element/Union"]);
        require(["./canvas/Element/Process"]);
        require(["./canvas/Element/Combine"]);
      }
    };
  });
