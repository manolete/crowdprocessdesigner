/**
 * Module providing serializing methods for the canvas
 * @module canvas/serializer
 */
define(["./canvasWrapper", "./configuration", "underscore"],
  /** @lends module:canvas/serializer */
  function(canvasWrapper, Configuration, _) {
    return {
      /** serializes the current canvas
       * @param {string} localStorageKey - The key to store the canvas in localStorage
       * @returns {json} The stringified version of the canvas
       */
      serialize: function(localStorageKey) {
        var json = JSON.stringify(canvasWrapper.canvas.toJSON(["portalServices"]));
        if (localStorageKey) {
          localStorage.setItem(localStorageKey, json);
        }
        return json;
      },
      /** deserializes a canvas from localStorage
       * @param {string} localStorageKey - The key to load the canvas from localStorage
       */
      deserialize: function(localStorageKey) {
        var json = localStorage.getItem(localStorageKey);
        this.deserializeFromString(json);
      },
      connectCanvasElements: function() {
        var objects = canvasWrapper.canvas.getObjects();
        _.each(objects, function(object) {
          /** connect loop inner output */
          if (object.innerConnectedToIds) {
            var connectToObject = _.find(objects, function(x) {
                return _.contains(object.innerConnectedToIds, x.id);
            });
              if (connectToObject.type === Configuration.DataFlow.type) {
                object.innerOutputConnector.connectTo(connectToObject.startPoint);
              } else {
                connectToObject.innerInputConnector.connectTo(object.endPoint);
              }
          }

          /** connect normal task output */
          if (object.connectedToIds && object.connectedToIds.length > 0) {
            var connectToObjects = _.filter(objects, function(x) {
              return _.contains(object.connectedToIds, x.id);
            });
            if (connectToObjects[0].type === Configuration.DataFlow.type) {
              _.forEach(connectToObjects, function(x) {
                object.outputConnector.connectTo(x.startPoint);
              });
            } else {
              _.forEach(connectToObjects, function(x) {
                x.inputConnector.connectTo(object.endPoint);
              });
            }
          }
        });
      },
      /** deserializes a canvas from a given string
       * @param {string} json - The stringified version of the canvas
       */
      deserializeFromString: function(json) {
        if (!json) {
          return;
        }

        var jsonCanvas = JSON.parse(json);
        var canvas = canvasWrapper.canvas;
        canvas.clear();
        canvas.setZoom(1);

        canvasWrapper.canvas.portalServices = jsonCanvas.portalServices;

        /** Remove null elements (connectorElements are not serialized with null and recreated for each canvas object) */
        jsonCanvas.objects = _.without(jsonCanvas.objects, null);

        /** The max id of the taskObject to set the id counter for the object ids
         * (needed when objects are getting added after deserialization) */
        var maxId = _.max(jsonCanvas.objects, function(x) {
          return x.id;
        }).id;
        canvasWrapper.idCounter = isNaN(maxId) ? 0 : maxId;

        /** after all elements are added to the canvas, connect the elements with each other */
        var connectElements = _.after(jsonCanvas.objects.length, this.connectCanvasElements);

        /** Add elements  to the canvas */
        _.each(jsonCanvas.objects, function(object) {
          var canvasObjectConstructor = fabric[object.type];
          if (typeof canvasObjectConstructor === 'function') {
            var canvasElement = new canvasObjectConstructor(object.left, object.top, object);

            canvasElement.addToCanvas(canvas);
            /** call the after function which is only executed the last time in this _.each loop */
            connectElements();
            // update the element view
            if (canvasElement.update) {
              canvasElement.update();
            }
          }
        });
      }
    };
  });
