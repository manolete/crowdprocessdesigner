/**
 * Module registering keystrokes for the canvas
 * @module canvas/keyStrokes
 */
define(["./canvasWrapper", "jquery.mousewheel.min", "./serializer", "../notification"],
  /** @lends module:canvas/keyStrokes  */
  function(canvasWrapper, jqueryMousewheel, serializer, notification) {
    return function() {
      var keyCodes = {
        DELETE: 46,
        G: 71,
        S: 83,
        O: 79
      };

      document.onkeyup = function(e) {
        if (!e.ctrlKey) {
          canvasWrapper.canvas.isGrabMode = false;
        }
      };

      document.onkeydown = function(e) {
        if (e.keyCode === keyCodes.DELETE && document.activeElement === document.body) {
          canvasWrapper.removeCurrentCanvasElement();
        }

        if (e.ctrlKey) {
          canvasWrapper.canvas.isGrabMode = true;
        }

        if (e.keyCode === keyCodes.S && e.ctrlKey) {
          e.preventDefault();
          serializer.serialize("canvas");
          notification.addMessage("Process saved to local storage");
        }

        if (e.keyCode === keyCodes.O && e.ctrlKey) {
          e.preventDefault();
          $("#upload:hidden").trigger('click');
        }
      };

      $('#canvas-container')
        .mousewheel(
          function(event) {
            event.preventDefault();
            var zoomFactor = event.deltaY * (0.1);
            var newZoomLevel = canvasWrapper.canvas.viewport.zoom + zoomFactor;
            if (newZoomLevel > 0.2 && newZoomLevel < 4) {
              canvasWrapper.canvas.setZoom(newZoomLevel);
              canvasWrapper.canvas.calcOffset();
              canvasWrapper.canvas.renderAll();
            }
          });
    };
  });
