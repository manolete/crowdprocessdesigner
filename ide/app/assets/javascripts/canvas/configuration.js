/**
 * Module containing configuration parameters for canvas elements
 * @module canvas/configuration
 */
define(["underscore"],
  /** @lends module:canvas/configuration  */
  function(_) {
    var taskTypeOptions = {
      borderColor: ("black"),
      stroke: 'black',
      fill: 'transparent',
      originX: 'center',
      originY: 'center'
    };

    /** @namespace */
    var Configuration = {};

    /** HCT configuration parameters for module {@link class:HCT } */
    Configuration.HCT = {
      type: 'HCT',
      width: 100,
      height: 50,
      rx: 12,
      ry: 12,
      label: 'HCT'
    };
    _.extend(Configuration.HCT, taskTypeOptions);

    /** Process configuration parameters for module {@link module:canvas/Element/Process }*/
    Configuration.Process = {
      type: 'Process',
      width: 50,
      height: 50,
      label: 'Process'
    };
    _.extend(Configuration.Process, taskTypeOptions);

    /** Loop configuration parameters for module {@link module:canvas/Element/Loop } */
    Configuration.Loop = {
      type: 'Loop',
      width: 400,
      height: 200,
      rx: 12,
      ry: 12,
      selectable: true,
      label: '\u21BB'
    };
    _.extend(Configuration.Loop, taskTypeOptions);

    /** MCT configuration parameters for module {@link module:canvas/Element/MCT } */
    Configuration.MCT = {
      type: 'MCT',
      width: 100,
      height: 50,
      rx: 0,
      ry: 0,
      label: 'MCT'
    };
    _.extend(Configuration.MCT, taskTypeOptions);

    /** File configuration parameters for module {@link module:canvas/Element/File } */
    Configuration.File = {
      type: 'File',
      borderColor: "transparent",
      stroke: 'transparent',
      fill: 'transparent',
      label: 'File',
      width: 55,
      height: 65,
      rx: 0,
      ry: 0
    };

    /** DataFlow configuration parameters for module {@link module:canvas/Element/DataFlow } */
    Configuration.DataFlow = {
      fill: 'black',
      type: 'DataFlow',
      stroke: 'black',
      strokeWidth: 4,
      selectable: true,
      defaultLength: 100
    };

    /** Split configuration parameters for module {@link module:canvas/Element/Split } */
    Configuration.Split = {
      type: 'Split',
      selectable: true,
      radius: 20,
      label: '+'
    };
    _.extend(Configuration.Split, taskTypeOptions);

    /** Divide configuration parameters for module {@link module:canvas/Element/Divide } */
    Configuration.Divide = {
      type: 'Divide',
      selectable: true,
      radius: 20,
      label: '\u00F7'
    };
    _.extend(Configuration.Divide, taskTypeOptions);

    /** Combine configuration parameters for module {@link module:canvas/Element/Combine } */
    Configuration.Combine = {
      type: 'Combine',
      selectable: true,
      radius: 20,
      label: 'C'
    };
    _.extend(Configuration.Combine, taskTypeOptions);

    /** Union configuration parameters for module {@link module:canvas/Element/Union } */
    Configuration.Union = {
      type: 'Union',
      selectable: true,
      radius: 20,
      label: '\u222A'
    };
    _.extend(Configuration.Union, taskTypeOptions);

    /** IfElse configuration parameters for module {@link module:canvas/Element/IfElse } */
    Configuration.IfElse = {
      type: 'Divide',
      selectable: true,
      radius: 20,
      label: '?'
    };
    _.extend(Configuration.IfElse, taskTypeOptions);

    /** Circle configuration parameters for module {@link module:canvas/Element/ConnectorCircle } */
    Configuration.Circle = {
      strokeWidth: 1,
      radius: 4,
      fill: 'black',
      stroke: 'black',
      selectable: true,
      type: 'Helper/Circle'
    };

    /** Triangle configuration parameters for module {@link module:canvas/Element/Helper/ConnectorTriangle } */
    Configuration.Triangle = {
      borderColor: "black",
      type: 'Helper/Triangle',
      stroke: 'black',
      fill: 'black',
      width: 20,
      height: 20,
      angle: 90
    };

    var connectorOptions = {
      fill: 'transparent',
      width: 20,
      height: 50,
      selectable: false
    };

    /** MultipleConnector configuration parameters for module {@link module:canvas/Element/Helper/MultipleConnector } */
    Configuration.MultipleConnector = {
      type: 'Helper/MultipleConnector',
    };
    _.extend(Configuration.MultipleConnector, connectorOptions);

    /** MultipleConnectorLoop configuration parameters */
    Configuration.MultipleConnectorLoop = {
      height: 200
    };
    _.extend(Configuration.MultipleConnectorLoop, Configuration.MultipleConnector);

    /** Text configuration parameters */
    Configuration.Text = {
      fontSize: 10,
      left: 0,
      originX: 'center',
      originY: 'center'
    };

    /** RecombinationText configuration parameters */
    Configuration.RecombinationText = {
      left: -38,
      top: -18
    };
    _.extend(Configuration.RecombinationText, Configuration.Text);

    /** Operators (Circle Elements such as Divide) configuration parameters */
    Configuration.TextAboveElement = {
      left: -38,
      top: -38
    };
    _.extend(Configuration.TextAboveElement, Configuration.Text);

    /** Get all available task types
     * @returns {Array}
     */
    Configuration.getTaskTypes = function() {
      return [
        Configuration.HCT.type,
        Configuration.MCT.type,
        Configuration.File.type,
        Configuration.Split.type,
        Configuration.Divide.type,
        Configuration.Union.type,
        Configuration.Loop.type,
        Configuration.Process.type,
        Configuration.Combine.type,
        Configuration.IfElse.type
      ];
    };

    return /** @alias module:canvas/configuration */ Configuration;
  });
