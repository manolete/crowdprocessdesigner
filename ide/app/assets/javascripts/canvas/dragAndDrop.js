/**
 * Module providing event handling functions for drag and drop
 * @module canvas/dragAndDrop
 */
define(["../fabric", "./canvasWrapper"],
/** @lends module:canvas/dragAndDrop  */
function(fabric, canvasWrapper) {
  return function() {

    function handleDragStart(e) {
      [].forEach.call(images, function(img) {
        img.classList.remove('img_dragging');
      });
      this.classList.add('img_dragging');
    }

    function handleDragOver(e) {
      if (e.preventDefault) {
        e.preventDefault(); // Necessary. Allows us to drop.
      }
      e.dataTransfer.dropEffect = 'copy'; // See the section on the DataTransfer object.
      return false;
    }

    function handleDragEnter(e) {
      this.classList.add('over');
    }

    function handleDragLeave(e) {
      this.classList.remove('over');
    }

    function handleDrop(e) {
      e.preventDefault();
      if (e.stopPropagation) {
        e.stopPropagation(); // stops the browser from redirecting.
      }

      var img = document.querySelector('#canvas-element-container img.img_dragging');

      var elementConstructor = fabric[img.id];
      if (typeof elementConstructor === 'function') {
        var positionWithOffsetX = e.layerX - canvasWrapper.canvas.viewport.position.x;
        var positionWithOffsetY = e.layerY - canvasWrapper.canvas.viewport.position.y;
        var object = new elementConstructor(positionWithOffsetX, positionWithOffsetY, {
          id: canvasWrapper.getUniqueId()
        }, img);
        object.addToCanvas(canvasWrapper.canvas);
      } else {
        throw "unknown element type";
      }
      return false;
    }

    function handleDragEnd(e) {
      [].forEach.call(images, function(img) {
        img.classList.remove('img_dragging');
      });
    }

    /** Bind the event listeners for the image elements */
    var images = document.querySelectorAll('#canvas-element-container img');
    [].forEach.call(images, function(img) {
      img.addEventListener('dragstart', handleDragStart, false);
      img.addEventListener('dragend', handleDragEnd, false);
    });

    /** Bind the event listeners for the canvas*/
    var canvasContainer = document.getElementById('canvas-container');
    canvasContainer.addEventListener('dragenter', handleDragEnter, false);
    canvasContainer.addEventListener('dragover', handleDragOver, false);
    canvasContainer.addEventListener('dragleave', handleDragLeave, false);
    canvasContainer.addEventListener('drop', handleDrop, false);
  };
});
