importScripts('../require.js');
/**
 * Webworker module to calculate the tree based on the given logs
 * @module treeWebWorkers/logTree
 */
require({
    baseUrl: "../",
    paths: {
      underscore: '../lib/underscorejs/underscore',
    },
  }, ["require", "underscore"],
  /** @lends module:treeWebWorkers/logTree  */
  function(require, _) {
    /** Calculates the logs on message
    @param {Array} oEvent - the message event containing the logs
    */
    this.onmessage = function(oEvent) {
      var logs = oEvent.data;

      var relevantLogs = _.filter(logs, function(l) {
        return (l.eventType == "TaskStart" || l.eventType == "TaskFinished");
      });

      var maxTreeDepth = _.max(relevantLogs, function(l) {
        return l.recombinationBranchIndexes.length;
      }).recombinationBranchIndexes.length;

      var rootNode = {
        name: "",
        parent: "null",
        children: [],
        recombinationBranchIndexes: []
      };

      var treeDepthCompare = function(log) {
        return log.recombinationBranchIndexes.length == this;
      };

      var recombinationIndexCompare = function(log) {
        var recombinationIndex = log.recombinationBranchIndexes;
        return recombinationIndex;
      };

      var parentNodeCompare = function(log) {
        var parentRecombination = log.recombinationBranchIndexes.join();
        return this.recombinationBranchIndexes.join().indexOf(parentRecombination) === 0;
      };

      var findTaskSettings = function(log) {
        return this.recombinationBranchIndexes &&
          log.recombinationBranchIndexes &&
          this.recombinationBranchIndexes.join() == log.recombinationBranchIndexes.join() &&
          log.eventType == "TaskSettings";
      };

      var currentNodes = [rootNode];
      for (var i = 1; i <= maxTreeDepth; i++) {

        var relevantLogsForNode = _.filter(relevantLogs, treeDepthCompare, i);
        var recombinationNodes = _.groupBy(relevantLogsForNode, recombinationIndexCompare);
        var newNodes = [];
        for (var recombinationIndex in recombinationNodes) {
          var logGroup = recombinationNodes[recombinationIndex];
          var taskSettings = _.find(logs, findTaskSettings, logGroup[0]);

          var node = {
            name: logGroup[0].msg,
            recombinationBranchIndexes: logGroup[0].recombinationBranchIndexes,
            children: [],
            taskType: logGroup[0].taskType,
            taskObjectId: logGroup[0].taskObjectId,
            taskSettings: JSON.stringify(taskSettings.data).replace(/("|{|})/g, "")
          };

          var parent = _.find(currentNodes, parentNodeCompare, node);
          parent.children.push(node);
          node.parent = parent;

          newNodes.push(node);
        }

        currentNodes = newNodes;
      }

      postMessage([rootNode]);
    };
  }
);
