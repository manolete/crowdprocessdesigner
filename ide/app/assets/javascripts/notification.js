/**
 * Provides notification methods to add notifications on the screen
 * @module notification
 */
define(['jquery'],
  /** @lends module:notification  */
  function($) {
    return {
      addMessage: function(message) {
        var el = $("#notification");
        el.html('<i class="fa fa-info-circle"></i> ' + message);
        el.before(el.clone(true)).remove();
      },
      addOverlay: function(html) {
        $("body").append(html);

        $("#overlay").click(function(e) {
          e.currentTarget.remove();
        });
      }
    };
  });
