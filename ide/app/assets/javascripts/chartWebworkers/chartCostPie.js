importScripts('../require.js');
/**
 * Webworker module to calculate the pie data for process costs
 * @module treeWebWorkers/logTree
 */
require({
    baseUrl: "../",
    paths: {
      underscore: '../lib/underscorejs/underscore',
    },
  }, ["require", "underscore"],
  /** @lends module:chartWebworkers/chartCostPie  */
  function(require, _) {
    /** Calculates the logs on message
    @param {Array} oEvent - the message event containing the logs
    */
    this.onmessage = function(oEvent) {

      var logs = oEvent.data;

      var costLogs = _.filter(logs, function(d) {
        return d.taskObjectId && d.taskObjectId !== null && d.eventType === "TaskCost";
      });

      var findTaskSettings = function(log) {
        return this.recombinationBranchIndexes &&
          log.recombinationBranchIndexes &&
          this.recombinationBranchIndexes.join() === log.recombinationBranchIndexes.join() &&
          log.eventType === "TaskSettings";
      };

      var maxId = _.max(logs, function(d) {
        return parseInt(d.taskObjectId);
      }).taskObjectId + 5; /* +5 to avoid dark colors */

      var runtimes = _.map(costLogs, function(costLog) {
        var taskSettings = _.find(logs, findTaskSettings, costLog);

        var obj = {
          label: costLog.msg,
          taskObjectId: costLog.taskObjectId,
          color: '#' + (0x1000000 + (parseInt(costLog.taskObjectId) / maxId) * 0xffffff).toString(16).substr(1, 6), // random color
          highlight: "#FF5A5E",
          tooltipLabel: "<b>" + costLog.msg + "</b>" + "<br>Cost: " + costLog.data + " <br>" + JSON.stringify(taskSettings.data).replace(/("|{|})/g, "").split(",").join("<br>") + "<br>RecombinationBranchIndexes: " + costLog.recombinationBranchIndexes.join(";"),
          value: costLog.data
        };

        return obj;
      });

      postMessage(_.sortBy(runtimes, function(d) {
        return d.taskObjectId;
      }));
    };
  }
);
