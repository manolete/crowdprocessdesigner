importScripts('../require.js');
/**
 * Webworker module to calculate the bar data for process time
 * @module treeWebWorkers/logTree
 */
require({
    baseUrl: "../",
    paths: {
      underscore: '../lib/underscorejs/underscore',
    },
  }, ["require", "underscore"],
  /** @lends module:chartWebworkers/chartTimeBar  */
  function(require, _) {
    /** Calculates the logs on message
    @param {Array} oEvent - the message event containing the logs
    */
    this.onmessage = function(oEvent) {

      var logs = oEvent.data;

      var finishLogs = _.filter(logs, function(d) {
        return d.taskObjectId && d.taskObjectId !== null && d.eventType === "TaskFinished";
      });

      var relevantLogs = _.filter(logs, function(d) {
        return d.taskObjectId && d.taskObjectId !== null && (d.eventType === "TaskStart" || d.eventType === "TaskFinished");
      });

      var findTaskStart = function(l) {
        return l.eventType === "TaskStart" && l.taskObjectId === this.taskObjectId && this.recombinationBranchIndexes.join(";") === l.recombinationBranchIndexes.join(";");
      };

      var findTaskFinished = function(l) {
        var eventType = l.eventType === "TaskFinished";
        var taskObjectId = l.taskObjectId === this.taskObjectId;
        var recombination = this.recombinationBranchIndexes.join(",") === l.recombinationBranchIndexes.join(",");
        return eventType && taskObjectId && recombination;
      };

      var findTaskSettings = function(log) {
        return this.recombinationBranchIndexes &&
          log.recombinationBranchIndexes &&
          this.recombinationBranchIndexes.join() === log.recombinationBranchIndexes.join() &&
          log.eventType === "TaskSettings";
      };

      var runtimes = _.map(finishLogs, function(relevantLog) {
        var taskSettings = _.find(logs, findTaskSettings, relevantLog);
        var logStart = _.find(relevantLogs, findTaskStart, relevantLog);
        var logEnd = _.find(relevantLogs, findTaskFinished, relevantLog);

        var obj = {
          label: relevantLog.msg,
          tooltipLabel: "<b>" + relevantLog.msg + "</b>" + "<br>" + JSON.stringify(taskSettings.data).replace(/("|{|})/g, "").split(",").join("<br>") + "<br>RecombinationBranchIndexes: " + relevantLog.recombinationBranchIndexes.join(";"),
          recombinationBranchIndexes: relevantLog.recombinationBranchIndexes,
          value: null,
          taskObjectId: relevantLog.taskObjectId
        };
        if (logEnd && logStart) {
          obj.value = logEnd.data - logStart.data;
        }
        return obj;
      });

      var labels = _.map(runtimes, function(d) {
        return d.label;
      });
      var runtimesForRecombination = _.map(runtimes, function(d) {
        return d.value;
      });

      var dataset = {
        fillColor: "rgba(220,220,220,0.5)",
        strokeColor: "rgba(220,220,220,0.8)",
        highlightFill: "rgba(220,220,220,0.75)",
        highlightStroke: "rgba(220,220,220,1)",
        data: runtimesForRecombination
      };

      postMessage({
        labels: labels,
        datasets: [dataset],
        runtimes: runtimes
      });
    };
  });
