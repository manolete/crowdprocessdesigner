importScripts('../require.js');
/**
 * Webworker module to calculate the pie data for process time
 * @module treeWebWorkers/logTree
 */
require({
    baseUrl: "../",
    paths: {
      underscore: '../lib/underscorejs/underscore',
    },
  }, ["require", "underscore"],
  /** @lends module:chartWebworkers/chartTimePie  */
  function(require, _) {
    /** Calculates the logs on message
    @param {Array} oEvent - the message event containing the logs
    */
    this.onmessage = function(oEvent) {

      var logs = oEvent.data;

      var finishLogs = _.filter(logs, function(d) {
        return d.taskObjectId && d.taskObjectId !== null && d.eventType === "TaskFinished";
      });

      var relevantLogs = _.filter(logs, function(d) {
        return d.taskObjectId && d.taskObjectId !== null && (d.eventType === "TaskStart" || d.eventType === "TaskFinished");
      });

      var findTaskStart = function(l) {
        return l.eventType === "TaskStart" && l.taskObjectId === this.taskObjectId && this.recombinationBranchIndexes.join(";") === l.recombinationBranchIndexes.join(";");
      };

      var findTaskFinished = function(l) {
        var eventType = l.eventType === "TaskFinished";
        var taskObjectId = l.taskObjectId === this.taskObjectId;
        var recombination = this.recombinationBranchIndexes.join(",") === l.recombinationBranchIndexes.join(",");
        return eventType && taskObjectId && recombination;
      };

      var findTaskSettings = function(log) {
        return this.recombinationBranchIndexes &&
          log.recombinationBranchIndexes &&
          this.recombinationBranchIndexes.join() === log.recombinationBranchIndexes.join() &&
          log.eventType === "TaskSettings";
      };

      var maxId = _.max(logs, function(d) {
        return parseInt(d.taskObjectId);
      }).taskObjectId + 5; /* +5 to avoid dark colors */

      var runtimes = _.map(finishLogs, function(relevantLog) {
        var taskSettings = _.find(logs, findTaskSettings, relevantLog);
        var logStart = _.find(relevantLogs, findTaskStart, relevantLog);
        var logEnd = _.find(relevantLogs, findTaskFinished, relevantLog);

        var runtime = logEnd && logStart ? logEnd.data - logStart.data : null;
        var obj = {
          label: relevantLog.msg,
          taskObjectId: relevantLog.taskObjectId,
          color: '#' + (0x1000000 + (parseInt(relevantLog.taskObjectId) / maxId) * 0xffffff).toString(16).substr(1, 6), // random color
          highlight: "#FF5A5E",
          tooltipLabel: "<b>" + relevantLog.msg + "</b>" + "<br>Time: " + runtime + " ms<br>" + JSON.stringify(taskSettings.data).replace(/("|{|})/g, "").split(",").join("<br>") + "<br>RecombinationBranchIndexes: " + relevantLog.recombinationBranchIndexes.join(";"),
          value: runtime
        };

        return obj;
      });

      postMessage(_.sortBy(runtimes, function(d) {
        return d.taskObjectId;
      }));
    };
  }
);
