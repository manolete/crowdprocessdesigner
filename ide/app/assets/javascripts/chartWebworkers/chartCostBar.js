importScripts('../require.js');
/**
 * Webworker module to calculate the bar data for process costs
 * @module treeWebWorkers/logTree
 */
require({
    baseUrl: "../",
    paths: {
      underscore: '../lib/underscorejs/underscore',
    },
  }, ["require", "underscore"],
  /** @lends module:chartWebworkers/chartCostBar  */
  function(require, _) {
    /** Calculates the logs on message
    @param {Array} oEvent - the message event containing the logs
    */
    this.onmessage = function(oEvent) {

      var logs = oEvent.data;

      var costLogs = _.filter(logs, function(d) {
        return d.taskObjectId && d.taskObjectId !== null && d.eventType === "TaskCost";
      });

      var findTaskSettings = function(log) {
        return this.recombinationBranchIndexes &&
          log.recombinationBranchIndexes &&
          this.recombinationBranchIndexes.join() === log.recombinationBranchIndexes.join() &&
          log.eventType === "TaskSettings";
      };

      var runtimes = _.map(costLogs, function(costLog) {
        var taskSettings = _.find(logs, findTaskSettings, costLog);
        var obj = {
          label: costLog.msg,
          tooltipLabel: "<b>" + costLog.msg + "</b>" + "<br>" + JSON.stringify(taskSettings.data).replace(/("|{|})/g, "").split(",").join("<br>") + "<br>RecombinationBranchIndexes: " + costLog.recombinationBranchIndexes.join(";"),
          recombinationBranchIndexes: costLog.recombinationBranchIndexes,
          value: costLog.data,
          taskObjectId: costLog.taskObjectId
        };

        return obj;
      });

      var labels = _.map(runtimes, function(d) {
        return d.label;
      });

      var runtimesForRecombination = _.map(runtimes, function(d) {
        return d.value;
      });

      var dataset = {
        fillColor: "rgba(220,220,220,0.5)",
        strokeColor: "rgba(220,220,220,0.8)",
        highlightFill: "rgba(220,220,220,0.75)",
        highlightStroke: "rgba(220,220,220,1)",
        data: runtimesForRecombination
      };

      postMessage({
        labels: labels,
        datasets: [dataset],
        runtimes: runtimes
      });
    };
  });
