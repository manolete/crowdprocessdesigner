/**
 * Module listening to repository events (SocketIO events)
 * @module dispatcher
 */
define(['backbone', 'underscore', './canvas/canvasWrapper', './views/serviceView', './repository'],
/** @lends module:dispatcher  */
function(Backbone, _, canvasWrapper, ServiceView, Repository) {
  var dispatcher = _.extend({}, Backbone.Events);

  /**
 * Appends the item to the local Storage.
 * @param {string} name - Key in localStorage.
 * @param {string} data - Value in localStorage.
 */
  var appendToStorage = function(name, data) {
    var old = JSON.parse(localStorage.getItem(name));
    if (old === null || !(old instanceof Array)) { old = []; }
    old.push(data);
    localStorage.setItem(name, JSON.stringify(old));
  };

  var dblClickEvent = "canvasEditor:dblClick";
  dispatcher.listenTo(canvasWrapper, dblClickEvent, function(object) {
    ServiceView.show(object);
  });

  var taskStartEvent = "TaskStart";
  dispatcher.listenTo(Repository, taskStartEvent, function(event) {
    dispatcher.trigger(taskStartEvent, event);
    appendToStorage("logs", event);
  });

  var taskStopEvent = "TaskFinished";
  dispatcher.listenTo(Repository, taskStopEvent, function(event) {
    dispatcher.trigger(taskStopEvent, event);
    appendToStorage("logs", event);
  });

  var taskFailedEvent = "TaskFailed";
  dispatcher.listenTo(Repository, taskFailedEvent, function(event) {
    dispatcher.trigger(taskFailedEvent, event);
    appendToStorage("logs", event);
  });

  var taskSettingsEvent = "TaskSettings";
  dispatcher.listenTo(Repository, taskSettingsEvent, function(event) {
    dispatcher.trigger(taskSettingsEvent, event);
    appendToStorage("logs", event);
  });

  var processFailed = "ProcessFailed";
  dispatcher.listenTo(Repository, processFailed, function(event) {
    var lastTaskObject = _.find(JSON.parse(localStorage.getItem("logs")).slice(0).reverse(), function(d) {
      return d.taskObjectId !== null;
    });
    dispatcher.trigger(taskFailedEvent, lastTaskObject);
    appendToStorage("logs", event);
  });

  var processFinished = "ProcessFinished";
  dispatcher.listenTo(Repository, processFinished, function(event) {
    appendToStorage("logs", event);
  });

  var processStart = "ProcessStart";
  dispatcher.listenTo(Repository, processStart, function(event) {
    appendToStorage("logs", event);
  });

  var taskTime = "TaskTime";
  dispatcher.listenTo(Repository, taskTime, function(event) {
    appendToStorage("logs", event);
  });

  var taskCost = "TaskCost";
  dispatcher.listenTo(Repository, taskCost, function(event) {
    appendToStorage("logs", event);
  });

  var intermediateResult = "IntermediateResult";
  dispatcher.listenTo(Repository, intermediateResult, function(event) {
    appendToStorage("logs", event);
  });

  var result = "Result";
  dispatcher.listenTo(Repository, result, function(event) {
    appendToStorage("logs", event);
  });

  return /** @alias module:dispatcher */dispatcher;
});
