package osgiFramework

import java.util.HashMap
import java.util.ServiceLoader

import scala.collection.JavaConversions.asScalaBuffer

import org.apache.felix.main.AutoProcessor
import org.osgi.framework.BundleContext
import org.osgi.framework.launch.Framework
import org.osgi.framework.launch.FrameworkFactory
import org.slf4j.LoggerFactory

import com.typesafe.config.ConfigFactory

object Launcher {
  def log = LoggerFactory.getLogger("OsgiFramework")

  val conf = ConfigFactory.load()

  val locator = ServiceLoader.load(classOf[FrameworkFactory]);
  val frameworkFactory = locator.iterator().next();
  val props = new HashMap[String, String]
  private[this] var context: Option[BundleContext] = None
  private[this] var framework: Option[Framework] = None

  def init() = {

    props.put("felix.auto.deploy.dir", "./osgiBundles")
    props.put("felix.auto.deploy.action", "install, start")
    //props.put("felix.fileinstall.poll", "2000")
    //props.put("felix.fileinstall.debug", "-1")
    //props.put("felix.fileinstall.dir", "./osgiBundles")
    props.put("org.osgi.framework.storage.clean", "onFirstInit")
    props.put("org.osgi.framework.bootdelegation", "sun, com.sun")

    val scalaPackagesToExportString = conf
      .getStringList("osgiFramework.scalaPackagesToExport")
      .map(s => "%s; version=2.11.4" format (s))
      .mkString(", ")

    val otherPackages = "sun.misc, com.sun, sun, org.slf4j; version=1.7.5, javax.inject; version=1.0.0, "

    props.put("org.osgi.framework.system.packages.extra", otherPackages + scalaPackagesToExportString)

    // To enable debug output:
    //props.put("ds.showtrace", "true")
    //props.put("ds.showerrors", "true")

    log.info("Starting OSGI Framework")
    framework = Some(frameworkFactory.newFramework(props))
    framework.map(_.start())

    context = framework.map(_.getBundleContext())

    val paxUrl = conf.getString("osgiFramework.paxUrl")
    val mvnProtocolBundle = context.map(_.installBundle(paxUrl))

    mvnProtocolBundle.map(_.start())

    def mvn(url: String) = context.map(_.installBundle(String.format("mvn:%s", url)))

    val fragmentBundles = conf.getStringList("osgiFramework.fragmentBundles")
    fragmentBundles.foreach(path => mvn(path))

    val mavenPaths = conf.getStringList("osgiFramework.mavenPaths")
    mavenPaths.foreach(path => mvn(path).map(_.start()))

    val projectPaths = conf.getStringList("osgiFramework.localPaths")
    projectPaths.foreach(path => context.map(_.installBundle(path).start()))

    context.map(ctx => AutoProcessor.process(props, ctx))

    log.info("OSGI Framework started")
  }

  def stop(): Unit = {
    context.map(_.getBundle(0).stop())
    framework.map(_.stop())
  }
}
