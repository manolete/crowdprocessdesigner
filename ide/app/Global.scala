import org.slf4j.LoggerFactory

import com.typesafe.config.ConfigFactory

import osgiFramework.Launcher
import play.api.Application
import play.api.GlobalSettings

object Global extends GlobalSettings {
  def log = LoggerFactory.getLogger("OsgiFramework")
  val conf = ConfigFactory.load()

  override def onStart(app: Application) {
    val useInternal = conf.getBoolean("osgiFramework.useInternalOsgi")
    if (useInternal) {
      Launcher.init()
    }

    log.info("Application has started")
  }

  override def onStop(app: Application) {    
    val useInternal = conf.getBoolean("osgiFramework.useInternalOsgi")
    if (useInternal) {
      Launcher.stop()
    }
  }
}