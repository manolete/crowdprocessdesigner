name := """ide"""

version := "1.0"

scalaVersion := "2.11.4"

scalacOptions ++= Seq("-feature", "-language:postfixOps")

javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint")

lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .enablePlugins(SbtWeb)

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws,
  "org.webjars" %% "webjars-play" % "2.3.0",
  "org.webjars" % "requirejs" % "2.1.14-3",
  "org.webjars" % "font-awesome" % "4.2.0",
  "org.webjars" % "underscorejs" % "1.6.0-3",
  "org.webjars" % "jquery" % "2.1.1",
  "org.webjars" % "backbonejs" % "1.1.2-2",
  "org.webjars" % "requirejs-text" % "2.0.10-2",
  "org.webjars" % "chartjs" % "1.0.1-beta.4",
  "org.webjars" % "d3js" % "3.5.2",
  "org.scalatest" %% "scalatest" % "2.2.1" % "test",
  "org.osgi" % "org.osgi.core" % "5.0.0",
  "org.osgi" % "org.osgi" % "3.0.0",
  "org.apache.felix" % "org.apache.felix.framework" % "4.6.0",
  "org.apache.felix" % "org.apache.felix.main" % "4.6.0",
  "org.slf4j" % "slf4j-api" % "1.7.5",
  "ch.qos.logback" % "logback-classic" % "1.0.9",
  "com.typesafe" % "config" % "1.2.1"
)

LessKeys.compress in Assets := true

pipelineStages := Seq(rjs, digest, gzip)

initialize := {
  val _ = initialize.value
  if (sys.props("java.specification.version") != "1.8")
    sys.error("Java 8 is required for this project.")
}

