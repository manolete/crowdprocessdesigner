package crowdProcessDesigner.PPLibService.Process

import scala.collection.JavaConverters.collectionAsScalaIterableConverter
import scala.concurrent.duration.Duration
import scala.reflect.classTag
import scala.util.Failure
import scala.util.Success

import org.osgi.framework.BundleContext
import org.osgi.framework.wiring.BundleWiring

import aQute.bnd.annotation.component.Activate
import aQute.bnd.annotation.component.Component
import ch.uzh.ifi.pdeboer.pplib.hcomp._
import ch.uzh.ifi.pdeboer.pplib.process.PPLibProcess
import ch.uzh.ifi.pdeboer.pplib.process._
import ch.uzh.ifi.pdeboer.pplib.process.parameter._
import crowdProcessDesigner.PPLibService.PPLibService
import crowdProcessDesigner.interfaces.EventType
import crowdProcessDesigner.interfaces.ProcessDataChunk
import crowdProcessDesigner.interfaces.ProcessMessageProvider
import crowdProcessDesigner.interfaces.ProcessTask
import crowdProcessDesigner.interfaces.ServiceInformation
import crowdProcessDesigner.interfaces.TaskObject

@Component
class PPLibProcessService extends PPLibService with ProcessTask {

  private[this] var processClassMap: Map[String, Class[_]] = Map.empty
  private[this] var inputParameterMap: Map[String, ServiceInformation] = Map.empty

  val ListIndexedPatchClass = classOf[List[IndexedPatch]]
  val IndexedPatchClass = classOf[IndexedPatch]
  val ListPatchClass = classOf[List[Patch]]
  val PatchClass = classOf[Patch]

  def process(input: Seq[Any], previouslyDiscardedInput: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {

    val portalServiceClassName = getParam[String](portalParam, params)
    val hCompPortalAdapter = getPortal(portalServiceClassName, portalConfigs)

    val serviceClassName = getParam[String](serviceParam, params)
    val process = getProcessClassInstance(serviceClassName, Map.empty)
    val processInstance = process.getOrElse(throw new Exception("PPLibService: process class could not be instantiated"))
    
    processInstance.allParams.map { p =>
      val definedParam = getParam[String](p.key, params)
      if (!definedParam.isEmpty()) {
        if (p.t == classTag[HCompPortalAdapter]) { processInstance.setParam(portalParam, hCompPortalAdapter) }
        else if (p.t == classTag[Int]) processInstance.setParam(p.key, definedParam.toInt)
        else if (p.t == classTag[Boolean]) processInstance.setParam(p.key, definedParam.toBoolean)
        else if (p.t == classTag[Duration]) processInstance.setParam(p.key, Duration.apply(definedParam))
        else if (p.t == classTag[String]) processInstance.setParam(p.key, definedParam)
        else throw new Exception("Not implemented processParameter type " + p.key)
      }
    }

    val result = util.Try {
      val pplibOutput = runProcess(processInstance, input)
      val typedOutput = getResult(processInstance, pplibOutput)

      processMessageProvider.sendEvent(taskObject, Some(recombinationBranchindex), EventType.TaskCost, 0)

      (typedOutput, previouslyDiscardedInput)
    }

    result match {
      case Success(v) => v
      case Failure(e) => {
        e.getStackTrace().foreach { x => System.err.println(x) }
        throw e
      }
    }
  }

  private[this] def getResult(processInstance: ProcessStub[Any, Any], pplibOutput: Any) = {
    val typedOutput = processInstance.outputType.runtimeClass match {
      case ListIndexedPatchClass =>
        val out = pplibOutput.asInstanceOf[List[IndexedPatch]]
        out.map(p => new ProcessDataChunk(Seq(p.index), p.value, None))
      case ListPatchClass =>
        val out = pplibOutput.asInstanceOf[List[Patch]]
        out.map(p => new ProcessDataChunk(Seq.empty, p.value, None))
      case IndexedPatchClass =>
        Seq(pplibOutput.asInstanceOf[IndexedPatch])
          .map(p => new ProcessDataChunk(Seq(p.index), p.value, None))
      case PatchClass =>
        Seq(pplibOutput.asInstanceOf[Patch])
          .map(p => new ProcessDataChunk(Seq.empty, p.value, None))
    }
    typedOutput
  }

  private[this] def runProcess(processInstance: ProcessStub[Any, Any], input: Seq[Any]) = {
    val pplibOutput = processInstance.inputType.runtimeClass match {
      case ListIndexedPatchClass | ListPatchClass => {
        val list = input.asInstanceOf[Seq[ProcessDataChunk]].map { chunk =>
          new IndexedPatch(chunk.data.asInstanceOf[String], chunk.chunkIds.last, None)
        }
        processInstance.process(list)
      }
      case IndexedPatchClass => {
        input.asInstanceOf[Seq[ProcessDataChunk]].map { chunk =>
          val patch = new IndexedPatch(chunk.data.asInstanceOf[String], chunk.chunkIds.last, None)
          processInstance.process(patch)
        }
      }
      case PatchClass => {
        input.asInstanceOf[Seq[ProcessDataChunk]].map { chunk =>
          val patch = new Patch(chunk.data.asInstanceOf[String], None)
          processInstance.process(patch)
        }
      }
    }
    pplibOutput
  }

  def getInformation(): Seq[ServiceInformation] = {
    inputParameterMap.values.toSeq
  }

  private[this] def getProcessClassInstance(canonicalClassName: String, params: Map[String, Any]): Option[ProcessStub[Any, Any]] = {
    val processClass: Class[_] = context.getBundle().loadClass(canonicalClassName)

    val constructor = processClass.getConstructor(classOf[Map[String, Any]])
    val processInstance = util.Try(constructor.newInstance(params).asInstanceOf[ProcessStub[Any, Any]])

    processInstance match {
      case Success(v) => Some(v)
      case Failure(e) => {
        System.err.print("PPLib process class could not be instantiated: " + processClass.getCanonicalName() + "\n")
        None
      }
    }
  }

  @Activate
  def start(context: BundleContext): Unit = {
    this.context = context
    setPortals(context)
    processClassMap = getAvailableProcessClassMap()
    inputParameterMap = getInputParameterMap()
  }

  // precalculate input parameters
  val HcompPortalAdapterTag = classTag[HCompPortalAdapter]
  val BooleanTag = classTag[Boolean]
  val IntTag = classTag[Int]
  val DoubleTag = classTag[Double]
  val StringTag = classTag[String]
  val DurationTag = classTag[Duration]
  val OptionAnyTag = classTag[Option[Any]]
  val passableProcessParam = classTag[PassableProcessParam[Any, Any]]
  
  private[this] def getInputParameterMap(): Map[String, ServiceInformation] = {
    val classes = processClassMap.map { case (className, klazz) => klazz }
    classes.map { klazz =>
      val processInstance = getProcessClassInstance(klazz.getCanonicalName(), Map.empty)
      val params = processInstance.map { f =>
        f.allParams.map { param =>
          param.t match {
            case HcompPortalAdapterTag => Option(param.key -> portals)
            case BooleanTag => Option(param.key -> Seq(true, false))
            case IntTag | DoubleTag | StringTag | DurationTag => Option(param.key -> param.get(f).toString())
            case OptionAnyTag => Option(param.key -> "")
            case `passableProcessParam` => Option(param.key -> "")
            case _ => None
          }
        }
          .flatten
      }
        .map(o => o.toMap)
        .getOrElse(Map.empty)

      (klazz.getCanonicalName() -> new ServiceInformation(klazz.getCanonicalName(), "PPLib Process", params))
    }
      .toMap
  }

  private[this] def getAvailableProcessClassMap(): Map[String, Class[_]] = {
    val wiring = context.getBundle().adapt(classOf[BundleWiring])

    val allResourcePathClassName: Seq[String] = wiring
      .listResources("ch/uzh/ifi/pdeboer/pplib/process/stdlib", "*.class",
        BundleWiring.LISTRESOURCES_RECURSE)
      .asScala
      .toSeq;

    allResourcePathClassName
      .filter(p => !p.contains('$'))
      .map(s => s.replace("/", "."))
      .map(s => s.replace(".class", ""))
      .map(s => (s -> context.getBundle().loadClass(s)))
      .filter { case (className, klazz) => klazz.isAnnotationPresent(classOf[PPLibProcess]) }
      .toMap
  }
}