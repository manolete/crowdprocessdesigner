package crowdProcessDesigner.PPLibService

import org.osgi.framework._
import scala.reflect._
import crowdProcessDesigner.interfaces._
import ch.uzh.ifi.pdeboer.pplib.hcomp._
import scala.util.matching.Regex
import crowdProcessDesigner.service.abstractService.AbstractService

abstract class PPLibService extends AbstractService {
  protected var context: BundleContext = null
  protected var portals: Seq[(String, String)] = Seq.empty

  protected val chunkIdsRegex = new Regex("""\[(\d+\|?)*\]""")
  
  protected val portalParam = "portal"

  def getPortal(className: String, portalConfigs: Map[String, Map[String, String]]): HCompPortalAdapter = {
    val filterString = "(component.Name=" + className + ")";
    val serviceReference = context.getServiceReferences(classOf[Portal].getName(), filterString);
    val portalService = context.getService(serviceReference(0)).asInstanceOf[Portal]

    val portalConfig = portalConfigs.get(portalService.getClass().getCanonicalName()).getOrElse(throw new Exception("No portal config defined"))
    portalService.getPortal(portalConfig).asInstanceOf[HCompPortalAdapter]
  }

  def setPortals(context: BundleContext) = {
    var serviceReferences = Option(context.getServiceReferences(classOf[Portal].getCanonicalName, null))

    portals = serviceReferences.map { srOption =>
      srOption.map { sr =>
        (context.getService(sr).asInstanceOf[Portal].getClass().getCanonicalName(),
          context.getService(sr).asInstanceOf[Portal].getClass().getCanonicalName())
      }.toSeq
    }.getOrElse(Seq.empty)
  }
}