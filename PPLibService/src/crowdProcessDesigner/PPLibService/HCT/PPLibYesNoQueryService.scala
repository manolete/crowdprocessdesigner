package crowdProcessDesigner.PPLibService.HCT

import org.osgi.framework.BundleContext
import aQute.bnd.annotation.component.Activate
import aQute.bnd.annotation.component.Component
import ch.uzh.ifi.pdeboer.pplib.hcomp.CompositeQuery
import ch.uzh.ifi.pdeboer.pplib.hcomp.CompositeQueryAnswer
import ch.uzh.ifi.pdeboer.pplib.hcomp.FreetextAnswer
import ch.uzh.ifi.pdeboer.pplib.hcomp.FreetextQuery
import crowdProcessDesigner.PPLibService.PPLibService
import crowdProcessDesigner.interfaces.HumanComputationTask
import crowdProcessDesigner.interfaces.ProcessDataChunk
import crowdProcessDesigner.interfaces.ProcessMessageProvider
import crowdProcessDesigner.interfaces.ServiceInformation
import crowdProcessDesigner.interfaces.TaskObject
import scala.util.Success
import scala.util.Failure
import scala.util.Try
import ch.uzh.ifi.pdeboer.pplib.hcomp.MultipleChoiceQuery
import scala.util.Random

@Component
class PPLibYesNoWithUniqueSolutionQueryService extends PPLibService with HumanComputationTask {

  def process(input: Seq[Any], previouslyDiscardedInput: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    val typedInput = input.asInstanceOf[Seq[ProcessDataChunk]];

    val participantCount =  getParam[Int](numberOfWorkersParam, params)
    val question = getParam[String](questionParam, params)

    val portalServiceClassName = getParam[String](portalParam, params)
    val hCompPortalAdapter = getPortal(portalServiceClassName, portalConfigs)

    val result = util.Try {
      val options = typedInput
        .map(s => "[" + s.chunkIds.mkString("|") + "]" + s.data.toString())
        .toList

      val queries = typedInput.map(s => new MultipleChoiceQuery("[" + s.chunkIds.mkString("|") + "]" + s.data.toString() + " " + s.context.toString(), List("Yes", "No"), 1, 1, "Verify Result", true))
      val query = new CompositeQuery(queries.toList, question)

      val submissions = (1 to participantCount).par.map { n =>
        val result = hCompPortalAdapter
          .sendQueryAndAwaitResult(query)
          .asInstanceOf[Option[CompositeQueryAnswer]]

        result
          .getOrElse(throw new Exception("No answer received"))
          .answers
      }

      val newChunks = submissions.map { data => data }
        .flatten
        .map {
          case (query, oAnswer) =>
            val submissionIndex = chunkIdsRegex.findFirstIn(query.question)
            val existingChunk = typedInput.find(p => Some("[" + p.chunkIds.mkString("|") + "]") == submissionIndex)
            val answer = oAnswer.map { answer =>
              answer.toString() match {
                case "Yes" => true
                case "No" => false
              }
            }

            val newChunk = existingChunk match {
              case Some(e) => new ProcessDataChunk(e, chunkIdsRegex.replaceAllIn(query.question, ""), e.context)
              case None => new ProcessDataChunk(Seq(0), chunkIdsRegex.replaceAllIn(query.question, ""), None)
            }
            newChunk.isCorrectData = answer
            newChunk
        }

      val newDiscardedJunks = typedInput
        .filter { oldInput => newChunks.find { newChunk => oldInput.chunkIds.equals(newChunk.chunkIds) } == None }

      (newChunks.seq, previouslyDiscardedInput.union(newDiscardedJunks))
    }

    result match {
      case Success(v) => v
      case Failure(e) => {
        e.getStackTrace().foreach { x => System.err.println(x) }
        throw e
      }
    }
  }

  def getInformation(): Seq[ServiceInformation] = {
    val inputParams: Map[String, Any] = Map(
      (portalParam -> portals),
      (questionParam -> "Is this correct?"),
      (numberOfWorkersParam -> 1))

    Seq(new ServiceInformation("PPLibYesNo", "A PPLib Query for verifying results", inputParams))
  }

  @Activate
  def start(context: BundleContext): Unit = {
    this.context = context
    setPortals(context)
  }
}