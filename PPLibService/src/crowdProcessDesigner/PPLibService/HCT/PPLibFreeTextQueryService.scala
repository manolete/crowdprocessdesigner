package crowdProcessDesigner.PPLibService.HCT

import org.osgi.framework.BundleContext

import aQute.bnd.annotation.component.Activate
import aQute.bnd.annotation.component.Component
import ch.uzh.ifi.pdeboer.pplib.hcomp.CompositeQuery
import ch.uzh.ifi.pdeboer.pplib.hcomp.CompositeQueryAnswer
import ch.uzh.ifi.pdeboer.pplib.hcomp.FreetextAnswer
import ch.uzh.ifi.pdeboer.pplib.hcomp.FreetextQuery
import crowdProcessDesigner.PPLibService.PPLibService
import crowdProcessDesigner.interfaces.HumanComputationTask
import crowdProcessDesigner.interfaces.ProcessDataChunk
import crowdProcessDesigner.interfaces.ProcessMessageProvider
import crowdProcessDesigner.interfaces.ServiceInformation
import crowdProcessDesigner.interfaces.TaskObject
import scala.util.Success
import scala.util.Failure
import scala.util.Try
import scala.util.matching.Regex

@Component
class PPLibFreeTextQueryService extends PPLibService with HumanComputationTask {

  val inputDataParam = "InputData"
  val inputAsQuestionOption = "InputAsQuestions"
  val ignoreInputOption = "IgnoreInput"
  val useGivenQuestionForEachChunk = "UseGivenQuestionForEachChunk"
  val inputDataOptions = Seq(inputAsQuestionOption, ignoreInputOption, useGivenQuestionForEachChunk)

  def process(input: Seq[Any], previouslyDiscardedInput: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {

    val typedInput = input.asInstanceOf[Seq[ProcessDataChunk]]

    val participantCount = getParam[Int](numberOfWorkersParam, params)
    val question = getParam[String](questionParam, params)

    val portalServiceClassName = getParam[String](portalParam, params)
    val hCompPortalAdapter = getPortal(portalServiceClassName, portalConfigs)

    val queries: List[FreetextQuery] = params.get(inputDataParam) match {
      case Some(`inputAsQuestionOption`) => {
        typedInput.map { inputVal =>
          FreetextQuery("[" + inputVal.chunkIds.mkString("|") + "]" + inputVal.data.asInstanceOf[String])
        } toList
      }
      case Some(`useGivenQuestionForEachChunk`) => {
        typedInput.map { f =>
          FreetextQuery(question + "[" + f.chunkIds.mkString("|") + "]" + "\n" + f.data)
        } toList
      }
      case Some(`ignoreInputOption`) => {
        List(FreetextQuery(question))
      }
      case _ => throw new Exception("input data options not selected")
    }

    val result = util.Try {
      val submissions = (1 to participantCount).par.map { n =>

        val result = hCompPortalAdapter.sendQueryAndAwaitResult(
          CompositeQuery(queries,
            "Please answer the following questions")).asInstanceOf[Option[CompositeQueryAnswer]]

        result
          .getOrElse(throw new Exception("No answer received"))
          .answers
      }

      val newChunks = submissions.map { data => data }
        .flatten
        .map {
          case (k, v) =>
            val submissionIndex = chunkIdsRegex.findFirstIn(k.question)
            val existingChunk = typedInput.find(p => Some("[" + p.chunkIds.mkString("|") + "]") == submissionIndex)
            val answer = v.getOrElse("")
            existingChunk match {
              case Some(e) => new ProcessDataChunk(e, answer, e.context)
              case None => new ProcessDataChunk(Seq(0), answer, None)
            }
        }

      (newChunks.seq, previouslyDiscardedInput)
    }

    result match {
      case Success(v) => v
      case Failure(e) => {
        e.getStackTrace().foreach { x => System.err.println(x) }
        throw e
      }
    }
  }

  def getInformation(): Seq[ServiceInformation] = {
    val inputParams: Map[String, Any] = Map(
      (portalParam -> portals),
      (numberOfWorkersParam -> 1),
      (inputDataParam -> inputDataOptions),
      (questionParam -> ""))

    Seq(new ServiceInformation("PPLibFreeText", "A PPLib service providing a free text query", inputParams))
  }

  @Activate
  def start(context: BundleContext): Unit = {
    this.context = context
    setPortals(context)
  }
}