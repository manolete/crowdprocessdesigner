package crowdProcessDesigner.PPLibService.HCT

import org.osgi.framework.BundleContext
import aQute.bnd.annotation.component.Activate
import aQute.bnd.annotation.component.Component
import ch.uzh.ifi.pdeboer.pplib.hcomp.CompositeQuery
import ch.uzh.ifi.pdeboer.pplib.hcomp.CompositeQueryAnswer
import ch.uzh.ifi.pdeboer.pplib.hcomp.FreetextAnswer
import ch.uzh.ifi.pdeboer.pplib.hcomp.FreetextQuery
import crowdProcessDesigner.PPLibService.PPLibService
import crowdProcessDesigner.interfaces.HumanComputationTask
import crowdProcessDesigner.interfaces.ProcessDataChunk
import crowdProcessDesigner.interfaces.ProcessMessageProvider
import crowdProcessDesigner.interfaces.ServiceInformation
import crowdProcessDesigner.interfaces.TaskObject
import scala.util.Success
import scala.util.Failure
import scala.util.Try
import ch.uzh.ifi.pdeboer.pplib.hcomp.MultipleChoiceQuery
import scala.util.matching.Regex

@Component
class PPLibVoteWithUniqueSolutionQueryService extends PPLibService with HumanComputationTask {

  val inputDataParam = "InputData"
  val titleParam = "Title"

  def process(input: Seq[Any], previouslyDiscardedInput: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    val typedInput = input.asInstanceOf[Seq[ProcessDataChunk]];

    val participantCount = getParam[Int](numberOfWorkersParam, params)
    val question = getParam[String](questionParam, params)
    val title = getParam[String](titleParam, params)

    val portalServiceClassName = getParam[String](portalParam, params)
    val hCompPortalAdapter = getPortal(portalServiceClassName, portalConfigs)

    val result = util.Try {
      val queries = typedInput
        .groupBy { chunk => chunk.chunkIds }
        .map {
          case (chunkIds, chunks) =>
            val options = chunks.map(c => c.data.toString()).toList
            new MultipleChoiceQuery("[" + chunkIds.mkString("|") + "]" + question, options, 1, 1, "", true)
        }
        .toList

      val compositeQuery = new CompositeQuery(queries, question)

      val submissions = (1 to participantCount).par.map { n =>

        val result = hCompPortalAdapter
          .sendQueryAndAwaitResult(compositeQuery)
          .asInstanceOf[Option[CompositeQueryAnswer]]

        result
          .getOrElse(throw new Exception("No answer received"))
          .answers
      }

      val newChunks = submissions.map { data => data }
        .flatten
        .groupBy { case (query, answer) => query.question }
        .map {
          case (question, answers) =>
            val submissionIndex = chunkIdsRegex.findFirstIn(question)
            val existingChunk = typedInput.find(p => Some("[" + p.chunkIds.mkString("|") + "]") == submissionIndex)

            val relevantAnswer = answers
              .map { case (query, answer) => answer }
              .groupBy { answer => answer.get.toString() }
              .map { case (answer, group) => (group.length, answer) }
              .toList
              .sortBy { case (numberOfVotes, answer) => numberOfVotes }
              .map { case (numberOfVotes, answer) => answer }
              .last

            existingChunk match {
              case Some(e) => new ProcessDataChunk(e, relevantAnswer, e.context)
              case None => new ProcessDataChunk(Seq(0), relevantAnswer, None)
            }
        }
        .toSeq

      val newDiscardedJunks = typedInput
        .filter { oldInput => newChunks.find { newChunk => oldInput.chunkIds.equals(newChunk.chunkIds) } == None }

      (newChunks.seq, previouslyDiscardedInput.union(newDiscardedJunks))
    }

    result match {
      case Success(v) => v
      case Failure(e) => {
        e.getStackTrace().foreach { x => System.err.println(x) }
        throw e
      }
    }
  }

  def getInformation(): Seq[ServiceInformation] = {
    val inputParams: Map[String, Any] = Map(
      (portalParam -> portals),
      (questionParam -> "Which solution do you prefer?"),
      (numberOfWorkersParam -> 1),
      (titleParam -> ""))

    Seq(new ServiceInformation("PPLibVoteWithUniqueSolution", "A PPLib service providing Voting with a Unique Solution", inputParams))
  }

  @Activate
  def start(context: BundleContext): Unit = {
    this.context = context
    setPortals(context)
  }
}