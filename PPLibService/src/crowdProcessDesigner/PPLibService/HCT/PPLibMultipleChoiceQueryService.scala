package crowdProcessDesigner.PPLibService.HCT

import org.osgi.framework.BundleContext
import aQute.bnd.annotation.component.Activate
import aQute.bnd.annotation.component.Component
import ch.uzh.ifi.pdeboer.pplib.hcomp.CompositeQuery
import ch.uzh.ifi.pdeboer.pplib.hcomp.CompositeQueryAnswer
import ch.uzh.ifi.pdeboer.pplib.hcomp.FreetextAnswer
import ch.uzh.ifi.pdeboer.pplib.hcomp.FreetextQuery
import crowdProcessDesigner.PPLibService.PPLibService
import crowdProcessDesigner.interfaces.HumanComputationTask
import crowdProcessDesigner.interfaces.ProcessDataChunk
import crowdProcessDesigner.interfaces.ProcessMessageProvider
import crowdProcessDesigner.interfaces.ServiceInformation
import crowdProcessDesigner.interfaces.TaskObject
import scala.util.Success
import scala.util.Failure
import scala.util.Try
import ch.uzh.ifi.pdeboer.pplib.hcomp.MultipleChoiceQuery
import scala.util.matching.Regex

@Component
class PPLibMultipleChoiceQueryService extends PPLibService with HumanComputationTask {

  val inputDataParam = "InputData"
  val maxNumberOfResultsParam = "MaxNumberOfResults"
  val minNumberOfResultsParam = "MinNumberOfResults"
  val titleParam = "Title"
  val valueIsRequiredParam = "ValueIsRequired"

  def process(input: Seq[Any], previouslyDiscardedInput: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    val typedInput = input.asInstanceOf[Seq[ProcessDataChunk]];

    val participantCount = getParam[Int](numberOfWorkersParam, params)
    val question = getParam[String](questionParam, params)
    val maxNumberOfResults = getParam[Int](maxNumberOfResultsParam, params)
    val minNumberOfResults = getParam[Int](minNumberOfResultsParam, params)
    val title =  getParam[String](titleParam, params)
    val valueIsRequired = getParam[Boolean](valueIsRequiredParam, params)

    val portalServiceClassName = getParam[String](portalParam, params)
    val hCompPortalAdapter = getPortal(portalServiceClassName, portalConfigs)

    val result = util.Try {
      val submissions = (1 to participantCount).par.map { n =>

        val options = typedInput.map(s => "[" + s.chunkIds.mkString("|") + "]" + s.data.toString()).toList
        val query = new MultipleChoiceQuery(question, options, maxNumberOfResults, minNumberOfResults, title, valueIsRequired)

        val result = hCompPortalAdapter
          .sendQueryAndAwaitResult(query)

        result.map(f => f.toString())
      }

      val newChunks = submissions.map { data => data }
        .flatten
        .map { data =>
          val submissionIndex = chunkIdsRegex.findFirstIn(data)
          val existingChunk = typedInput.find(p => Some("[" + p.chunkIds.mkString("|") + "]") == submissionIndex)
          existingChunk match {
            case Some(e) => new ProcessDataChunk(e, chunkIdsRegex.replaceAllIn(data, ""), e.context)
            case None => new ProcessDataChunk(Seq(0), chunkIdsRegex.replaceAllIn(data, ""), None)
          }
        }

      val newDiscardedJunks = typedInput
        .filter { oldInput => newChunks.find { newChunk => oldInput.chunkIds.equals(newChunk.chunkIds) } == None }

      (newChunks.seq, previouslyDiscardedInput.union(newDiscardedJunks))
    }

    result match {
      case Success(v) => v
      case Failure(e) => {
        e.getStackTrace().foreach { x => System.err.println(x) }
        throw e
      }
    }
  }

  def getInformation(): Seq[ServiceInformation] = {
    val inputParams: Map[String, Any] = Map(
      (portalParam -> portals),
      (questionParam -> ""),
      (numberOfWorkersParam -> 1),
      (maxNumberOfResultsParam -> 1),
      (minNumberOfResultsParam -> 1),
      (titleParam -> ""),
      (valueIsRequiredParam -> Seq(true, false)))

    Seq(new ServiceInformation("PPLibMultipleChoice", "A PPLib service providing a multiple choice query", inputParams))
  }

  @Activate
  def start(context: BundleContext): Unit = {
    this.context = context
    setPortals(context)
  }
}