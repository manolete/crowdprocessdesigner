package crowdProcessDesigner.PPLibService.Portal

import org.osgi.framework.BundleContext

import aQute.bnd.annotation.component.Activate
import aQute.bnd.annotation.component.Component
import ch.uzh.ifi.pdeboer.pplib.hcomp.crowdflower._
import crowdProcessDesigner.interfaces.Portal
import crowdProcessDesigner.interfaces.ServiceInformation

@Component
class CrowdFlowerPortal extends AbstractPortalBuilderService[CrowdFlowerPortalBuilder]( {() => new CrowdFlowerPortalBuilder() }) with Portal {

  val allParams = inputParams + (portalBuilderCtor().SANDBOX -> Seq(true, false))
  
  def getInformation(): Seq[ServiceInformation] = {
    Seq(new ServiceInformation(classOf[CrowdFlowerPortal].getCanonicalName(), "CrowdFlower Portal Configuration", allParams))
  }

  @Activate
  def start(context: BundleContext): Unit = {}
}