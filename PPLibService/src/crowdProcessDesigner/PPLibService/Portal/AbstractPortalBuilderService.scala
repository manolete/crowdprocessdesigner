package crowdProcessDesigner.PPLibService.Portal

import org.osgi.framework.BundleContext

import aQute.bnd.annotation.component.Activate
import aQute.bnd.annotation.component.Component
import ch.uzh.ifi.pdeboer.pplib.hcomp._
import crowdProcessDesigner.interfaces.Portal
import crowdProcessDesigner.interfaces.ServiceInformation

abstract class AbstractPortalBuilderService[T <: HCompPortalBuilder](val portalBuilderCtor: () => T) {
  protected val inputParams: Map[String, String] = portalBuilderCtor()
    .expectedParameters
    .map(f => (f -> ""))
    .toMap
    
  def getPortal(params: Map[String, String]): Any = {
    val portalBuilder = portalBuilderCtor()
    params.foreach { case (key, value) => portalBuilder.setParameter(key, value) }

    portalBuilder.build
  }
}