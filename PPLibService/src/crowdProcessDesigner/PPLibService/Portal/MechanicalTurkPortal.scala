package crowdProcessDesigner.PPLibService.Portal

import org.osgi.framework.BundleContext

import aQute.bnd.annotation.component.Activate
import aQute.bnd.annotation.component.Component
import ch.uzh.ifi.pdeboer.pplib.hcomp.mturk._
import crowdProcessDesigner.interfaces.Portal
import crowdProcessDesigner.interfaces.ServiceInformation

@Component
class MechanicalTurkPortal extends AbstractPortalBuilderService[MechanicalTurkPortalBuilder]( {() => new MechanicalTurkPortalBuilder() }) with Portal {

  val allParams = inputParams + (portalBuilderCtor().SANDBOX -> Seq(true, false))
  
  def getInformation(): Seq[ServiceInformation] = {
    Seq(new ServiceInformation(classOf[MechanicalTurkPortal].getCanonicalName(), "Mechanical Turk Portal Configuration", allParams))
  }

  @Activate
  def start(context: BundleContext): Unit = {}
}