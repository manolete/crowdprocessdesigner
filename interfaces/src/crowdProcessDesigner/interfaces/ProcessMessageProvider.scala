package crowdProcessDesigner.interfaces

import java.util.UUID

/** Defines methods that the instance may service as channel to send events from a process*/
trait ProcessMessageProvider {
  /**
   * sends the given message
   * @param message the message to send
   */
  def sendMessage(message: String): Unit

  /**
   * sends the given event
   * @param event the [[crowdProcessDesigner.interfaces.Event]] to send
   */
  def sendEvent(event: Event): Unit

  /**
   * sends a new event for the given parameters, the msg will be the label of the provided taskObject
   * @param obj the taskObject to identify the event uniquely
   * @param recombinationBranchIndexes the Seq of tuples to identify the event within the execution tree
   * @param eventType a string to identify the event (see [[crowdProcessDesigner.interfaces.EventType]])
   * @param data content of the message
   */
  def sendEvent(obj: TaskObject,
    recombinationBranchIndexes: Option[Seq[Tuple2[Int, Int]]],
    eventType: String,
    data: Any)

  /**
   * Returns an unique identifier of this process within the execution environment
   */
  def getProcessId(): UUID
}
