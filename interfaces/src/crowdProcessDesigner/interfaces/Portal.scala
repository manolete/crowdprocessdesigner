package crowdProcessDesigner.interfaces

/** Service trait which defines a service that may return portal instances */
trait Portal extends ServiceProvider {
  /**
   * Returns the configured portal
   *  @param params map containing the configuration parameters provided by the process
   */
  def getPortal(params: Map[String, String]): Any
}