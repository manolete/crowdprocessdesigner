package crowdProcessDesigner.interfaces

/**
 * A serializable event class
 *  Running processes may send events of this type
 *
 * @constructor Creates a new event
 * @param taskObjectId id of the taskObject in the process
 * @param taskType type of the taskObject, e.g. HCT or Loop
 * @param recombinationBranchIndexes list of tuples denoting (taskObjectId, serviceId) to uniquely identify the branch of the execution tree
 * @param eventType type of the event
 * @param msg place for content, usually the label of the taskObject
 * @param data the responsible data, e.g. a time stamp or costs
 */
case class Event(
  taskObjectId: Option[Int],
  taskType: Option[String],
  recombinationBranchIndexes: Option[Seq[Tuple2[Int, Int]]],
  eventType: String,
  msg: Option[String],
  data: Any)

/** Object containing commonly used predefined event types */
object EventType {
  val ProcessStart = "ProcessStart"
  val ProcessFinished = "ProcessFinished"
  val ProcessFailed = "ProcessFailed"
  val TaskStart = "TaskStart"
  val TaskFinished = "TaskFinished"
  val TaskFailed = "TaskFailed"
  val TaskCost = "TaskCost"
  val Result = "Result"
  val TaskSettings = "TaskSettings"
  val ProcessEvent = "ProcessEvent" // general event for SocketIO
}
