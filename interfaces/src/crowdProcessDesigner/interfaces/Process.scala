package crowdProcessDesigner.interfaces

/**
 * Crowd Process Definition Class. Contains all the needed properties to run a process. The naming derives from the fabricJS canvas from the IDE from which it may deserialize.
 *
 * @param objects Seq of all tasks of the process
 * @param portalServices Seq of all configuration parameters of configured portals
 */
case class Process(
  objects: Seq[TaskObject],
  portalServices: Option[Seq[Seq[InputParameter]]])

/**
 * Single Task Object in the Process.
 *
 * @param id Unique Id of the Task within the Process
 * @param label Displayed name of the task
 * @param services Seq of Recombination Indexes containing all input parameters
 * @param connectedToIds Seq of ids of the next connected taskObjects
 * @param innerConnectedToIds: Seq of ids of the next inner connected taskObjects (for the Loop construct)
 * @param `type` type of the TaskObject as defined in [[crowdProcessDesigner.interfaces.TaskObjectTypes]]
 */
case class TaskObject(
  id: Option[Int],
  label: Option[String],
  services: Option[Seq[Seq[InputParameter]]] = None,
  connectedToIds: Option[Seq[Int]] = None,
  innerConnectedToIds: Option[Seq[Int]] = None,
  `type`: Option[String])

/**
 * Key/Value which deserializes from a JQuery Form Post.
 *
 * @param name key of the input parameter
 * @param value value of the parameter
 */
case class InputParameter(name: String, value: String)

/** Object containing all currently supported taskObjectTypes */
object TaskObjectTypes {
  val DataFlow = "DataFlow"
  val HumanComputationTask = "HCT"
  val MachineComputationTask = "MCT"
  val Union = "Union"
  val IfElse = "IfElse"
  val Input = "File"
  val Divide = "Divide"
  val Process = "Process"
  val Loop = "Loop"
  val Split = "Split"
  val Combine = "Combine"
}
