package crowdProcessDesigner.interfaces

/** Trait for de-/serialization methods */
trait Serializer {
  def serialize[T](json: T): String
  def deserialize[T](json: String, classType: Class[T]): T
}
