package crowdProcessDesigner.interfaces

/**
 * Service class containing an own set of input parameters to display in the IDE
 *
 * @param name Displayed name of the service
 * @param description Service description
 * @param inputParams Map of all input parameters needed for the service to run
 */
case class ServiceInformation(
  name: String,
  description: String,
  inputParams: Map[String, Any])

/**
 * Wrapper class for a OSGI Bundle containing several services
 *
 * @param information the ServiceInformation provided by the class
 * @param className Qualified className of the class which runs the service
 */
case class BundleInformation(
  information: ServiceInformation,
  className: String)
