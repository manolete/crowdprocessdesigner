package crowdProcessDesigner.interfaces

/** A chunk defining a part of the data (e.g. a paragraph of a text) */
class ProcessDataChunk extends Serializable {
  var chunkIds: Seq[Int] = Seq.empty
  /** the data to modify within the process */
  var data: Any = None
  /** context of the data, e.g. the original text or audio file */
  var context: Any = None
  /** variable to show whether a chunk has been verified */
  var isCorrectData: Option[Boolean] = None

  /**
   * Creates a new ProcessDataChunk
   * @param chunkIds Seq of ids that are contained in this chunk
   * @param data the content of the chunk
   * @param context the original data of the chunk before modifications
   */
  def this(chunkIds: Seq[Int], data: Any, context: Any) {
    this()
    this.chunkIds = chunkIds
    this.data = data
    this.context = context
  }

  /**
   * Creates a new ProcessDataChunk from an existing chunk (e.g. for splitting or multiplication)
   * @param chunk the original chunk
   * @param data the content of the new chunk
   * @param context the original data of the chunk before modifications (should usually be the same as existing chunk)
   */
  def this(chunk: ProcessDataChunk, data: Any, context: Any) {
    this()
    this.chunkIds = chunk.chunkIds
    this.data = data
    this.context = context
  }
}