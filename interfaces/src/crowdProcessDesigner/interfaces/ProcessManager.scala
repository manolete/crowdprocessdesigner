package crowdProcessDesigner.interfaces

/**
 *  Service providing capabilities to run a process
 */
trait ProcessManager {
  /**
   * Runs the given process
   * @param x the process to run
   * @param processMessageProvider the channel to send process events
   */
  def run(x: Process, processMessageProvider: ProcessMessageProvider): Unit
}