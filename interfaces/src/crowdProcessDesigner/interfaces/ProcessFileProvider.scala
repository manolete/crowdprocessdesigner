package crowdProcessDesigner.interfaces

/**
 * A file may not have a clear uri to access within OSGI
 *  A ProcessFileProvider provides an uri
 */
trait ProcessFileProvider {
  /**
   * Provides the location of a process artifact (usually a file)
   * @param processId: The id of the process execution
   * @param recombinationBranchIndex A string of concatenated branch indices
   * @param fileName the filename as provided in the taskObject service configuration
   */
  def getLinkToFile(processId: String, recombinationBranchIndex: String, fileName: String): String
}
