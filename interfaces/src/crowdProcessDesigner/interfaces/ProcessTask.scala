package crowdProcessDesigner.interfaces

/**
 * Trait that an instance offers services consumed by a process
 */
trait ServiceProvider {
  /** Returns all consumable services */
  def getInformation(): Seq[ServiceInformation]
}

/**
 * Trait defining that the instance provides data processing capabilities
 * @param <I> input type
 * @param <T> output / return type
 */
trait ServiceTask[I, T] extends ServiceProvider {
  /**
   * Process and return the data
   * @param input data
   * @param previousDiscardedInput data which is not in the active processing set but may be relevant to the process
   * @param recombinationIndexes Seq of tuples (taskObjectId, serviceConfigurationId) identifying the branch of the execution tree
   * @param params the map of configuration parameters for this service
   * @param portalConfigs map containing the configuration parameters for the defined portals from the process
   * @param processMessageProvider channel to send events from the service
   * @param taskObject the taskObject (needed to send events)
   */
  def process(
    input: I,
    previousDiscardedInput: I,
    recombinationIndexes: Seq[Tuple2[Int, Int]],
    params: Map[String, String],
    portalConfigs: Map[String, Map[String, String]],
    processMessageProvider: ProcessMessageProvider,
    taskObject: TaskObject): Tuple2[T, T]
}

/** Defines a task which has only one data flow as input and one as output */
trait SingleInputSingleOutputTask extends ServiceTask[Seq[Any], Seq[Any]]
/** Defines a task which has multiple connected input dataFlows but only one output */
trait MultipleInputSingleOutputTask extends ServiceTask[Seq[Seq[Any]], Seq[Any]]
/** Defines a task with one connected input dataFlow but multiple output dataFlows */
trait SingleInputMultipleOutputTask extends ServiceTask[Seq[Any], Seq[Seq[Any]]]

/** Provides Human Computation Task capabilities */
trait HumanComputationTask extends SingleInputSingleOutputTask
/** Provides Machine Computation Task capabilities */
trait MachineComputationTask extends SingleInputSingleOutputTask
/** Provides File Task capabilities (writing or reading uris) */
trait FileTask extends SingleInputSingleOutputTask
/** Provides Divide Task capabilities splitting the input into more chunks */
trait DivideTask extends SingleInputSingleOutputTask
/** Provides Process Task capabilities for non-trivial processes as defined in PPLib */
trait ProcessTask extends SingleInputSingleOutputTask
/** Provides Union Task capabilities for joining multiple inputs */
trait UnionTask extends MultipleInputSingleOutputTask
/** Provides IfElse Task capabilities */
trait IfElseTask extends SingleInputMultipleOutputTask
/** Provides Split Task capabilities splitting the input among multiple outputs */
trait SplitTask extends SingleInputMultipleOutputTask
/** Provides Combine Task capabilities reverting the divide option joining multiple chunks together */
trait CombineTask extends SingleInputSingleOutputTask
/** Providing the loop construct, decides over which data should be looped again */
trait LoopTask extends SingleInputSingleOutputTask


