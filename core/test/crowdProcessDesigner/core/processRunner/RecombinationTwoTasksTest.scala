package crowdProcessDesigner.core.processRunner

import org.scalatest.junit.AssertionsForJUnit
import scala.collection.mutable.ListBuffer
import org.junit.Assert._
import org.junit.Test
import org.junit.Before
import crowdProcessDesigner.interfaces._
import org.scalatest.junit.JUnitSuite
import org.scalatest.mock.MockitoSugar._
import org.mockito.Mockito.stub
import org.mockito.Matchers.any
import org.junit.Test
import org.junit.Before
import org.mockito.Mockito.{ when, doCallRealMethod }
import scala.concurrent._
import scala.concurrent.duration._
import scala.concurrent.Future
import ExecutionContext.Implicits.global
import scala.collection.JavaConversions._

class RecombinationTwoTasksTest extends AssertionsForJUnit {

  val recombinationServices = Seq(Seq(new InputParameter("service", "1,MyService")), Seq(new InputParameter("service", "2,MyService")))
  val singleService = Seq(Seq(new InputParameter("service", "1,MyService")))

  val task1 = new TaskObject(Some(1), Some("MyLabel"), Some(recombinationServices), Some(Seq(2)), None, Some(TaskObjectTypes.MachineComputationTask))
  val dataFlow2 = new TaskObject(Some(2), Some("MyLabel"), None, Some(Seq(3)), None, Some(TaskObjectTypes.DataFlow))
  val task3 = new TaskObject(Some(3), Some("MyLabel"), Some(recombinationServices), Some(Seq(4)), None, Some(TaskObjectTypes.MachineComputationTask))
  val dataFlow4 = new TaskObject(Some(4), Some("MyLabel"), None, Some(Seq(5)), None, Some(TaskObjectTypes.DataFlow))
  val task5 = new TaskObject(Some(5), Some("MyLabel"), Some(singleService), None, None, Some(TaskObjectTypes.Input))
  val tasks = List(task1, dataFlow2, task3, dataFlow4, task5)
  val process = Process(tasks, None)

  val processMessageProvider = MockHelpers.getMessageProviderMock()

  val processRunner = MockHelpers.getProcessRunner(process)

  @Test def contains3Elements() {
    assertEquals(process.objects.length, 5)
  }

  @Test def returnsCorrectNumberOfRecombinationsWithBranchIndex() {
    MockHelpers.fileTask.results.clear()
    val resultFutures = processRunner.run()

    assertEquals(2, resultFutures.length) // Two Starting Recombinations

    val futureRecombination1 = Await.result(resultFutures(0), 10 seconds)
    val futureRecombination2 = Await.result(resultFutures(1), 10 seconds)

    assertEquals(4, MockHelpers.fileTask.branchIndexes.length) // 2 * 2

    MockHelpers.fileTask.results.iterator().toSeq.foreach { result =>
      assertEquals(result, Seq("TaskTask"))
    }
  }
}