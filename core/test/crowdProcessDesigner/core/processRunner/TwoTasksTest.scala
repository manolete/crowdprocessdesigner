package crowdProcessDesigner.core.processRunner

import org.scalatest.junit.AssertionsForJUnit

import scala.collection.mutable.ListBuffer

import org.junit.Assert._
import org.junit.Test
import org.junit.Before

import crowdProcessDesigner.interfaces._

import org.scalatest.junit.JUnitSuite
import org.scalatest.mock.MockitoSugar._
import org.mockito.Mockito.stub
import org.mockito.Matchers.any
import org.junit.Test
import org.junit.Before
import org.mockito.Mockito.{ when, doCallRealMethod }

import scala.concurrent._
import scala.concurrent.duration._

class TwoTasksTest extends AssertionsForJUnit {

  val services = Seq(Seq(new InputParameter("service", "1,MyService")))

  val task1 = new TaskObject(Some(1), Some("MyLabel"), Some(services), Some(Seq(2)), None, Some(TaskObjectTypes.MachineComputationTask))
  val dataFlow1 = new TaskObject(Some(2), Some("MyLabel"), None, Some(Seq(3)), None, Some(TaskObjectTypes.DataFlow))
  val task2 = new TaskObject(Some(3), Some("MyLabel"), Some(services), Some(Seq(4)), None, Some(TaskObjectTypes.MachineComputationTask))
  val tasks = List(task1, dataFlow1, task2)
  val process = Process(tasks, None)

  val processRunner: ProcessRunner = MockHelpers.getProcessRunner(process)
  val processMessageProvider: ProcessMessageProvider = MockHelpers.getMessageProviderMock()

  @Test def contains3Elements() { // Uses JUnit-style assertions
    assertEquals(process.objects.length, 3)
  }

  @Test def returnsCorrectProcessedString() {
    val solution = List("TaskTask")

    val resultFutures = processRunner.run()

    assertEquals(1, resultFutures.length)

    val resultFuture = resultFutures(0)
    Await.result(resultFuture, 5 seconds)
    val result = MockHelpers.stringDoublerTask.results.get(MockHelpers.stringDoublerTask.results.size() - 1)

    assertEquals(solution, result)
  }
}