package crowdProcessDesigner.core.processRunner

import org.scalatest.junit.AssertionsForJUnit

import scala.collection.mutable.ListBuffer

import org.junit.Assert._
import org.junit.Test
import org.junit.Before

import crowdProcessDesigner.interfaces._

import org.scalatest.junit.JUnitSuite
import org.scalatest.mock.MockitoSugar._
import org.mockito.Mockito.stub
import org.mockito.Matchers.any
import org.junit.Test
import org.junit.Before
import org.mockito.Mockito.{ when, doCallRealMethod }

import scala.concurrent._
import scala.concurrent.duration._

class SimpleLoopTest extends AssertionsForJUnit {

  val services = Seq(Seq(new InputParameter("service", "1,MyService")))

  val task5 = new TaskObject(Some(5), Some("MCT"), Some(services), Some(Seq(6)), None, Some(TaskObjectTypes.MachineComputationTask))
  val dataFlow6 = new TaskObject(Some(6), Some("dataFlow2"), None, Some(Seq(1)), None, Some(TaskObjectTypes.DataFlow))

  val task1 = new TaskObject(Some(1), Some("Loop"), Some(services), Some(Seq(7)), Some(Seq(2)), Some(TaskObjectTypes.Loop))
  val dataFlow2 = new TaskObject(Some(2), Some("dataFlow2"), None, Some(Seq(3)), None, Some(TaskObjectTypes.DataFlow))
  val task3 = new TaskObject(Some(3), Some("MCT"), Some(services), Some(Seq(4)), None, Some(TaskObjectTypes.MachineComputationTask))
  val dataFlow4 = new TaskObject(Some(4), Some("dataFlow4"), None, None, Some(Seq(1)), Some(TaskObjectTypes.DataFlow))

  val dataFlow7 = new TaskObject(Some(7), Some("dataFlow2"), None, Some(Seq(8)), None, Some(TaskObjectTypes.DataFlow))
  val task8 = new TaskObject(Some(8), Some("MCT"), Some(services), None, None, Some(TaskObjectTypes.MachineComputationTask))

  val tasks = List(task1, dataFlow2, task3, dataFlow4, task5, dataFlow6, dataFlow7, task8)
  val process = Process(tasks, None)

  val processMessageProvider = MockHelpers.getMessageProviderMock()

  val processRunnerSimpleLoop = MockHelpers.getProcessRunner(process, true)

  @Test def returnsCorrectProcessedString() {
    val solution = Seq("Task", "TaskTaskTaskTask") // one input MCT one output MCT + 3 times loop

    val resultFutures = processRunnerSimpleLoop.run()

    assertEquals(1, resultFutures.length)

    val resultFuture = resultFutures(0)
    Await.result(resultFuture, 120 seconds)
    val result = MockHelpers.stringDoublerTask.lastResult
    
    assert(result != None)
    
    result.map { case (activeSet, inactiveSet) =>
      assertEquals(solution, activeSet.union(inactiveSet))
    }
  }
}