package crowdProcessDesigner.core.processRunner

import org.scalatest.junit.AssertionsForJUnit

import scala.collection.mutable.ListBuffer

import org.junit.Assert._
import org.junit.Test
import org.junit.Before

import crowdProcessDesigner.interfaces._

import org.scalatest.junit.JUnitSuite
import org.scalatest.mock.MockitoSugar._
import org.mockito.Mockito.stub
import org.mockito.Matchers.any
import org.junit.Test
import org.junit.Before
import org.mockito.Mockito.{ when, doCallRealMethod }

import scala.concurrent._
import scala.concurrent.duration._

class IfElseTaskTest extends AssertionsForJUnit {

  val services = Seq(Seq(new InputParameter("service", "1,MyService")))

  val task1 = new TaskObject(Some(1), Some("Task1"), Some(services), Some(Seq(2)), None, Some(TaskObjectTypes.MachineComputationTask))
  val dataFlow2 = new TaskObject(Some(2), Some("dataFlow2"), None, Some(Seq(3)), None, Some(TaskObjectTypes.DataFlow))
  val task3 = new TaskObject(Some(3), Some("IfElseTask"), Some(services), Some(Seq(4, 6)), None, Some(TaskObjectTypes.IfElse))
  val dataFlow4 = new TaskObject(Some(4), Some("dataFlow4"), None, Some(Seq(5)), None, Some(TaskObjectTypes.DataFlow))
  val task5 = new TaskObject(Some(5), Some("Task5"), Some(services), None, None, Some(TaskObjectTypes.MachineComputationTask))
  val dataFlow6 = new TaskObject(Some(6), Some("dataFlow6"), None, Some(Seq(7)), None, Some(TaskObjectTypes.DataFlow))
  val task7 = new TaskObject(Some(7), Some("Task7"), Some(services), Some(Seq(8)), None, Some(TaskObjectTypes.MachineComputationTask))
  val dataFlow8 = new TaskObject(Some(8), Some("dataFlow8"), None, Some(Seq(9)), None, Some(TaskObjectTypes.DataFlow))
  val task9 = new TaskObject(Some(9), Some("Task9"), Some(services), None, None, Some(TaskObjectTypes.MachineComputationTask))

  val tasks = List(task1, dataFlow2, task3, dataFlow4, task5, dataFlow6, task7, dataFlow8, task9)
  val process = Process(tasks, None)

  val processMessageProvider = MockHelpers.getMessageProviderMock()

  val processRunnerWithIfElseTrue = MockHelpers.getProcessRunner(process, true)
  val processRunnerWithIfElseFalse = MockHelpers.getProcessRunner(process, false)

  @Test def returnsCorrectProcessedStringIfElseTrue() {

    val solution = Seq.empty

    MockHelpers.stringDoublerTask.results.clear()
    MockHelpers.ifElseTask.setValue(true)

    val resultFutures = processRunnerWithIfElseTrue.run()

    assertEquals(1, resultFutures.length)

    val resultFuture = resultFutures(0)
    Await.result(resultFuture, 120 seconds)

    val exists = MockHelpers.stringDoublerTask.results.contains(Seq("TaskTask"))
    assert(exists)

    val notExists = MockHelpers.stringDoublerTask.results.contains(Seq("TaskTaskTask"))
    assert(!notExists)
  }

  @Test def returnsCorrectProcessedStringIfElseFalse() {
    val solution = Seq("TaskTaskTask")

    MockHelpers.stringDoublerTask.results.clear()
    MockHelpers.ifElseTask.setValue(false)

    val resultFutures = processRunnerWithIfElseFalse.run()

    assertEquals(1, resultFutures.length)

    val resultFuture = resultFutures(0)
    Await.result(resultFuture, 120 seconds)

    val exists = MockHelpers.stringDoublerTask.results.contains(Seq("TaskTaskTask"))

    assert(exists)
  }
}