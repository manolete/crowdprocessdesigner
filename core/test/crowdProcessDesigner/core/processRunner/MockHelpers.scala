package crowdProcessDesigner.core.processRunner

import org.scalatest.junit.AssertionsForJUnit

import scala.collection.mutable.ListBuffer

import org.junit.Assert._
import org.junit.Test
import org.junit.Before

import crowdProcessDesigner.interfaces._

import org.scalatest.junit.JUnitSuite
import org.scalatest.mock.MockitoSugar._
import org.mockito.Mockito.stub
import org.mockito.Matchers._
import org.junit.Test
import org.junit.Before
import org.mockito.Mockito.{ when, doCallRealMethod }

import scala.concurrent._
import scala.concurrent.duration._
import crowdProcessDesigner.core.processRunner.utils._
import crowdProcessDesigner.core.testTasks._

object MockHelpers {
  val loopTask = new Loop3TimesTask()
  val stringDoublerTask = new StringDoublerTask()
  val unionCombinatorTask = new UnionCombinatorTask()
  val ifElseTask = new IfElseTaskReturnsValue()
  val fileTask = new FileTestTask()
  
  def getProcessRunner(process: Process, ifElseTaskReturn: Boolean = true): ProcessRunner = {
    val processHelper = new ProcessHelper(process)
    
    val serviceHelper = mock[ServiceHelper]
    when(serviceHelper.getLoopTaskService(any())).thenReturn(loopTask)
    when(serviceHelper.getMachineComputationTaskService(any())).thenReturn(stringDoublerTask)
    when(serviceHelper.getUnionTaskService(any())).thenReturn(unionCombinatorTask)
    when(serviceHelper.getIfElseTaskService(any())).thenReturn(ifElseTask)
    when(serviceHelper.getFileTask(any())).thenReturn(fileTask)
    
    new ProcessRunner(process, getMessageProviderMock(), serviceHelper)
  }
  
  def getMessageProviderMock() : ProcessMessageProvider = {
    mock[ProcessMessageProvider]
  }
}