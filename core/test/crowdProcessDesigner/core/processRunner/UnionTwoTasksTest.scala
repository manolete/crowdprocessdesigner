package crowdProcessDesigner.core.processRunner

import org.scalatest.junit.AssertionsForJUnit

import scala.collection.mutable.ListBuffer

import org.junit.Assert._
import org.junit.Test
import org.junit.Before

import crowdProcessDesigner.interfaces._

import org.scalatest.junit.JUnitSuite
import org.scalatest.mock.MockitoSugar._
import org.mockito.Mockito.stub
import org.mockito.Matchers.any
import org.junit.Test
import org.junit.Before
import org.mockito.Mockito.{ when, doCallRealMethod }

import scala.concurrent._
import scala.concurrent.duration._

class UnionTwoTasksTest extends AssertionsForJUnit {

  val services = Seq(Seq(new InputParameter("service", "1,MyService")))

  // Task with two connected output ids
  val task1 = new TaskObject(Some(1), Some("MyLabel"), Some(services), Some(Seq(2, 3)), None, Some(TaskObjectTypes.MachineComputationTask))

  val dataFlow2 = new TaskObject(Some(2), Some("MyLabel"), None, Some(Seq(4)), None, Some(TaskObjectTypes.DataFlow))
  val task4 = new TaskObject(Some(4), Some("MyLabel"), Some(services), Some(Seq(11)), None, Some(TaskObjectTypes.MachineComputationTask))
  val dataFlow11 = new TaskObject(Some(11), Some("MyLabel"), None, Some(Seq(12)), None, Some(TaskObjectTypes.DataFlow))
  val task12 = new TaskObject(Some(12), Some("MyLabel"), Some(services), Some(Seq(6)), None, Some(TaskObjectTypes.MachineComputationTask))
  val dataFlow6 = new TaskObject(Some(6), Some("MyLabel"), None, Some(Seq(8)), None, Some(TaskObjectTypes.DataFlow))

  val dataFlow3 = new TaskObject(Some(3), Some("MyLabel"), None, Some(Seq(5)), None, Some(TaskObjectTypes.DataFlow))
  val task5 = new TaskObject(Some(5), Some("MyLabel"), Some(services), Some(Seq(7)), None, Some(TaskObjectTypes.MachineComputationTask))
  val dataFlow7 = new TaskObject(Some(7), Some("MyLabel"), None, Some(Seq(8)), None, Some(TaskObjectTypes.DataFlow))

  val task8 = new TaskObject(Some(8), Some("MyLabel"), Some(services), Some(Seq(9)), None, Some(TaskObjectTypes.Union))
  val dataFlow9 = new TaskObject(Some(9), Some("MyLabel"), None, Some(Seq(10)), None, Some(TaskObjectTypes.DataFlow))
  val task10 = new TaskObject(Some(10), Some("MyLabel"), Some(services), None, None, Some(TaskObjectTypes.Input))

  val tasks = List(task1, dataFlow2, dataFlow3, task4, task5, dataFlow6, dataFlow7, task8, dataFlow9, task10, dataFlow11, task12)
  val process = Process(tasks, None)

  val processMessageProvider = MockHelpers.getMessageProviderMock()

  val processRunner = MockHelpers.getProcessRunner(process)

  @Test def contains10Elements() {
    assertEquals(process.objects.length, 12)
  }

  @Test def returnsCorrectProcessedString() {
    val solution = List("TaskTaskTask", "TaskTask")

    val resultFutures = processRunner.run()

    assertEquals(1, resultFutures.length)

    val resultFuture = resultFutures(0)
    Await.result(resultFuture, 120 seconds)

    val result = MockHelpers.fileTask.results.get(MockHelpers.fileTask.results.size() - 1)

    assertEquals(solution, result)
  }
}