package crowdProcessDesigner.core.processRunner

import org.scalatest.junit.AssertionsForJUnit

import scala.collection.mutable.ListBuffer

import org.junit.Assert._
import org.junit.Test
import org.junit.Before

import crowdProcessDesigner.interfaces._

import org.scalatest.junit.JUnitSuite
import org.scalatest.mock.MockitoSugar._
import org.mockito.Mockito.stub
import org.mockito.Matchers.any
import org.junit.Test
import org.junit.Before
import org.mockito.Mockito.{ when, doCallRealMethod }

import scala.concurrent._
import scala.concurrent.duration._

class SingleTaskTest extends AssertionsForJUnit {

  val services = Seq(Seq(new InputParameter("service", "1,MyService")))

  val task1 = new TaskObject(Some(1), Some("MyLabel"), Some(services), None, None, Some(TaskObjectTypes.MachineComputationTask))
  val tasks = List(task1)
  val process = Process(tasks, None)

  val processMessageProvider = MockHelpers.getMessageProviderMock()

  val processRunner = MockHelpers.getProcessRunner(process)

  @Test def returnsCorrectProcessedString() {
    val solution = List("Task")

    val resultFutures = processRunner.run()
    assertEquals(1, resultFutures.length)

    val resultFuture = resultFutures(0)
    Await.result(resultFuture, 120 seconds)

    val result = MockHelpers.stringDoublerTask.results.get(MockHelpers.stringDoublerTask.results.size() - 1)

    assertEquals(solution, result)
  }
}