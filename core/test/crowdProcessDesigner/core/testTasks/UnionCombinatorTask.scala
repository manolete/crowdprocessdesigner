package crowdProcessDesigner.core.testTasks

import crowdProcessDesigner.interfaces._

class UnionCombinatorTask extends UnionTask {
  
  val results = java.util.Collections.synchronizedList(new java.util.ArrayList[Seq[Any]]())
  
  def process(input: Seq[Seq[Any]], previouslyDiscardedInput: Seq[Seq[Any]], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    val lastResult = input.flatMap { x => x }
    results.add(lastResult)
    (lastResult, previouslyDiscardedInput.flatMap { x => x })
  }

  def getInformation() = Seq.empty
}