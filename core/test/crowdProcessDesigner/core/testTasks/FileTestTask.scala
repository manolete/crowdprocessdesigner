package crowdProcessDesigner.core.testTasks

import crowdProcessDesigner.interfaces._

class FileTestTask extends FileTask {
  val results = java.util.Collections.synchronizedList(new java.util.ArrayList[Seq[Any]]())

  var branchIndexes: Seq[Seq[Tuple2[Int, Int]]] = Seq.empty

  def process(input: Seq[Any], previouslyDiscardedInput: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    results.add(input)
    branchIndexes = branchIndexes :+ recombinationBranchindex
    (input, previouslyDiscardedInput)
  }

  def getInformation() = Seq.empty
}