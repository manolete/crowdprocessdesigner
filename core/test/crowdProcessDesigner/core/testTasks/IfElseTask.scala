package crowdProcessDesigner.core.testTasks

import crowdProcessDesigner.interfaces._

class IfElseTaskReturnsValue() extends IfElseTask {
  var value: Boolean = true
  var results: Seq[Seq[Seq[Any]]] = Seq.empty

  def process(input: Seq[Any], previouslyDiscardedInput: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Seq[Any]], Seq[Seq[Any]]] = {
    if (this.value) {
      results = results :+ Seq(input, Seq.empty)
    } else {
      results = results :+ Seq(Seq.empty, input)
    }
    (results.last, Seq(Seq.empty, Seq.empty))
  }

  def setValue(value: Boolean) = {
    this.value = value
  }

  def getInformation() = Seq.empty
}