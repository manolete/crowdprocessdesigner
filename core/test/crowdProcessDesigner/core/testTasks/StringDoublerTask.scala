package crowdProcessDesigner.core.testTasks

import crowdProcessDesigner.interfaces._

class StringDoublerTask extends MachineComputationTask {

  val results = java.util.Collections.synchronizedList(new java.util.ArrayList[Seq[Any]]())

  var lastResult: Option[Tuple2[Seq[Any], Seq[Any]]] = None

  def process(input: Seq[Any], previouslyDiscardedInput: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    var result: Seq[String] = Seq.empty
    if (input.nonEmpty) {
      result = input.map(i => i + "Task")
      results.add(result)
    } else {
      result = Seq("Task")
      results.add(result)
    }
    
    lastResult = Some(result, previouslyDiscardedInput)
    lastResult.getOrElse(throw new Exception())
  }

  def getInformation() = Seq.empty
}