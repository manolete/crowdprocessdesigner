package crowdProcessDesigner.core.testTasks

import crowdProcessDesigner.interfaces._

class Loop3TimesTask extends LoopTask {
  var loopCounter = 0;

  def process(input: Seq[Any], previouslyDiscardedInput: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    loopCounter += 1

    if (loopCounter > 3) {
      (Seq.empty, input.union(previouslyDiscardedInput))
    } else {
      (input, previouslyDiscardedInput)
    }
  }

  def getInformation() = Seq.empty
}