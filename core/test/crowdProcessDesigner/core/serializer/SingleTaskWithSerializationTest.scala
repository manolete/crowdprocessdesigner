package crowdProcessDesigner.core.serializer

import org.scalatest.junit.AssertionsForJUnit
import scala.collection.mutable.ListBuffer
import org.junit.Assert._
import org.junit.Test
import org.junit.Before
import crowdProcessDesigner.interfaces._
import org.scalatest.junit.JUnitSuite
import org.scalatest.mock.MockitoSugar._
import org.mockito.Mockito.stub
import org.mockito.Matchers.any
import org.junit.Test
import org.junit.Before
import org.mockito.Mockito.{ when, doCallRealMethod }
import scala.concurrent._
import scala.concurrent.duration._
import crowdProcessDesigner.core.processRunner._

class SingleTaskWithSerializationTest extends AssertionsForJUnit {

  val serializer = new JsonSerializer()

  val source = scala.io.Source.fromFile("test/crowdProcessDesigner/core/resources/SingleTask.json")
  val lines = source.mkString
  source.close()

  val process = serializer.deserialize(lines, classOf[Process])

  val processRunner = MockHelpers.getProcessRunner(process)

  @Test def returnsCorrectProcessedString() {
    val solution = (List("Task"), List())

    val resultFutures = processRunner.run()
    val resultFuture = resultFutures(0)
    val result = Await.result(resultFuture, 120 seconds)

    assertEquals(solution, result)
  }
}