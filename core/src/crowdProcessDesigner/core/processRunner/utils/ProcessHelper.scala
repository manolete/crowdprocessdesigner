package crowdProcessDesigner.core.processRunner.utils

import crowdProcessDesigner.interfaces.Process
import crowdProcessDesigner.interfaces.TaskObject
import crowdProcessDesigner.interfaces.TaskObjectTypes

/**Provider helper options to traverse the process */
class ProcessHelper(process: Process) {

  def getRelevantTaskObjects(): Seq[TaskObject] = {
    process.objects
      .map(o => Option(o))
      .flatten
      .filter(o => o.id.isDefined)
  }

  /** Returns TaskObjects without Input */
  def getStartTaskObjects(): Seq[TaskObject] = {

    val taskObjects = getRelevantTaskObjects()

    // Get Start objects
    val allConnectedToIds: Seq[Int] = taskObjects
      .flatMap(o => o.connectedToIds)
      .flatten

    taskObjects
      .filter(p => p.id.isDefined)
      .filter(o => !allConnectedToIds.contains(o.id.getOrElse(-1)))
      .filter(o => o.`type` != Some(TaskObjectTypes.DataFlow))
  }

  /**
   * Returns the connected taskObjects for the given taskObject
   *  @param obj the taskObject to get its connections
   */
  def getConnectedTasks(obj: TaskObject): Seq[TaskObject] = {
    val connectedDataFlows = getRelevantTaskObjects()
      .filter(o => obj.connectedToIds.getOrElse(Seq.empty).contains(o.id.get))

    val connectedTaskObjects: Seq[TaskObject] = connectedDataFlows.map { connectedDataFlow =>
      val connectedToIds = connectedDataFlow.connectedToIds.getOrElse(Seq.empty).union(connectedDataFlow.innerConnectedToIds.getOrElse(Seq.empty))
      getRelevantTaskObjects()
        .find(o => connectedToIds.contains(o.id.getOrElse(-1)))
    }
      .flatMap { x => x }

    connectedTaskObjects
  }

  /**
   * Returns the inner connected taskObjects for the given taskObject (for the loop construct)
   *  @param obj the taskObject to get its inner connections
   */
  def getInnerConnectedTasks(obj: TaskObject): Seq[TaskObject] = {
    val connectedDataFlows = getRelevantTaskObjects()
      .filter(o => obj.innerConnectedToIds.getOrElse(Seq.empty).contains(o.id.get))

    val connectedTaskObjects: Seq[TaskObject] =
      connectedDataFlows.flatMap { connectedDataFlow =>
        val connectedToIds = connectedDataFlow.connectedToIds.getOrElse(Seq.empty).union(connectedDataFlow.innerConnectedToIds.getOrElse(Seq.empty))
        getRelevantTaskObjects()
          .find(o => connectedToIds.contains(o.id.getOrElse(-1)))
      }
    connectedTaskObjects
  }

  /** Returns a map with the portalName as key and a map of the params as values */
  def getPortalConfiguration(): Map[String, Map[String, String]] = {

    val portalMap: Map[String, Map[String, String]] = process
      .portalServices.map { portalConfigs =>
        portalConfigs.map { portalConfig =>
          val params = portalConfig.map { x => (x.name, x.value) } toMap;
          (params.getOrElse("portal", throw new Exception("No portal config found")).split(",")(1) -> params);
        }.toMap
      }.getOrElse(Map.empty)

    portalMap
  }

  /**
   * Returns the service className for the given recombinationIndex
   * @param obj the taskObject
   * @param serviceIndex the recombination index of the desired service
   */
  def getServiceClassName(obj: TaskObject, serviceIndex: Int): String = {
    val classNameAndServiceName = obj.services.flatMap(x => x(serviceIndex).find(s => s.name == "service"))
    classNameAndServiceName.getOrElse(throw new Exception("Service classname not found"))
      .value
      .split(",")(0)
  }

  /**
   * Returns a seq of all recombination service configuration parameters
   * @param obj the relevant taskObject
   */
  def getServiceParams(obj: TaskObject): Seq[Map[String, String]] = {
    val recombinations: Option[Seq[Map[String, String]]] =
      obj.services.map { service =>
        service.map { params =>
          val bundleIdAndServiceName = params.find(s => s.name == "service").map(s => s.value)
          val serviceName = bundleIdAndServiceName.getOrElse(throw new Exception("Service name not found " + bundleIdAndServiceName))
            .split(",")(1)
          val paramMap = params.map { p => (p.name, p.value) } toMap;
          (paramMap + ("service" -> serviceName))
        }
      }

    recombinations.getOrElse(Seq.empty)
  }

  /**
   * Traverses the tree and gets the next taskObjects
   */
  def getNextTaskForType(connectedTaskObject: TaskObject, taskObjectType: String): Seq[TaskObject] = {
    val nextTasks: Seq[TaskObject] = getConnectedTasks(connectedTaskObject)
    val nextUnionTasks = nextTasks.filter(o => o.`type` == Some(taskObjectType))

    val foundDeeperUnionTasks = nextTasks.map { connectedTask =>
      val resultTasks = getNextTaskForType(connectedTask, taskObjectType)
      resultTasks.filter(o => o.`type` == Some(taskObjectType))
    }
      .flatten

    nextUnionTasks.union(foundDeeperUnionTasks)
  }

  /**
   *  When the next task is a union object, the current branch has to return to be joined together with other branches to union
   */
  def isReturnNeeded(obj: TaskObject) = {
    val connectedTaskObjects: Seq[TaskObject] = getConnectedTasks(obj)
    val innerConnectedTaskObjects: Seq[TaskObject] = getInnerConnectedTasks(obj)

    val taskObjects = connectedTaskObjects.union(innerConnectedTaskObjects)
    taskObjects.size == 0 || (
      taskObjects.size == 1 && taskObjects(0).`type` == Some(TaskObjectTypes.Union))
  }
}