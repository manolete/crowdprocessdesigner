package crowdProcessDesigner.core.processRunner.utils

import crowdProcessDesigner.interfaces._
import org.osgi.framework.BundleContext
import scala.reflect.ClassTag
import scala.reflect.runtime.universe._

/**Returns OSGI-related Bundles */
class ServiceHelper(context: BundleContext) {

  def getService[T: ClassTag](className: String): T = {
    val tClass = implicitly[ClassTag[T]].runtimeClass
    val filterString = "(component.Name=" + className + ")";
    val serviceReference = context.getServiceReferences(tClass.getName(), filterString);
    if (serviceReference.nonEmpty) {
      context.getService(serviceReference(0)).asInstanceOf[T]
    } else {
      throw new Exception("No service found for className " + className)
    }
  }

  // How to do this in unit tests?
  def getLoopTaskService(className: String): LoopTask = {
    getService[LoopTask](className);
  }
  def getMachineComputationTaskService(className: String): MachineComputationTask = {
    getService[MachineComputationTask](className);
  }

  def getHumanComputationTaskService(className: String): HumanComputationTask = {
    getService[HumanComputationTask](className);
  }

  def getUnionTaskService(className: String): UnionTask = {
    getService[UnionTask](className);
  }
  def getIfElseTaskService(className: String): IfElseTask = {
    getService[IfElseTask](className);
  }

  def getSplitTaskService(className: String): SplitTask = {
    getService[SplitTask](className);
  }

  def getFileTask(className: String): FileTask = {
    getService[FileTask](className)
  }

  def getProcessTask(className: String): ProcessTask = {
    getService[ProcessTask](className)
  }

  def getDivideTask(className: String): DivideTask = {
    getService[DivideTask](className)
  }

  def getCombineTask(className: String): CombineTask = {
    getService[CombineTask](className)
  }
}