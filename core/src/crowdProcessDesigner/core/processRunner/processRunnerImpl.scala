package crowdProcessDesigner.core.processRunner

import java.util.Calendar

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt

import crowdProcessDesigner.core.processRunner.utils.ProcessHelper
import crowdProcessDesigner.core.processRunner.utils.ServiceHelper
import crowdProcessDesigner.interfaces.EventType
import crowdProcessDesigner.interfaces.Process
import crowdProcessDesigner.interfaces.ProcessMessageProvider
import crowdProcessDesigner.interfaces.TaskObject
import crowdProcessDesigner.interfaces.TaskObjectTypes

/**
 * Runs a process object by creating and returning futures that process the data
 * @param process the process to run
 * @param processMessageProvider channel to send process events
 * @param serviceHelper Utility to return task services instances within the OSGI environment
 */
class ProcessRunner(process: Process, processMessageProvider: ProcessMessageProvider, serviceHelper: ServiceHelper) {

  val processHelper: ProcessHelper = new ProcessHelper(process)

  /** Builds and returns the futures */
  def run(): Seq[Future[Any]] = {
    // for all input objects create a process future

    // get all objects with no input and start the process for each project
    val startObjects = processHelper.getStartTaskObjects()
    val futureSeq: Seq[Future[Any]] = startObjects.map { startObject =>
      val recombinations = processHelper.getServiceParams(startObject)
      recombinations.zipWithIndex.map {
        case (params, index) =>
          val recombinationBranchindex = Seq((startObject.id.get, index))
          processFuture(startObject, recombinationBranchindex, getFutureOnlySingleInput(startObject, index, Seq.empty, Seq.empty, params, recombinationBranchindex))
      }
    }
      .flatMap { x => x }

    futureSeq
  }

  /** Recursive method that calculates remaining futures starting from the given taskObject */
  private[this] def processFuture(obj: TaskObject, recombinationBranchIndex: Seq[Tuple2[Int, Int]], currentFuture: Future[Tuple2[Any, Any]]): Future[Any] = {
    // The current branch needs return (e.g. Union or no connected elements)
    if (processHelper.isReturnNeeded(obj)) {
      currentFuture flatMap { output => Future.successful(output.asInstanceOf[Tuple2[Seq[Any], Seq[Any]]]) }
    } else {
      val connectedTaskObjects: Seq[TaskObject] = processHelper.getConnectedTasks(obj)
      val innerConnectedTaskObjects: Seq[TaskObject] = processHelper.getInnerConnectedTasks(obj)
      val nextTaskObjects = connectedTaskObjects.union(innerConnectedTaskObjects)

      // if there is just one future return it
      val futures: Seq[Future[Any]] =
        if (obj.`type` == Some(TaskObjectTypes.Loop)) {
          HandleLoop(currentFuture.asInstanceOf[Future[Tuple2[Seq[Any], Seq[Any]]]], obj, connectedTaskObjects.diff(innerConnectedTaskObjects), innerConnectedTaskObjects, recombinationBranchIndex)
        } else if (nextTaskObjects.length == 1) {
          HandleSingleConnection(currentFuture.asInstanceOf[Future[Tuple2[Seq[Any], Seq[Any]]]], nextTaskObjects(0), recombinationBranchIndex)
        } else if (obj.`type` == Some(TaskObjectTypes.IfElse)) {
          HandleIfElse(currentFuture.asInstanceOf[Future[Tuple2[Seq[Seq[Object]], Seq[Seq[Object]]]]], nextTaskObjects, recombinationBranchIndex)
        } else {
          HandleMultipleConnection(currentFuture.asInstanceOf[Future[Tuple2[Seq[Object], Seq[Object]]]], obj, nextTaskObjects, recombinationBranchIndex)
        }

      if (futures.length == 1) {
        // usually the future should be of length 1, then return it directly since Future.sequence nests the output 
        // --> union does not work correctly
        futures(0)
      } else {
        // in the case that there is no union operator however we might end up with multiple futures
        Future.sequence(futures)
      }
    }
  }

  private[this] def HandleLoop(currentFuture: Future[Tuple2[Seq[Any], Seq[Any]]], currentTaskObject: TaskObject, nextTaskObjects: Seq[TaskObject], nextInnerTaskObjects: Seq[TaskObject], recombinationBranchIndex: Seq[Tuple2[Int, Int]]) = {
    Seq(currentFuture.flatMap {
      case (activeSet, inactiveSet) =>
        if (activeSet.nonEmpty) {
          val updatedBranchIndex = recombinationBranchIndex :+ (nextInnerTaskObjects(0).id.get, 0)
          val nextParam = processHelper.getServiceParams(nextInnerTaskObjects(0))(0)
          processFuture(nextInnerTaskObjects(0), updatedBranchIndex, getFutureOnlySingleInput(nextInnerTaskObjects(0), (0), activeSet, inactiveSet, nextParam, updatedBranchIndex))
        } else {
          val updatedBranchIndex = recombinationBranchIndex :+ (nextTaskObjects(0).id.get, 0)
          val nextParam = processHelper.getServiceParams(nextTaskObjects(0))(0)
          processFuture(nextTaskObjects(0), updatedBranchIndex, getFutureOnlySingleInput(nextTaskObjects(0), (0), activeSet, inactiveSet, nextParam, updatedBranchIndex))
        }
    })
  }

  // Handle* Methods: ControlFlow Methods
  private[this] def HandleIfElse(currentFuture: Future[Tuple2[Seq[Seq[Any]], Seq[Seq[Any]]]], nextTaskObjects: Seq[TaskObject], recombinationBranchIndex: Seq[Tuple2[Int, Int]]): Seq[Future[Any]] = {
    val f = currentFuture.flatMap {
      case (activeSet, inactiveSet) =>
        val ifRecombinationParams = processHelper.getServiceParams(nextTaskObjects(0))
        val ifRecombinations = ifRecombinationParams.zipWithIndex.map {
          case (param, index) =>
            val updatedBranchIndex = recombinationBranchIndex :+ (nextTaskObjects(0).id.get, index)
            val nextFuture = getFutureOnlySingleInput(nextTaskObjects(0), index, activeSet(0), inactiveSet(0), param, updatedBranchIndex)
            processFuture(nextTaskObjects(0), updatedBranchIndex, nextFuture)
        }

        val elseRecombinationParams = processHelper.getServiceParams(nextTaskObjects(1))
        val elseRecombinations = elseRecombinationParams.zipWithIndex.map {
          case (param, index) =>
            val updatedBranchIndex = recombinationBranchIndex :+ (nextTaskObjects(1).id.get, index)
            val nextFuture = getFutureOnlySingleInput(nextTaskObjects(1), index, activeSet(1), inactiveSet(1), param, updatedBranchIndex)
            processFuture(nextTaskObjects(1), updatedBranchIndex, nextFuture)
        }

        Future.sequence(ifRecombinations.union(elseRecombinations))
    }

    Seq(f)
  }

  private[this] def HandleMultipleConnection(currentFuture: Future[Tuple2[Seq[Any], Seq[Any]]], currentTaskObject: TaskObject, nextTaskObjects: Seq[TaskObject], recombinationBranchIndex: Seq[Tuple2[Int, Int]]): Seq[Future[Any]] = {
    val connectedSingleInputFutures: Seq[Tuple2[TaskObject, Future[Any]]] =
      nextTaskObjects.map { connectedTaskObject =>
        val recombinations = processHelper.getServiceParams(connectedTaskObject)
        val future = currentFuture flatMap {
          case (activeSet, inactiveSet) =>
            if (currentTaskObject.`type`.getOrElse(throw new Exception("Element has no Type")) == TaskObjectTypes.Split) {
              val splittedActive = activeSet.asInstanceOf[Seq[Seq[Any]]]
              val splittedInactive = inactiveSet.asInstanceOf[Seq[Seq[Any]]]
              val currentConnectionIndex = nextTaskObjects.indexOf(connectedTaskObject)
              val updatedBranchIndex = recombinationBranchIndex :+ (connectedTaskObject.id.get, 0)
              val nextFuture = getFutureOnlySingleInput(connectedTaskObject, 0, splittedActive(currentConnectionIndex), splittedInactive(currentConnectionIndex), recombinations(0), updatedBranchIndex)
              processFuture(connectedTaskObject, updatedBranchIndex, nextFuture)
            } else {
              val updatedBranchIndex = recombinationBranchIndex :+ (connectedTaskObject.id.get, 0)
              val nextFuture = getFutureOnlySingleInput(connectedTaskObject, 0, activeSet, inactiveSet, recombinations(0), updatedBranchIndex)
              processFuture(connectedTaskObject, updatedBranchIndex, nextFuture)
            }
        }
        Tuple2(connectedTaskObject, future)
      }

    val result = Future.sequence(connectedSingleInputFutures.map { case (obj, fut) => fut })

    val nextUnionTasks: Seq[Seq[TaskObject]] = nextTaskObjects.map { connectedTaskObject =>
      processHelper.getNextTaskForType(connectedTaskObject, TaskObjectTypes.Union)
    }
    var unionTasks: Seq[TaskObject] = nextUnionTasks(0)
    nextUnionTasks.foreach { n => unionTasks = unionTasks.intersect(n) }

    if (unionTasks.size == 1) {
      val unionRecombinations = processHelper.getServiceParams(unionTasks(0))
      unionRecombinations.zipWithIndex.map {
        case (params, index) =>
          val updatedBranchIndex = recombinationBranchIndex :+ (unionTasks(0).id.get, index)
          result flatMap { output =>
            val typeSpecificOutput = output.asInstanceOf[Seq[Tuple2[Seq[Seq[Any]], Seq[Seq[Any]]]]]
            val nextFuture = getFutureOnlyMultipleInput(unionTasks(0), index,
              typeSpecificOutput.map { case (activeSet, inactiveSet) => activeSet },
              typeSpecificOutput.map { case (activeSet, inactiveSet) => inactiveSet }, params, updatedBranchIndex)
            processFuture(unionTasks(0), updatedBranchIndex, nextFuture)
          }
      }
    } else {
      Seq(result.flatMap { output => Future.successful(output) }) // No Union operator, the branches calculate themselves and return
    }
  }

  private[this] def HandleSingleConnection(currentFuture: Future[Tuple2[Seq[Any], Seq[Any]]], nextTaskObject: TaskObject, recombinationBranchIndex: Seq[Tuple2[Int, Int]]): Seq[Future[Any]] = {
    val recombinations = processHelper.getServiceParams(nextTaskObject)

    if (nextTaskObject.`type` == Some(TaskObjectTypes.IfElse)) {
      recombinations.zipWithIndex.map {
        case (params, index) =>
          val updatedBranchIndex = recombinationBranchIndex :+ (nextTaskObject.id.get, index)
          currentFuture.flatMap {
            case (activeSet, inactiveSet) =>
              val nextFuture = getFutureOnlyMultipleOutput(nextTaskObject, index, activeSet, inactiveSet, params, updatedBranchIndex)
              processFuture(nextTaskObject, updatedBranchIndex, nextFuture)
          }
      }
    } else {
      recombinations.zipWithIndex.map {
        case (params, index) =>
          val updatedBranchIndex = recombinationBranchIndex :+ (nextTaskObject.id.get, index)
          currentFuture.flatMap {
            case (activeSet, inactiveSet) =>
              val nextFuture = getFutureOnlySingleInput(nextTaskObject, index, activeSet, inactiveSet, params, updatedBranchIndex)
              processFuture(nextTaskObject, updatedBranchIndex, nextFuture)
          }
      }
    }
  }

  private[this] def getFutureOnlySingleInput(obj: TaskObject, recombinationServiceIndex: Int, input: Seq[Any], previouslyDiscardedInput: Seq[Any], params: Map[String, String], recombinationBranchIndex: Seq[Tuple2[Int, Int]]): Future[Tuple2[Seq[Any], Seq[Any]]] = {
    Future {
      val serviceClassName = processHelper.getServiceClassName(obj, recombinationServiceIndex)

      val service = obj.`type` match {
        case Some(TaskObjectTypes.MachineComputationTask) => serviceHelper.getMachineComputationTaskService(serviceClassName)
        case Some(TaskObjectTypes.HumanComputationTask) => serviceHelper.getHumanComputationTaskService(serviceClassName)
        case Some(TaskObjectTypes.Input) => serviceHelper.getFileTask(serviceClassName)
        case Some(TaskObjectTypes.Process) => serviceHelper.getProcessTask(serviceClassName)
        case Some(TaskObjectTypes.Divide) => serviceHelper.getDivideTask(serviceClassName)
        case Some(TaskObjectTypes.Combine) => serviceHelper.getCombineTask(serviceClassName)
        case Some(TaskObjectTypes.Loop) => serviceHelper.getLoopTaskService(serviceClassName)
        case _ => throw new Exception("Wrong Service Type")
      }

      processMessageProvider.sendEvent(obj, Some(recombinationBranchIndex), EventType.TaskStart, Calendar.getInstance().getTime())
      processMessageProvider.sendEvent(obj, Some(recombinationBranchIndex), EventType.TaskSettings, params)

      val portalConfigs = processHelper.getPortalConfiguration()
      val processedResult = service.process(input, previouslyDiscardedInput, recombinationBranchIndex, params, portalConfigs, processMessageProvider, obj)

      processMessageProvider.sendEvent(obj, Some(recombinationBranchIndex), EventType.TaskFinished, Calendar.getInstance().getTime())

      processedResult
    }
  }

  private[this] def getFutureOnlyMultipleInput(obj: TaskObject, recombinationServiceIndex: Int, input: Seq[Seq[Any]], previouslyDiscardedInput: Seq[Seq[Any]], params: Map[String, String], recombinationBranchIndex: Seq[Tuple2[Int, Int]]): Future[Tuple2[Seq[Any], Seq[Any]]] = {
    Future {
      val serviceClassName = processHelper.getServiceClassName(obj, recombinationServiceIndex)

      val service = obj.`type` match {
        case Some(TaskObjectTypes.Union) => serviceHelper.getUnionTaskService(serviceClassName)
        case _ => throw new Exception("Wrong Service Type")
      }

      processMessageProvider.sendEvent(obj, Some(recombinationBranchIndex), EventType.TaskStart, Calendar.getInstance().getTime())
      processMessageProvider.sendEvent(obj, Some(recombinationBranchIndex), EventType.TaskSettings, params)

      val portalConfigs = processHelper.getPortalConfiguration()
      val processedResult = service.process(input, previouslyDiscardedInput, recombinationBranchIndex, params, portalConfigs, processMessageProvider, obj)

      processMessageProvider.sendEvent(obj, Some(recombinationBranchIndex), EventType.TaskFinished, Calendar.getInstance().getTime())

      processedResult
    }
  }

  private[this] def getFutureOnlyMultipleOutput(obj: TaskObject, recombinationServiceIndex: Int, input: Seq[Any], previouslyDiscardedInput: Seq[Any], params: Map[String, String], recombinationBranchIndex: Seq[Tuple2[Int, Int]]): Future[Tuple2[Seq[Seq[Any]], Seq[Seq[Any]]]] = {
    Future {
      val serviceClassName = processHelper.getServiceClassName(obj, recombinationServiceIndex)

      val service = obj.`type` match {
        case Some(TaskObjectTypes.IfElse) => serviceHelper.getIfElseTaskService(serviceClassName)
        case Some(TaskObjectTypes.Split) => serviceHelper.getSplitTaskService(serviceClassName)
        case _ => throw new Exception("Wrong Service Type")
      }

      processMessageProvider.sendEvent(obj, Some(recombinationBranchIndex), EventType.TaskStart, Calendar.getInstance().getTime())
      processMessageProvider.sendEvent(obj, Some(recombinationBranchIndex), EventType.TaskSettings, params)

      val portalConfigs = processHelper.getPortalConfiguration()
      val processedResult = service.process(input, previouslyDiscardedInput, recombinationBranchIndex, params, portalConfigs, processMessageProvider, obj)

      processMessageProvider.sendEvent(obj, Some(recombinationBranchIndex), EventType.TaskFinished, Calendar.getInstance().getTime())

      processedResult
    }
  }
}