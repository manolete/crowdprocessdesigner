package crowdProcessDesigner.core.serializer

import crowdProcessDesigner.interfaces._
import aQute.bnd.annotation.component._
import java.io.File;  
import java.util.Collection;  
  
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.fasterxml.jackson.databind.DeserializationFeature

/**
 * Serializer class serializing obejcts with the jackson scala mapper
 */
@Component
class JsonSerializer extends Serializer {
  def deserialize[T](json: String, classType: Class[T]): T = {
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)
    // Not all properties of the json are mapped, so it should not fail on unknown properties
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    val process = mapper.readValue(json, classType)
    process
  }
  
  def serialize[T](obj: T) : String = {
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)
    mapper.writeValueAsString(obj)
  }
}