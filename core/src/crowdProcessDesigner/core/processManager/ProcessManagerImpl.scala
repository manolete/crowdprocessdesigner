package crowdProcessDesigner.core.processManager

import java.io.PrintWriter
import java.io.StringWriter

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Failure
import scala.util.Success

import org.osgi.framework.BundleContext

import aQute.bnd.annotation.component.Activate
import aQute.bnd.annotation.component.Component
import crowdProcessDesigner.core.processRunner.ProcessRunner
import crowdProcessDesigner.core.processRunner.utils.ServiceHelper
import crowdProcessDesigner.interfaces.Event
import crowdProcessDesigner.interfaces.EventType
import crowdProcessDesigner.interfaces.Process
import crowdProcessDesigner.interfaces.ProcessManager
import crowdProcessDesigner.interfaces.ProcessMessageProvider

@Component
class ProcessManagerImpl extends ProcessManager {

  private[this] var context: BundleContext = null

  def run(x: Process, processMessageProvider: ProcessMessageProvider): Unit = {
    // context is needed to retrieve task service instances within osgi
    val serviceHelper = new ServiceHelper(context)
    val processRunner: ProcessRunner = new ProcessRunner(x, processMessageProvider, serviceHelper)

    val startEvent = new Event(None, None, None, EventType.ProcessStart, None, processMessageProvider.getProcessId())
    processMessageProvider.sendEvent(startEvent)

    val processFutures = processRunner.run()
    processFutures.foreach(f => f onComplete {
      case Success(r) => ()
      case Failure(t) => {
        // print stack trace to console on failure
        val sw: StringWriter = new StringWriter();
        val pw: PrintWriter = new PrintWriter(sw);
        println(t.printStackTrace(pw).toString());
        processMessageProvider.sendEvent(new Event(None, None, None, EventType.TaskFailed, None, Some(Seq(t.getMessage()))))
      }
    })

    // Process is completed when all starting futures are completed
    val processFuture = Future.sequence(processFutures)
    processFuture onComplete {
      case Success(r) => processMessageProvider.sendEvent(
        new Event(None, None, None, EventType.ProcessFinished, None, processMessageProvider.getProcessId()))
      case Failure(t) => processMessageProvider.sendEvent(
        new Event(None, None, None, EventType.ProcessFailed, None, Some(Seq(t.getMessage()))))
    }
  }

  @Activate
  def start(context: BundleContext): Unit = {
    this.context = context
  }
}
