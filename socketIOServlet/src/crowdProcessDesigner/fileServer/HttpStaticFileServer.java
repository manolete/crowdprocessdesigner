package crowdProcessDesigner.fileServer;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslProvider;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import crowdProcessDesigner.interfaces.*;
import aQute.bnd.annotation.component.*;

/** 
 * Static file server. Taken and adapted from:
 * https://github.com/netty/netty/tree/4.0/example/src/main/java/io/netty/example/http/file
 *
 */
@Component
public final class HttpStaticFileServer implements ProcessFileProvider {

	static final boolean SSL = System.getProperty("ssl") != null;
	static final int PORT = Integer.parseInt(System.getProperty("port",
			SSL ? "8443" : "8080"));

	private EventLoopGroup bossGroup = null;
	private EventLoopGroup workerGroup = null;
	private Channel ch = null;

	@Activate
	public void start() throws Exception {
		// Configure SSL.
		final SslContext sslCtx;
		if (SSL) {
			SelfSignedCertificate ssc = new SelfSignedCertificate();
			sslCtx = SslContext.newServerContext(SslProvider.JDK,
					ssc.certificate(), ssc.privateKey());
		} else {
			sslCtx = null;
		}

		bossGroup = new NioEventLoopGroup(1);
		workerGroup = new NioEventLoopGroup();

		ServerBootstrap b = new ServerBootstrap();
		b.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
				.handler(new LoggingHandler(LogLevel.INFO))
				.childHandler(new HttpStaticFileServerInitializer(sslCtx));

		ch = b.bind(PORT).sync().channel();

		System.err.println("Open your web browser and navigate to "
				+ (SSL ? "https" : "http") + "://127.0.0.1:" + PORT + '/');
	}

	@Deactivate
	public void stop() throws Exception {
		ch.closeFuture().sync();
		bossGroup.shutdownGracefully();
		workerGroup.shutdownGracefully();
	}

	@Override
	public String getLinkToFile(String processId,
			String recombinationBranchIndex, String fileName) {
		System.out.println(processId);
		System.out.println(recombinationBranchIndex);
		System.out.println(fileName);
		return "http://127.0.0.1:" + PORT + "/output/" + processId + "/"
				+ recombinationBranchIndex + "_" + fileName;
	}
}
