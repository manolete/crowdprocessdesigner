package crowdProcessDesigner.socketIOProcessMessageProvider;

import java.io.ByteArrayInputStream
import java.util.concurrent.ConcurrentHashMap

import org.osgi.framework.BundleContext

import com.corundumstudio.socketio.{ Configuration, SocketConfig, SocketIOClient, SocketIOServer}
import com.corundumstudio.socketio.listener.ConnectListener
import com.corundumstudio.socketio.listener.DisconnectListener

import aQute.bnd.annotation.component.{ Activate, Component, Deactivate, Reference }

import crowdProcessDesigner.interfaces.{ ProcessManager, Serializer, ProcessFileProvider }
import crowdProcessDesigner.socketIOProcessMessageProvider.eventListener._

/** A SocketIO Based implementation of a Server which runs processes and serves available services within the osgi framework */
@Component(immediate = true)
class SocketIOServerImpl {
  
  private[this] var server: SocketIOServer = null
  private[this] val clients: ConcurrentHashMap[java.util.UUID, SocketIOMessageProvider] = new ConcurrentHashMap[java.util.UUID, SocketIOMessageProvider]()
  private[this] var serializer: Serializer = null
  private[this] var context: BundleContext = null
  private[this] var processManager: ProcessManager = null
  private[this] var processFileProvider: ProcessFileProvider = null

  @Reference
  def setSerializer(serializer: Serializer) = {
    this.serializer = serializer
  }
  def unsetSerializer(serializer: Serializer) = {
    this.serializer = null
  }
  
  @Reference
  def setFileProvider(processFileProvider: ProcessFileProvider) = {
    this.processFileProvider = processFileProvider
  }
  
  def unsetFileProvider(processFileProvider: ProcessFileProvider) = {
    this.processFileProvider = null
  }
  
  @Reference
  def setProcessManager(processManager: ProcessManager) = {
    this.processManager = processManager
  }
  def unsetProcessManager(processManager: ProcessManager) = {
    this.processManager = null
  }

  @Activate
  def start(context: BundleContext) = {
    this.context = context;

    val configuration = getServerConfiguration();
    server = new SocketIOServer(configuration);

    server.addConnectListener(new ConnectListener() {
      def onConnect(client: SocketIOClient) {
        clients.put(client.getSessionId(), new SocketIOMessageProvider(client, serializer));
      }
    });

    server.addDisconnectListener(new DisconnectListener() {
      def onDisconnect(client: SocketIOClient) {
        clients.remove(client.getSessionId().toString());
      }
    });

    server.addEventListener("getServices", classOf[String], new GetServicesListener(context, serializer))
    server.addEventListener("loadProcess", classOf[String], new LoadProcessListener(context))
    server.addEventListener("getPortals", classOf[String], new GetPortalsListener(context, serializer))
    server.addEventListener("getProcesses", classOf[String], new GetProcessesListener(context, serializer))
    server.addEventListener("runProcess", classOf[String], new RunProcessListener(clients, processManager, serializer))
    server.addEventListener("getFileLink", classOf[String], new GetFileLinkListener(processFileProvider, serializer))
    
    server.start();
  }

  def getServerConfiguration() : Configuration = {
    val configuration = new Configuration();
    configuration.setHostname("localhost");
    configuration.setPort(4000);
    configuration.setAllowCustomRequests(true);

    // netty takes some time to free up the port, this fixes the port blocked restart bug in eclipse 
    val socketConfig = new SocketConfig()
    socketConfig.setReuseAddress(true);
    configuration.setSocketConfig(socketConfig)

    // the ide server may run on a different location
    val str = "Access-Control-Allow-Origin: *";
    val is = new ByteArrayInputStream(str.getBytes());
    configuration.setCrossDomainPolicy(is);
    configuration
  }

  @Deactivate
  def stop() {
    clients.clear()
    server.stop()
  }
}