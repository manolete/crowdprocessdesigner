package crowdProcessDesigner.socketIOProcessMessageProvider;

import scala.collection.mutable.Queue
import scala.concurrent.Future
import scala.concurrent.Promise

import com.corundumstudio.socketio.SocketIOClient

import crowdProcessDesigner.interfaces.Event
import crowdProcessDesigner.interfaces.EventType
import crowdProcessDesigner.interfaces.ProcessMessageProvider
import crowdProcessDesigner.interfaces.Serializer
import crowdProcessDesigner.interfaces.TaskObject
import java.util.UUID

/** Message event provider which sends events over SocketIO, e.g. the ide log */
class SocketIOMessageProvider(client: SocketIOClient, serializer: Serializer) extends ProcessMessageProvider {

  val breakpointPromises = Queue[Promise[String]]()
  
  @Override
  def sendMessage(message: String) {
      client.sendMessage(message);
  }

  @Override
  def sendEvent(event: Event) {
      client.sendEvent(EventType.ProcessEvent, serializer.serialize(event));
  }
  
  @Override
  def sendEvent(obj: TaskObject, recombinationBranchIndex: Option[Seq[Tuple2[Int, Int]]], eventType: String, data: Any) {
    val event = new Event(obj.id, obj.`type`, recombinationBranchIndex, eventType, obj.label, data)
    client.sendEvent(EventType.ProcessEvent, serializer.serialize(event));
  }
  
  @Override
  def getProcessId() : UUID = {
    client.getSessionId()
  }
}
