package crowdProcessDesigner.socketIOProcessMessageProvider.eventListener

import scala.collection.JavaConverters.collectionAsScalaIterableConverter

import org.osgi.framework.BundleContext
import org.osgi.framework.wiring.BundleWiring

import com.corundumstudio.socketio.AckRequest
import com.corundumstudio.socketio.SocketIOClient
import com.corundumstudio.socketio.listener.DataListener

import crowdProcessDesigner.interfaces.Serializer

class GetProcessesListener(context: BundleContext, serializer: Serializer) extends DataListener[String] {
  def onData(client: SocketIOClient, taskType: String, arg2: AckRequest) {

    case class BundleProcess(bundleId: Long, processName: String)

    val processes: Seq[BundleProcess] = context
      .getBundles()
      .flatMap { bundle =>
        Option(bundle.adapt(classOf[BundleWiring]))
      }
      .flatMap { wiring =>
        wiring.listResources("/processes/", "*.json", BundleWiring.LISTRESOURCES_LOCAL)
          .asScala
          .map(p => new BundleProcess(wiring.getBundle().getBundleId, p))
      }

    arg2.sendAckData(serializer.serialize(processes))
  }
}