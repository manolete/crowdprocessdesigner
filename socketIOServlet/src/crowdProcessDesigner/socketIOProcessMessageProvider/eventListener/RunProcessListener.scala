package crowdProcessDesigner.socketIOProcessMessageProvider.eventListener

import java.util.concurrent.ConcurrentHashMap

import com.corundumstudio.socketio.AckRequest
import com.corundumstudio.socketio.SocketIOClient
import com.corundumstudio.socketio.listener.DataListener

import crowdProcessDesigner.interfaces.Process
import crowdProcessDesigner.interfaces.ProcessManager
import crowdProcessDesigner.interfaces.Serializer
import crowdProcessDesigner.socketIOProcessMessageProvider.SocketIOMessageProvider

class RunProcessListener(clients: ConcurrentHashMap[java.util.UUID, SocketIOMessageProvider], processManager: ProcessManager, serializer: Serializer) extends DataListener[String] {
      def onData(client: SocketIOClient, json: String, arg2: AckRequest) {
        val process = serializer.deserialize(json, classOf[Process])
        val messageProvider = clients.get(client.getSessionId())
        processManager.run(process, messageProvider)
      }
}