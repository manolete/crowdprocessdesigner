package crowdProcessDesigner.socketIOProcessMessageProvider.eventListener

import scala.io.Source

import org.osgi.framework.BundleContext

import com.corundumstudio.socketio.AckRequest
import com.corundumstudio.socketio.SocketIOClient
import com.corundumstudio.socketio.listener.DataListener

class LoadProcessListener(context: BundleContext) extends DataListener[String] {
      def onData(client: SocketIOClient, bundleIdAndResource: String, arg2: AckRequest) {
        val bundleIdResourceArr = bundleIdAndResource.split(",")
        val bundle = context.getBundle(bundleIdResourceArr(0).toLong)
        val url = bundle.getResource(bundleIdResourceArr(1))
        val processJson = Source.fromURL(url).mkString  

        arg2.sendAckData(processJson)
      }
}