package crowdProcessDesigner.socketIOProcessMessageProvider.eventListener

import org.osgi.framework.BundleContext

import com.corundumstudio.socketio.AckRequest
import com.corundumstudio.socketio.SocketIOClient
import com.corundumstudio.socketio.listener.DataListener

import crowdProcessDesigner.interfaces._

class GetServicesListener(context: BundleContext, serializer: Serializer) extends DataListener[String] {
  def onData(client: SocketIOClient, taskType: String, arg2: AckRequest) {
    val interfaceClass = taskType match {
      case TaskObjectTypes.HumanComputationTask => classOf[HumanComputationTask]
      case TaskObjectTypes.MachineComputationTask => classOf[MachineComputationTask]
      case TaskObjectTypes.Union => classOf[UnionTask]
      case TaskObjectTypes.Divide => classOf[DivideTask]
      case TaskObjectTypes.IfElse => classOf[IfElseTask]
      case TaskObjectTypes.Input => classOf[FileTask]
      case TaskObjectTypes.Process => classOf[ProcessTask]
      case TaskObjectTypes.Split => classOf[SplitTask]
      case TaskObjectTypes.Combine => classOf[CombineTask]
      case TaskObjectTypes.Loop => classOf[LoopTask]
      case _ => throw new Exception("unknown service type")
    }

    val filterString = "(objectClass=" + interfaceClass.getName + ")";
    val oServiceReferences = Option(context.getServiceReferences(interfaceClass.getName, filterString))

    val bundleInformations = oServiceReferences.map { serviceReferences =>
      serviceReferences.map { x =>
        val serviceOption = Option(context.getService(x).asInstanceOf[ServiceProvider])
        serviceOption.map { service =>
          service.getInformation().map { taskInformation =>
            new BundleInformation(taskInformation, service.getClass().getCanonicalName())
          }
        }
      }
      .flatten
      .flatMap { x => x }
    }.getOrElse(Seq.empty)

    arg2.sendAckData(serializer.serialize(bundleInformations))
  }
}