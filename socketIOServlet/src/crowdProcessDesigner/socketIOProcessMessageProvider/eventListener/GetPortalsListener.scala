package crowdProcessDesigner.socketIOProcessMessageProvider.eventListener

import org.osgi.framework.BundleContext

import com.corundumstudio.socketio.AckRequest
import com.corundumstudio.socketio.SocketIOClient
import com.corundumstudio.socketio.listener.DataListener

import crowdProcessDesigner.interfaces.BundleInformation
import crowdProcessDesigner.interfaces.Portal
import crowdProcessDesigner.interfaces.Serializer
import crowdProcessDesigner.interfaces.ServiceProvider

class GetPortalsListener(context: BundleContext, serializer: Serializer) extends DataListener[String] {
  def onData(client: SocketIOClient, taskType: String, arg2: AckRequest) {

    val interfaceClass = classOf[Portal]
    val filterString = "(objectClass=" + interfaceClass.getName + ")";
    val oServiceReferences = Option(context.getServiceReferences(interfaceClass.getName, filterString))

    val bundleInformations = oServiceReferences.map { serviceReferences =>
      serviceReferences.map { sr =>
        context.getService(sr).asInstanceOf[ServiceProvider]
      }
        .map { service =>
          service.getInformation()
            .map { taskInformation =>
              new BundleInformation(taskInformation, service.getClass().getCanonicalName())
            }
        }
        .flatten
    }
    .getOrElse(Seq.empty)

    arg2.sendAckData(serializer.serialize(bundleInformations))
  }
}