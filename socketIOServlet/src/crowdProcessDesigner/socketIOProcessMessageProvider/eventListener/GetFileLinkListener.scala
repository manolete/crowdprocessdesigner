package crowdProcessDesigner.socketIOProcessMessageProvider.eventListener

import org.osgi.framework.BundleContext

import com.corundumstudio.socketio.AckRequest
import com.corundumstudio.socketio.SocketIOClient
import com.corundumstudio.socketio.listener.DataListener

import crowdProcessDesigner.interfaces.BundleInformation
import crowdProcessDesigner.interfaces.Portal
import crowdProcessDesigner.interfaces.Serializer
import crowdProcessDesigner.interfaces.ServiceProvider
import crowdProcessDesigner.interfaces.ProcessFileProvider

case class FileLink(processId: String, recombinationBranchIndex: String, fileName: String)

class GetFileLinkListener(processFileProvider: ProcessFileProvider, serializer: Serializer) extends DataListener[String]() {
  @Override
  def onData(client: SocketIOClient, data: String, ackRequest: AckRequest) {
    println(data)
    val fileLink = data.split(";")
    val linkToFile = processFileProvider.getLinkToFile(fileLink(0), fileLink(1), fileLink(2))
    ackRequest.sendAckData(linkToFile)
  }
}