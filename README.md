[![Codacy Badge](https://www.codacy.com/project/badge/6390ab1f22624e55b00b56565d91213f)](https://www.codacy.com/public/manuelgugger/CrowdProcessDesigner)

CrowdProcessDesigner
=================
An IDE written in html, javascript and scala which aims at rapid prototyping of crowd processes supporting recombination for finding optimal process configuration parameters.

There is an Introduction Video ([OGV](https://github.com/mgugger/CrowdProcessDesigner/raw/master/ide/public/video/introduction.ogv), [MP4](https://github.com/mgugger/CrowdProcessDesigner/raw/master/ide/public/video/introduction.ogv)) to see the IDE in action.

The corresponding thesis can be found [here](https://github.com/mgugger/CrowdProcessDesigner/raw/master/thesis/thesis.pdf). 

## Features
* Combine independent tasks
* Add multiple configurations to a task and have them executed in parallel (recombination)
* Loop construct for partial iterations over data that needs refinement
* Charts for runtime and process costs
* Access process artifacts (such as output files)
* Easily implement your own processes and tasks and run them from within the IDE. A sample implementation of two processes is available in the *services* folder.

## Architecture
<img src="https://raw.githubusercontent.com/mgugger/CrowdProcessDesigner/master/ide/public/images/architecture.png" alt="Architecture" title="CrowdProcessDesigner" />

The application is divided into 4 parts.
* **interfaces:**: 
	The package containing all the interfaces and classes for deserialization etc.
* **ide:**
	A [play framework](https://www.playframework.com/) application serving the html and javascript
* **core:**
	Executes a process by chaining and executing the selected tasks
* **socketIOServlet:**
	Sends messages to and from the ide, providing events and such (in the reference implementation via SocketIO)

All packages except the *ide* are osgi-packages (using [bnd](https://github.com/bndtools/bnd)). The tasks are services within an osgi container and picked up by the core.

Play framework cannot be run in osgi, therefore communication has been realised via a socketIO connection.

### Sample Process Implementations
Two sample packages containing the tasks and processes are available. 

The *services* package contains a sample implementation of two processes (in json format) and the osgi services.

The *PPLibService* package is an integration of PPLib. While building, it needs the PPLib assembly the pplibAssembly folder to copy the content into the classpath.

## Getting Started
To get started, just clone the project. In the *ide* project run
```
sbt run
```
to start the project. The ide will run an internal osgi framework (apache felix).

#### Running within Eclipse/BndTools
The internal OSGI framework of the IDE application can be deactivated. The configuration option to activate/deactive the internal framework can be found in the following file:
[osgiFramework.Conf / osgiFramework.useInternalOsgi](https://github.com/mgugger/CrowdProcessDesigner/blob/master/ide/conf/osgiFramework.conf)
With the internal osgi framework of the ide deactivated, the packages may be run in eclipse (or another ide/osgi container) for easier debugging of processes. Just import the packages into eclipse and add the scala and bndTools project nature. Launch configurations containing all the necessary bundles are provided in the *services* and *socketIOServlet* packages (launch.bndrun).

Notes:
* You need to change the output path of the test folder to *bin_test* by right clicking on the test folder and change the output path
* To copy the needed dependencies into the local bndTools repository, in the *deps* folder, the build.sbt fetches all the needed jars into a *lib_managed* folder, from there they may be easily copied into the bndTools repository




