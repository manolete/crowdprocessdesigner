package crowdProcessDesigner.service.hct

import crowdProcessDesigner.interfaces._
import aQute.bnd.annotation.component.{ Component, Activate }
import org.osgi.framework._
import scala.io.Source
import scala.util.Random
import crowdProcessDesigner.service.abstractService._

@Component
class RandomYesNo extends AbstractService with HumanComputationTask {

  val inputParams = Map[String, Any](numberOfWorkersParam -> 1, distributeWorkParam -> trueFalseValues)

  def process(input: Seq[Any], previouslyDiscardedChunks: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    val numberOfWorkers = getParam[Int](numberOfWorkersParam, params)
    val distributeWork = getParam[Boolean](distributeWorkParam, params)

    val output: Seq[ProcessDataChunk] = if (distributeWork) {
      val groupSize = math.max(input.length / numberOfWorkers, 1)
      val inputGroups = input.asInstanceOf[Seq[ProcessDataChunk]].grouped(groupSize)
      inputGroups.map { processDataChunks =>
        assignRandomIsCorrectData(input)
      }.flatMap { x => x }.toSeq
    } else {
      1 to numberOfWorkers flatMap { worker =>
        assignRandomIsCorrectData(input)
      }
    }

    processMessageProvider.sendEvent(taskObject, Some(recombinationBranchindex), EventType.TaskCost, Random.nextInt(30))

    (output, previouslyDiscardedChunks)
  }

  def assignRandomIsCorrectData(input: Seq[Any]) = {
    val workerOutput: Seq[ProcessDataChunk] = input.asInstanceOf[Seq[ProcessDataChunk]].map { processDataChunk =>
      processDataChunk.isCorrectData = Some(Random.nextBoolean())
      processDataChunk
    }
    workerOutput
  }

  def getInformation() = {
    Seq(new ServiceInformation(getDisplayName(), "Returns modified inputs", inputParams))
  }

  @Activate
  def start: Unit = {}
}