package crowdProcessDesigner.service.hct

import crowdProcessDesigner.interfaces._
import aQute.bnd.annotation.component.{ Component, Activate }
import org.osgi.framework._
import scala.io.Source
import scala.util.Random
import crowdProcessDesigner.service.abstractService._

@Component
class RandomMultipleChoice extends AbstractService with HumanComputationTask {

  val inputParams = Map[String, Any](numberOfSelectionsParam -> 10, numberOfWorkersParam -> 5)

  def process(input: Seq[Any], previouslyDiscardedChunks: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    val numberOfSelections = getParam[Int](numberOfSelectionsParam, params)
    val numberOfWorkers = getParam[Int](numberOfWorkersParam, params)

    val output: Seq[ProcessDataChunk] =
      1 to numberOfWorkers flatMap { worker =>
        val selections = input.asInstanceOf[Seq[ProcessDataChunk]]
          .map(x => (x, util.Random.nextInt()))
          .sortBy { case (chunk, randomInt) => randomInt }
          .take(numberOfSelections)
          .map { case (chunk, randomInt) => chunk }

        selections.map { selection =>
          new ProcessDataChunk(selection, selection.data, selection.context)
        }
      }

    val discardedOutput = input.asInstanceOf[Seq[ProcessDataChunk]]
      .filter(processDataChunk => !output.map(p => p.chunkIds).contains(processDataChunk.chunkIds))

    processMessageProvider.sendEvent(taskObject, Some(recombinationBranchindex), EventType.TaskCost, Random.nextInt(30))

    (output, discardedOutput.union(previouslyDiscardedChunks))
  }

  def getInformation() = {
    Seq(new ServiceInformation(getDisplayName(), "Returns a random crowd selection", inputParams))
  }

  @Activate
  def start: Unit = {}
}