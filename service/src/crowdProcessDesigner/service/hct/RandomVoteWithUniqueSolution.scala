package crowdProcessDesigner.service.hct

import crowdProcessDesigner.interfaces._
import aQute.bnd.annotation.component.{ Component, Activate }
import org.osgi.framework._
import scala.io.Source
import scala.util.Random
import crowdProcessDesigner.service.abstractService._

@Component
class RandomVoteWithUniqueSolution extends AbstractService with HumanComputationTask {

  def process(input: Seq[Any], previouslyDiscardedChunks: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    val output: Seq[ProcessDataChunk] = input.asInstanceOf[Seq[ProcessDataChunk]]
      .groupBy { processDataChunk => processDataChunk.chunkIds }
      .map{ case (chunkIds, groups) => groups(Random.nextInt(groups.length)) }
      .toSeq

    processMessageProvider.sendEvent(taskObject, Some(recombinationBranchindex), EventType.TaskCost, Random.nextInt(30))

    (output, previouslyDiscardedChunks)
  }

  def getInformation() = {
    Seq(new ServiceInformation(getDisplayName(), "Returns modified inputs", Map.empty))
  }

  @Activate
  def start: Unit = {}
}