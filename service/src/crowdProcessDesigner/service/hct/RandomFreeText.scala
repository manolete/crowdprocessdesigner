package crowdProcessDesigner.service.hct

import crowdProcessDesigner.interfaces._
import aQute.bnd.annotation.component.{ Component, Activate }
import org.osgi.framework._
import scala.io.Source
import scala.util.Random
import crowdProcessDesigner.service.abstractService._

@Component
class RandomFreeText extends AbstractService with HumanComputationTask {

  val inputParams = Map[String, Any](numberOfWorkersParam -> 5, distributeWorkParam -> trueFalseValues)

  def process(input: Seq[Any], previouslyDiscardedChunks: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    val numberOfWorkers = getParam[Int](numberOfWorkersParam, params)
    val distributeWork = getParam[Boolean](distributeWorkParam, params)

    val output: Seq[ProcessDataChunk] =
      if (distributeWork) {
        val groupSize = math.max(input.length / numberOfWorkers, 1)
        input.asInstanceOf[Seq[ProcessDataChunk]].grouped(groupSize)
          .map { selection =>
            selection.map { processDataChunk =>
              new ProcessDataChunk(processDataChunk.chunkIds, "@Input:" + Random.nextString(10), processDataChunk.context)
            }
          }.flatten
          .toSeq
      } else {
        (1 to numberOfWorkers toList).map { s =>
          input.asInstanceOf[Seq[ProcessDataChunk]].map { chunk =>
            new ProcessDataChunk(chunk, "@Input:" + Random.nextString(10), chunk.context)
          }
        }
          .flatten
      }

    processMessageProvider.sendEvent(taskObject, Some(recombinationBranchindex), EventType.TaskCost, Random.nextInt(30))

    (output, previouslyDiscardedChunks)
  }

  def getInformation() = {
    Seq(new ServiceInformation(getDisplayName(), "Returns modified inputs", inputParams))
  }

  @Activate
  def start: Unit = {}
}