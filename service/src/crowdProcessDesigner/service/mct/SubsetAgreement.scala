package crowdProcessDesigner.service.mct

import crowdProcessDesigner.interfaces._
import aQute.bnd.annotation.component.{ Component, Activate }
import org.osgi.framework._
import crowdProcessDesigner.service.abstractService._

@Component
class SubsetAgreement extends AbstractService with MachineComputationTask {

  val minSubmissionsParams = "MinSubmissionsRequired"
  val inputParams = Map[String, Any]((minSubmissionsParams -> "2"))

  def process(input: Seq[Any], previouslyDiscardedChunks: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    val minSubmissionsRequired = getParam[Int](minSubmissionsParams, params)

    val output = input.asInstanceOf[Seq[ProcessDataChunk]]
      .groupBy { processDataChunk => processDataChunk.chunkIds.last }
      .filter { case (lastChunkId, group) => group.length >= minSubmissionsRequired }
      .map { case (lastChunkId, group) => group(0) } toSeq

    val discardedOutput = input.asInstanceOf[Seq[ProcessDataChunk]]
      .filter(processDataChunk => !output.map(o => o.chunkIds).contains(processDataChunk.chunkIds))

    (output, discardedOutput.union(previouslyDiscardedChunks))
  }

  def getInformation() = {
    Seq(new ServiceInformation(getDisplayName(), "Returns the subset which matches the acceptance percentage", inputParams))
  }

  @Activate
  def start: Unit = {}
}