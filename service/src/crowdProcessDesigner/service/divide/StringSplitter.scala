package crowdProcessDesigner.service.divide

import crowdProcessDesigner.interfaces._
import aQute.bnd.annotation.component.{ Component, Activate }
import org.osgi.framework._
import scala.io.Source
import crowdProcessDesigner.service.abstractService._

@Component
class StringSplitter extends AbstractService with DivideTask {
  
  val splitByParam = "SplitBy"
  val bySentence = "Sentence"
  val byParagraph = "Paragraph"
  val splitByValues = Seq(bySentence, byParagraph) 
  val inputParams = Map[String, Any]((splitByParam -> splitByValues))
  
  def process(input: Seq[Any], previouslyDiscardedChunks: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]]= {
    
    val splitByOption = getParam[String](splitByParam, params)
        
    val output: Seq[ProcessDataChunk] = input.asInstanceOf[Seq[ProcessDataChunk]].zipWithIndex.flatMap { case(processDataChunk, index) =>  
        val string = processDataChunk.data.asInstanceOf[String]
        
        val splittedString = splitByOption match {
          case `bySentence` => string.split('.')
          case `byParagraph` => string.split('\n')
        }
        splittedString.zipWithIndex.map { case(part, subIndex) => new ProcessDataChunk(Seq((index+1)*(subIndex+1)), part, part) }      
    }
    
    (output, previouslyDiscardedChunks)
  }  
  
  def getInformation() = {
    
    Seq(new ServiceInformation(getDisplayName(), "Splits the input string", inputParams))
  }
  
  @Activate
  def start: Unit = { }
}