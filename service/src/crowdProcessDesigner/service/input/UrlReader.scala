package crowdProcessDesigner.service.input

import crowdProcessDesigner.interfaces._
import aQute.bnd.annotation.component.{ Component, Activate }
import org.osgi.framework._
import scala.io.Source
import java.nio.charset.CodingErrorAction
import scala.io.Codec
import crowdProcessDesigner.service.abstractService._

@Component
class UrlReader extends AbstractService with FileTask {

  val inputParams = Map[String, Any](urlParam -> "")

  // Handle decoding errors instead of failure
  implicit val codec = Codec("UTF-8")
  codec.onMalformedInput(CodingErrorAction.REPLACE)
  codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

  def process(input: Seq[Any], previouslyDiscardedChunks: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    val url = if (getParam[String](urlParam, params).startsWith("www.")) {
      "http://" + getParam[String](urlParam, params)
    } else {
      getParam[String](urlParam, params)
    }

    val text = Source.fromURL(url)
    val loadedText = text.mkString

    processMessageProvider.sendEvent(taskObject, Some(recombinationBranchindex), EventType.Result, url)

    (Seq(new ProcessDataChunk(Seq(0), loadedText, loadedText)), previouslyDiscardedChunks)
  }

  def getInformation() = {
    Seq(new ServiceInformation(getDisplayName(), "Reads an url into a string", inputParams))
  }

  @Activate
  def start: Unit = {}
}