package crowdProcessDesigner.service.input

import crowdProcessDesigner.interfaces._
import aQute.bnd.annotation.component.{ Component, Activate }
import org.osgi.framework._
import scala.io.Source
import java.nio.charset.CodingErrorAction
import scala.io.Codec
import java.io._
import crowdProcessDesigner.service.abstractService._

@Component
class FileWriter extends AbstractService with FileTask {

  def process(input: Seq[Any], previouslyDiscardedChunks: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {

    val fileName = getParam[String](fileNameParam, params)
    val includeContext = getParam[Boolean](includeContextParam, params)
    val includeDiscarded = getParam[Boolean](includeDiscardedParam, params)
    val includeWrongData = getParam[Boolean](includeWrongDataParam, params)

    val folderPath = "output/" + processMessageProvider.getProcessId().toString() + "/"
    val folder = new File(folderPath)
    if (!folder.exists()) { folder.mkdirs() }

    val fileNameWithRecombination = recombinationBranchindex.map {
      case (taskObjectId, serviceId) => taskObjectId + "-" + serviceId
    }.mkString("_") + "_" + fileName

    val writer = new PrintWriter(new File(folderPath + fileNameWithRecombination))

    val dataToConsider = if (includeDiscarded) {
      input
        .union(previouslyDiscardedChunks)
        .asInstanceOf[Seq[ProcessDataChunk]]
        .filter { chunk => includeWrongData || chunk.isCorrectData.getOrElse(false) }
    } else {
      input
        .asInstanceOf[Seq[ProcessDataChunk]]
        .filter { chunk => includeWrongData || chunk.isCorrectData.getOrElse(false) }
    }

    dataToConsider.foreach { processDataChunk =>
      if (includeContext) {
        writer.write(processDataChunk.context.toString() + "\t" + processDataChunk.data.toString() + "\n")
      } else {
        writer.write(processDataChunk.data.toString() + "\n")
      }
    }

    writer.close()

    processMessageProvider.sendEvent(taskObject, Some(recombinationBranchindex), EventType.Result, fileName)

    (input, previouslyDiscardedChunks)
  }

  val inputParams = Map[String, Object](
    fileNameParam -> "",
    includeContextParam -> trueFalseValues,
    includeDiscardedParam -> trueFalseValues,
    includeWrongDataParam -> trueFalseValues)
    
  def getInformation() = {
    Seq(new ServiceInformation(getDisplayName(), "Writes the input to the given File", inputParams))
  }

  @Activate
  def start: Unit = {}
}