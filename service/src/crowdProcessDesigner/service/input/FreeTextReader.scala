package crowdProcessDesigner.service.input

import crowdProcessDesigner.interfaces._
import aQute.bnd.annotation.component.{ Component, Activate }
import org.osgi.framework._
import scala.io.Source
import java.nio.charset.CodingErrorAction
import scala.io.Codec
import crowdProcessDesigner.service.abstractService._

@Component
class FreeTextReader extends AbstractService with FileTask {

  val inputParams = Map[String, Any](freeTextParam -> "")

  def process(input: Seq[Any], previouslyDiscardedChunks: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    val freeText = getParam[String](freeTextParam, params)
    
    (Seq(new ProcessDataChunk(Seq(0), freeText, freeText)), previouslyDiscardedChunks)
  }

  def getInformation() = {
    Seq(new ServiceInformation(getDisplayName(), "Reads the freetext into a string", inputParams))
  }

  @Activate
  def start: Unit = {}
}