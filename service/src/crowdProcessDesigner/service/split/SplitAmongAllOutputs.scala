package crowdProcessDesigner.service.mct

import crowdProcessDesigner.interfaces._
import aQute.bnd.annotation.component.{ Component, Activate }
import org.osgi.framework._
import crowdProcessDesigner.service.abstractService._

@Component
class SplitAmongAllOutputs extends AbstractService with SplitTask {

  def process(input: Seq[Any], previouslyDiscardedChunks: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Seq[Any]], Seq[Seq[Any]]] = {
    val numberOfSplits = taskObject.connectedToIds.getOrElse(Seq.empty).size
    
    val output = input.asInstanceOf[Seq[ProcessDataChunk]]
      .grouped(numberOfSplits)
      .toSeq
    val discardedOutput = previouslyDiscardedChunks.asInstanceOf[Seq[ProcessDataChunk]]
      .grouped(numberOfSplits)
      .toSeq

    (output, discardedOutput)
  }

  def getInformation() = {
    Seq(new ServiceInformation(getDisplayName(), "Splits the output among all connected outputs equally", Map.empty))
  }

  @Activate
  def start: Unit = {}
}