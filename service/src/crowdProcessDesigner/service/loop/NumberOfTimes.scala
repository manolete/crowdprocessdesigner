package crowdProcessDesigner.service.loop

import crowdProcessDesigner.interfaces._
import aQute.bnd.annotation.component.{ Component, Activate }
import org.osgi.framework._
import scala.io.Source
import crowdProcessDesigner.service.abstractService._
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic._

@Component
class NumberOfTimes extends AbstractService with LoopTask {

  val recombinationBranchMap: ConcurrentHashMap[Int, AtomicLong] = new ConcurrentHashMap[Int, AtomicLong]()

  def process(input: Seq[Any], previouslyDiscardedChunks: Seq[Any], recombinationBranchIndex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    val recombinationBranchHashCode = processMessageProvider.hashCode() + recombinationBranchIndex.toSet().hashCode()
    recombinationBranchMap.putIfAbsent(recombinationBranchHashCode, new AtomicLong(0))
    val numberOfIteration = recombinationBranchMap.get(recombinationBranchHashCode).incrementAndGet()
    
    val considerOnlyIncorrectDataValue = getParam[Boolean](considerOnlyIncorrectData, params)
    val chunksToConsider = if (considerOnlyIncorrectDataValue) { input.asInstanceOf[Seq[ProcessDataChunk]].filter { x => !x.isCorrectData.getOrElse(false) } } else { input.asInstanceOf[Seq[ProcessDataChunk]] }

    val numberOfTimesValue = getParam[Int](numberOfTimesParam, params)
    if (numberOfIteration > numberOfTimesValue) {
      recombinationBranchMap.remove(recombinationBranchHashCode)
      (Seq.empty, input.union(previouslyDiscardedChunks))
    } else {
      (chunksToConsider, previouslyDiscardedChunks.union(input.asInstanceOf[Seq[ProcessDataChunk]].diff(chunksToConsider)))
    }
  }

  val inputParams = Map[String, Any](numberOfTimesParam -> 3, considerOnlyIncorrectData -> true)

  def getInformation() = {
    Seq(new ServiceInformation(getDisplayName(), "Iterates the given number of times", inputParams))
  }

  @Activate
  def start: Unit = {}
}