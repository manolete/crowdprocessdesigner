package crowdProcessDesigner.service.abstractService

import crowdProcessDesigner.interfaces._
import aQute.bnd.annotation.component.{ Component, Activate }
import org.osgi.framework._
import scala.io.Source
import scala.reflect.ClassTag

abstract class AbstractService {
  val numberOfWorkersParam = "NumberOfWorkers"
  val distributeWorkParam = "DistributeWork"
  val numberOfSelectionsParam = "NumberOfSelections"
  val numberOfTimesParam = "MaxNumberOfTimes"
  val fileNameParam = "FileName"
  val considerOnlyIncorrectData = "ConsiderOnlyIncorrectData"
  val includeContextParam = "IncludeContext"
  val includeDiscardedParam = "IncludeDiscarded"
  val includeWrongDataParam = "IncludeWrongData"
  val urlParam = "Url"
  val freeTextParam = "FreeText"
  val trueFalseValues = Seq(true, false)
  // the className when multiple bundleInformations are provided in a service
  val serviceParam = "service"
  val questionParam = "Question"

  def getParam[T: ClassTag](param: String, params: Map[String, String], defaultValue: Option[Any] = None): T = {
    val paramValue = params.get(param).getOrElse(throw new Exception(f"$param%s is not defined"))

    val tClass = implicitly[ClassTag[T]].runtimeClass
    val intClass = classOf[Int]
    val stringClass = classOf[String]
    val booleanClass = classOf[Boolean]
    val doubleClass = classOf[Double]

    val result = tClass match {
      case `intClass` => paramValue.toInt
      case `stringClass` => paramValue
      case `booleanClass` => paramValue.toBoolean
      case `doubleClass` => paramValue.toDouble
      case _ => throw new Exception("Unhandled type")
    }
    
    result.asInstanceOf[T]
  }
  
  def getDisplayName(): String = {
    this.getClass.getName.split('.').last
  }
}