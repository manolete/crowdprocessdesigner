package crowdProcessDesigner.service.combine

import crowdProcessDesigner.interfaces._
import aQute.bnd.annotation.component.{ Component, Activate }
import org.osgi.framework._
import scala.io.Source
import crowdProcessDesigner.service.abstractService._

@Component
class StringCombiner extends AbstractService with CombineTask  {

  def process(input: Seq[Any], previouslyDiscardedChunks: Seq[Any], recombinationBranchindex: Seq[Tuple2[Int, Int]], params: Map[String, String], portalConfigs: Map[String, Map[String, String]], processMessageProvider: ProcessMessageProvider, taskObject: TaskObject): Tuple2[Seq[Any], Seq[Any]] = {
    val output: String = input.asInstanceOf[Seq[ProcessDataChunk]]
      .union(previouslyDiscardedChunks.asInstanceOf[Seq[ProcessDataChunk]])
      .sortBy { processDataChunk => processDataChunk.chunkIds.last }
      .map(processDataChunk => processDataChunk.data)
      .mkString("\n")
    (Seq(new ProcessDataChunk(Seq(0), output, output)), Seq.empty)
  }

  def getInformation() = {
    Seq(new ServiceInformation(getDisplayName(), "Combines the string list to a string", Map.empty))
  }

  @Activate
  def start: Unit = {}
}