package crowdProcessDesigner.service.process

import org.scalatest.junit.AssertionsForJUnit
import scala.collection.mutable.ListBuffer
import org.junit.Assert._
import org.junit.Test
import org.junit.Before
import crowdProcessDesigner.interfaces._
import org.scalatest.junit.JUnitSuite
import org.scalatest.mock.MockitoSugar._
import org.mockito.Mockito.stub
import org.mockito.Matchers.any
import org.junit.Test
import org.junit.Before
import org.mockito.Mockito.{ when, doCallRealMethod }
import scala.concurrent._
import scala.concurrent.duration._
import crowdProcessDesigner.service.combine._
import crowdProcessDesigner.service.divide._
import crowdProcessDesigner.service.hct._
import crowdProcessDesigner.service.input._
import crowdProcessDesigner.service.mct._

class FindFixVerify extends JUnitSuite {

  val text: String = """It is generally assumed that people who participate in sport can be divided into two categories: One consisting of those who engage in sports either motivated by the excitement involved in preferably successful competition; and in the case of professionals, driven by the economic rewards following it, and one much more numerous group consisting of the less serious exercisers for whom the ultimate goal is not the attainment of high athletic abilities, their aim is merely to maintain a modicum of physical fitness for preserving bodily health or relieve stress. 
  There are sound reasons why one might say that both categories actually fail to optimally reap the potential rewards of regular athletic training. 
  The people in the first group have a very serious approach, and in most cases they consequently show a high degree of development of their athletic potential. However, a most important negative aspect regarding competitive athletics on the definite upper level is that the motivation of the participants tends to depend on competitive ability, hence all their activity will seem to have no reason if and when their measurable results do not meet the standards defined by levels only attainable for people in the physical prime of their lives. As a result of this rather short-sighted approach, a majority of competitive sportsmen and sportswomen cease to train regularly when the results begin to decline. Then they lose their abilities and gradually are reduced to the average level of the untrained. Seen in a broad perspective, all their efforts have ultimately been wasted. It may therefore be argued that any motivation rooted in the isolated aim to participate in competitions on the highest level is bound to fade away over time, soon to be replaced with absence of any motivation and a concomitant athletic inactivity. One more fact to consider here is that the very extensive preparation required for top-level competition more often than not involves 3 hours or more with training every day. Everybody agrees that this is not permanently practicable. Luckily, it is perfectly possible to maintain an athletic ability immeasurably higher than the untrained level with, as an example, about sixty to seventy minutes five times a week, and some more on Sundays, a total amount of activity which falls in the region of 7-9 hours a week. 
  People belonging to the second group admittedly have a potentially permanent objective. However, this advantage is largely offset by the deplorable fact that those who solely train for the maintenance of physical health very seldom develop in themselves anything near high abilities. In reality, in most cases they possess a limited athletic level on or near the average level of "normal," untrained people. Typically those less serious keep-fit-exercisers train no more than three times a week, and in a great many cases, even less. Such a level of activity represents the recommendable starting level for people who have not trained before. Later on, a higher frequency and not least regularity is imperatively necessary in order to develop the eminently capable body which ought to be the obligatory companion to a similarly developed and capable mind. Unfortunately, the situation is that these exercisers tend to unnecessarily continue to limit their training to the aforementioned level. This because of the widespread idea that all you need to be "in shape" is this modest activity. It is basically a question of values. When a given milieu or society does not value a particular ability, those who want to develop and possess it are frequently accused of overdoing, and also of selfishness, because devoting time to an "unworthy" activity is by definition a revelation of selfishness. This approach has long ago been irrefutably contradicted by scientific evidence, in addition to the even more significant living proof provided by large numbers of athletes who with themselves show all humanity what you and I can really do, as long as we preserve the undying desire to eliminate and transcend our present limitations, and that the rewards from breaking out of the adaptive limits of the untrained are truly immense. """

  @Test
  def test_process() {

    val sentencerDivider: DivideTask = new StringSplitter()
    val findPatches: HumanComputationTask = new RandomMultipleChoice()
    val agreementDivider: MachineComputationTask = new SubsetAgreement()
    val fixPatches: HumanComputationTask = new RandomFreeText()
    val verifyPatches: HumanComputationTask = new RandomVoteWithUniqueSolution()
    val combiner: CombineTask = new StringCombiner()
    val processMessageProvider = mock[ProcessMessageProvider]
    val sampleTaskObject = new TaskObject(None, None, None, None, None, None)

    var numberOfPatches = 1;

    val input: Option[Tuple2[Seq[Any], Seq[Any]]] = Some((Seq(new ProcessDataChunk(Seq(0), text, None)), Seq.empty))

    // Divide into Patches
    val stringSplitter = sentencerDivider.asInstanceOf[StringSplitter]
    val splitParams = Map((stringSplitter.splitByParam -> stringSplitter.bySentence))
    val splittedInput = input.map {
      case (activeSet, inactiveSet) =>
        sentencerDivider.process(activeSet, inactiveSet, Seq.empty, splitParams, Map.empty, processMessageProvider, sampleTaskObject)
    }

    numberOfPatches = splittedInput.map { case (activeSet, inactiveSet) => activeSet.length + inactiveSet.length }.getOrElse(-1)

    // Find faulty patches
    val randomMultipleChoice = findPatches.asInstanceOf[RandomMultipleChoice]
    val mcParams = Map(randomMultipleChoice.numberOfSelectionsParam -> "3", randomMultipleChoice.numberOfWorkersParam -> "5")
    val selectedInput = splittedInput.map {
      case (activeSet, inactiveSet) =>
        findPatches.process(activeSet, inactiveSet, Seq.empty, mcParams, Map.empty, processMessageProvider, sampleTaskObject)
    }

    assert(selectedInput != None)
    selectedInput.map { case (activeSet, inactiveSet) => assert(numberOfPatches <= activeSet.length + inactiveSet.length) }

    // Find agreement among patches
    val subsetAgreement = agreementDivider.asInstanceOf[SubsetAgreement]
    val agreementParams = Map(subsetAgreement.minSubmissionsParams -> "2")
    val agreedInput = selectedInput.map {
      case (activeSet, inactiveSet) =>
        agreementDivider.process(activeSet, inactiveSet, Seq.empty, agreementParams, Map.empty, processMessageProvider, sampleTaskObject)
    }

    agreedInput.map {
      case (activeSet, inactiveSet) =>
        val groupedProcessDataChunks = activeSet.asInstanceOf[Seq[ProcessDataChunk]].groupBy { processDataChunk => processDataChunk.chunkIds }
        groupedProcessDataChunks.foreach { case (chunkIds, groupedChunks) => assertEquals(1, groupedChunks.size) } // After agreement chunks should be unique
        assert(numberOfPatches <= activeSet.length + inactiveSet.length)
    }

    // Fix Patches
    val randomFreeText = fixPatches.asInstanceOf[RandomFreeText]
    val fixParams = Map(randomFreeText.numberOfWorkersParam -> "5", randomFreeText.distributeWorkParam -> "false")
    val fixedInput = agreedInput.map {
      case (activeSet, inactiveSet) =>
        fixPatches.process(activeSet, inactiveSet, Seq.empty, fixParams, Map.empty, processMessageProvider, sampleTaskObject)
    }

    assert(fixedInput != None)
    fixedInput.map { case (activeSet, inactiveSet) => assert(numberOfPatches <= activeSet.length + inactiveSet.length) }

    // Verify patches
    val randomVoteWithUniqueSolution = verifyPatches.asInstanceOf[RandomVoteWithUniqueSolution]
    val votedInput = fixedInput.map {
      case (activeSet, inactiveSet) =>
        verifyPatches.process(activeSet, inactiveSet, Seq.empty, Map.empty, Map.empty, processMessageProvider, sampleTaskObject)
    }

    votedInput.map {
      case (activeSet, inactiveSet) =>
        val groupedProcessDataChunks = activeSet.asInstanceOf[Seq[ProcessDataChunk]].groupBy { processDataChunk => processDataChunk.chunkIds }
        groupedProcessDataChunks.foreach { case (chunkIds, groupedChunks) => assertEquals(1, groupedChunks.size) } // After voting chunks should be unique

        assertEquals(numberOfPatches, activeSet.length + inactiveSet.length)

        activeSet.union(inactiveSet).asInstanceOf[Seq[ProcessDataChunk]].foreach { processDataChunk =>
          val sentence = processDataChunk.context.asInstanceOf[String]
          assert(text.contains(sentence))

          val correctedSentence = processDataChunk.data.asInstanceOf[String]
          assert(correctedSentence != "")
        }
    }

    // Combine patches
    val result: Any = fixedInput.map {
      case (activeSet, inactiveSet) =>
        combiner.process(activeSet, inactiveSet, Seq.empty, Map.empty, Map.empty, processMessageProvider, sampleTaskObject).toString()
    }
    assertFalse(result == "")
    assertFalse(result == None)
  }
}