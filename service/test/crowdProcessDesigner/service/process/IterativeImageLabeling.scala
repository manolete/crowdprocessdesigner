package crowdProcessDesigner.service.process

import org.scalatest.junit.AssertionsForJUnit
import scala.collection.mutable.ListBuffer
import org.junit.Assert._
import org.junit.Test
import org.junit.Before
import crowdProcessDesigner.interfaces._
import org.scalatest.junit.JUnitSuite
import org.scalatest.mock.MockitoSugar._
import org.mockito.Mockito.stub
import org.mockito.Matchers.any
import org.junit.Test
import org.junit.Before
import org.mockito.Mockito.{ when, doCallRealMethod }
import scala.concurrent._
import scala.concurrent.duration._
import crowdProcessDesigner.service.combine._
import crowdProcessDesigner.service.divide._
import crowdProcessDesigner.service.hct._
import crowdProcessDesigner.service.input._
import crowdProcessDesigner.service.loop._
import java.net._
import java.io._

class IterativeImageLabeling extends JUnitSuite {

  implicit def t2mapper[A, B](t: (A, B)) = new {
    def map[R](f: A => R, g: B => R) = (f(t._1), g(t._2))
  }

  val imageUrls = Seq("https://i.imgur.com/QUUvZo8.png", "https://i.imgur.com/iV0A5J1.png", "https://i.imgur.com/IUooBfD.png", "https://i.imgur.com/UGOBGdZ.png")

  private[this] var processDataChunks = new {
    var activeSet: Seq[ProcessDataChunk] = imageUrls.zipWithIndex.map {
      case (url, index) =>
        new ProcessDataChunk(Seq(index), "", new URL(url))
    }
    var inactiveSet: Seq[ProcessDataChunk] = Seq.empty
  }

  val processMessageProvider = mock[ProcessMessageProvider]
  val sampleTaskObject = new TaskObject(None, None, None, None, None, None)

  @Test
  def test_process() {
    val labelImage: HumanComputationTask = new RandomFreeText()
    val qualityControl: HumanComputationTask = new RandomYesNo()
    val numberOfTimesLoop: LoopTask = new NumberOfTimes()

    var counter = 0

    while (processDataChunks.activeSet.nonEmpty) {
      counter += 1
      // Labeling Task
      val randomFreeText = labelImage.asInstanceOf[RandomFreeText]
      var params = Map(randomFreeText.numberOfWorkersParam -> "5", randomFreeText.distributeWorkParam -> "true")

      val freeTextResult = randomFreeText.process(processDataChunks.activeSet, processDataChunks.inactiveSet, Seq.empty, params, Map.empty, processMessageProvider, sampleTaskObject)
      freeTextResult map (
        activeSet => processDataChunks.activeSet = activeSet.asInstanceOf[Seq[ProcessDataChunk]],
        inactiveSet => processDataChunks.inactiveSet = inactiveSet.asInstanceOf[Seq[ProcessDataChunk]])
        
      processDataChunks.activeSet.foreach(processDataChunk => assert(processDataChunk.data != None))

      // Quality Control
      val randomYesNo = qualityControl.asInstanceOf[RandomYesNo]
      params = Map(randomYesNo.numberOfWorkersParam -> "1", randomYesNo.distributeWorkParam -> "true")
      val yesNoResult = randomYesNo.process(processDataChunks.activeSet, processDataChunks.inactiveSet, Seq.empty, params, Map.empty, processMessageProvider, sampleTaskObject)
      yesNoResult map (
        activeSet => processDataChunks.activeSet = activeSet.asInstanceOf[Seq[ProcessDataChunk]],
        inactiveSet => processDataChunks.inactiveSet = inactiveSet.asInstanceOf[Seq[ProcessDataChunk]])

      // Loop Task
      val numberOftimesLoop = numberOfTimesLoop.asInstanceOf[NumberOfTimes]
      params = Map(numberOftimesLoop.numberOfTimesParam -> "3", numberOftimesLoop.considerOnlyIncorrectData -> "true")
      val loopResult = numberOfTimesLoop.process(processDataChunks.activeSet, processDataChunks.inactiveSet, Seq.empty, params, Map.empty, processMessageProvider, sampleTaskObject)
      loopResult.map (
        activeSet => processDataChunks.activeSet = activeSet.asInstanceOf[Seq[ProcessDataChunk]],
        inactiveSet => processDataChunks.inactiveSet = inactiveSet.asInstanceOf[Seq[ProcessDataChunk]])
    }

    // No Doubled Data
    assertEquals(imageUrls.length, processDataChunks.activeSet.union(processDataChunks.inactiveSet).length)

    // Correctly labeled data and input data are same size
    val correctlyLabeledChunks = processDataChunks.activeSet.union(processDataChunks.inactiveSet)
      .asInstanceOf[Seq[ProcessDataChunk]]
      .filter(p => p.isCorrectData.getOrElse(false))

    val correctlyLabeledChunkIds = correctlyLabeledChunks.map(p => p.chunkIds)
    val inputDataWithoutCorrectlyLabeledData = processDataChunks.activeSet
      .union(processDataChunks.inactiveSet).asInstanceOf[Seq[ProcessDataChunk]]
      .filter { p => !correctlyLabeledChunkIds.contains(p.chunkIds) }

    assertEquals(imageUrls.length, correctlyLabeledChunkIds.length + inputDataWithoutCorrectlyLabeledData.length)

    // Each image is labeled
    correctlyLabeledChunks.asInstanceOf[Seq[ProcessDataChunk]]
      .foreach(chunk => assert(chunk.data.asInstanceOf[String].contains("@Input:")))
  }
}